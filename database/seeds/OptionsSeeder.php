<?php

use Illuminate\Database\Seeder;
use App\Option;

class OptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            Option::create([
                        'key' => 'keyOrName',
                        'value' => '1',
                    ]);
            Option::create([
                        'key' => 'rowCount',
                        'value' => '10',
                    ]);
            Option::create([
                        'key' => 'themeCount',
                        'value' => '0',
                    ]);
            # после теста поставить 180
            Option::create([
                        'key' => 'openNewThemeTime',
                        'value' => '0',
                    ]);
            Option::create([
                        'key' => 'guide',
                        'value' => '0',
                    ]);
            Option::create([
                        'key' => 'offlineUser',
                        'value' => '1',
                    ]);
            Option::create([
                        'key' => 'offlineKey',
                        'value' => 'Recu2YCzcmvA3KXA',
                    ]);

    }
}
