<?php

use Illuminate\Database\Seeder;
use App\Question;

class QuestionsModule03Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            $array = [
                        'key'=>'Какой процент молодых граждан-членов должен быть в некоммерческой организации, чтобы она считалась молодёжной:',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Какой процент молодых граждан-членов должен быть в некоммерческой организации, чтобы она считалась молодёжной:',
                    'content'=>json_decode('[
                        {"check":false,"text":"Не менее 90%"},
                        {"check":false,"text":"Не менее 85%"},
                        {"check":true,"text":"Не менее 75%"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'«Юридические лица, а также граждане, осуществляющие предпринимательскую деятельность без образования юридического лица, оказывающие услуги по социальной поддержке молодым гражданам и семьям, находящимся в трудной жизненной ситуации», это:',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'«Юридические лица, а также граждане, осуществляющие предпринимательскую деятельность без образования юридического лица, оказывающие услуги по социальной поддержке молодым гражданам и семьям, находящимся в трудной жизненной ситуации», это:',
                    'content'=>json_decode('[
                        {"check":false,"text":"Неформальные объединения молодёжи"},
                        {"check":true,"text":"Социальные службы для молодёжи"},
                        {"check":false,"text":"Молодёжные организации"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Молодёжная политика и работа с молодёжью имеют одинаковое значение',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Молодёжная политика и работа с молодёжью имеют одинаковое значение',
                    'content'=>json_decode('[
                        {"check":false,"text":"Верно"},
                        {"check":true,"text":"Не верно"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'В какой модели реализации государственной молодёжной политики муниципальный уровень является ведущим:',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'В какой модели реализации государственной молодёжной политики муниципальный уровень является ведущим:',
                    'content'=>json_decode('[
                        {"check":false,"text":"Консервативная"},
                        {"check":true,"text":"Государственно-общественное партнёрство"},
                        {"check":false,"text":"Социально-государственная модель"},
                        {"check":false,"text":"Коммунитарная модель"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Доказательная молодёжная политика строится на (выберите три правильных ответа):',
                        'module_id'=>3,
                        'type_id'=>2,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Доказательная молодёжная политика строится на (выберите три правильных ответа):',
                    'content'=>json_decode('[
                        {"check":true,"text":"Научных фактах"},
                        {"check":false,"text":"Поверхностных представлениях"},
                        {"check":true,"text":"Данных объективных исследований"},
                        {"check":false,"text":"Впечатлениях и обычных знаниях о ситуации и положении молодёжи"},
                        {"check":true,"text":"Теоретических выводах экспертов"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Выберите правильные ответы в продолжение фразы «Законодательство о государственной молодёжной политике в Кыргызской Республике основывается на»: ',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Выберите правильные ответы в продолжение фразы «Законодательство о государственной молодёжной политике в Кыргызской Республике основывается на»: ',
                    'content'=>json_decode('[
                        {"check":false,"text":"Конституции и действующих законах"},
                        {"check":false,"text":"Нормативных правовых актах, принимаемых в соответствии с действующими законами"},
                        {"check":false,"text":"Международных договорах, вступивших в силу в установленном законом порядке, участником которых является Кыргызская Республика, регулирующие ГМП прямо либо косвенно"},
                        {"check":true,"text":"Все ответы верны"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Для обеспечения качественного анализа эффективность выполнения локальных программ для молодёжи используют: ',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Для обеспечения качественного анализа эффективность выполнения локальных программ для молодёжи используют: ',
                    'content'=>json_decode('[
                        {"check":false,"text":"SNW и SMART анализ"},
                        {"check":true,"text":"SWOT и PEST анализ"},
                        {"check":false,"text":"STEEP и STEEPV-анализ"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'«Координация сотрудничества между органом государственной власти ответственным за реализацию молодёжной политики и другими секторами по вопросам молодёжи и услуг для неё» является целью:',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'«Координация сотрудничества между органом государственной власти ответственным за реализацию молодёжной политики и другими секторами по вопросам молодёжи и услуг для неё» является целью:',
                    'content'=>json_decode('[
                        {"check":true,"text":"Межсекторной молодёжной политики"},
                        {"check":false,"text":"Муниципальной молодёжной политики"},
                        {"check":false,"text":"Международных организаций"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Выберите три признака эффективного и социально значимого участия молодёжи в политической жизни:',
                        'module_id'=>3,
                        'type_id'=>2,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Выберите три признака эффективного и социально значимого участия молодёжи в политической жизни:',
                    'content'=>json_decode('[
                        {"check":true,"text":"Совещательный характер"},
                        {"check":true,"text":"Участие, возглавляемое молодёжью"},
                        {"check":true,"text":"Совместная деятельность, при которой молодые люди эффективно участвуют в регулярных процессах принятия политических решений в качестве избирателей, членов парламента, активистов политических партий или членов правозащитных групп"},
                        {"check":false,"text":"Избирательное участие"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Является ли молодёжное участи формой партнёрства между молодёжью и взрослыми: ',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Является ли молодёжное участи формой партнёрства между молодёжью и взрослыми: ',
                    'content'=>json_decode('[
                        {"check":true,"text":"Да"},
                        {"check":false,"text":"Нет"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Молодых людей обязывают прийти для участия в демонстрации, заставляя их нести плакаты и баннеры. К какой форме участия можно отнести пример:',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Молодых людей обязывают прийти для участия в демонстрации, заставляя их нести плакаты и баннеры. К какой форме участия можно отнести пример:',
                    'content'=>json_decode('[
                        {"check":false,"text":"Вовлечение и информирование"},
                        {"check":true,"text":"Манипуляция"},
                        {"check":false,"text":"Инициатива и правление молодёжи"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Бойкотирование продукции или заведений:',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Бойкотирование продукции или заведений:',
                    'content'=>json_decode('[
                        {"check":true,"text":"Является формой участия молодёжи"},
                        {"check":false,"text":"Не является формой участия молодёжи"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Первые навыки принятия решений приобретаются:',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Первые навыки принятия решений приобретаются:',
                    'content'=>json_decode('[
                        {"check":false,"text":"В работе общественных организаций"},
                        {"check":false,"text":"На государственной службе"},
                        {"check":true,"text":"В семье и обществе, где растёт молодой человек"},
                        {"check":false,"text":"Нигде"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Одним из самых понятных методов участия молодёжи в процессах принятия решений в КР являются выборы в представительные органы – местные кенеши и Жогорку Кенеш:',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Одним из самых понятных методов участия молодёжи в процессах принятия решений в КР являются выборы в представительные органы – местные кенеши и Жогорку Кенеш:',
                    'content'=>json_decode('[
                        {"check":true,"text":"Да"},
                        {"check":false,"text":"Нет"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Эффективное и социально значимое участие молодёжи в политической жизни обладает одним из следующих трёх признаков:',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Эффективное и социально значимое участие молодёжи в политической жизни обладает одним из следующих трёх признаков:',
                    'content'=>json_decode('[
                        {"check":false,"text":"Носит совещательный характер"},
                        {"check":false,"text":"Его результатом может стать участие, возглавляемое молодёжью, при котором молодые люди непосредственно влияют на принятие решений внутри собственных молодёжных объединений"},
                        {"check":false,"text":"Включает в себя совместную деятельность, при которой молодые люди эффективно участвуют в регулярных процессах принятия политических решений в качестве избирателей, членов парламента, активистов политических партий или членов правозащитных групп"},
                        {"check":true,"text":"Все ответы верны"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Списки кандидатов в депутаты КР должны учитывать требование:',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Списки кандидатов в депутаты КР должны учитывать требование:',
                    'content'=>json_decode('[
                        {"check":true,"text":"Не менее 15% кандидатов должны быть молодыми, не старше 35 лет"},
                        {"check":false,"text":"Не менее 15% кандидатов должны быть молодыми, не старше 28 лет"},
                        {"check":false,"text":"Не менее 20% кандидатов должны быть молодыми, не старше 28 лет"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Являются ли основными направлениями деятельности органов МСУ в области молодёжной политики: обеспечение информационного сопровождения, разработка, утверждение и реализация программ местного уровня?',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Являются ли основными направлениями деятельности органов МСУ в области молодёжной политики: обеспечение информационного сопровождения, разработка, утверждение и реализация программ местного уровня?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Нет"},
                        {"check":true,"text":"Да"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Эдвокаси это:',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Эдвокаси это:',
                    'content'=>json_decode('[
                        {"check":false,"text":"PR деятельность"},
                        {"check":true,"text":"Деятельность по защите прав и продвижению общественных интересов"},
                        {"check":false,"text":"Деятельность по мобилизации населения"},
                        {"check":false,"text":"Деятельность по лоббированию интересов"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'ОМСУ не имеет полномочий разрабатывать, утверждать и реализовывать программы местного уровня в сфере государственной молодёжной политики:',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'ОМСУ не имеет полномочий разрабатывать, утверждать и реализовывать программы местного уровня в сфере государственной молодёжной политики:',
                    'content'=>json_decode('[
                        {"check":false,"text":"Верно"},
                        {"check":true,"text":"Не верно"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Какой орган местного самоуправления принимает молодёжную политику муниципалитета?',
                        'module_id'=>3,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Какой орган местного самоуправления принимает молодёжную политику муниципалитета?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Айыл окмоту / мэрия"},
                        {"check":true,"text":"Местный кенеш"},
                        {"check":false,"text":"Глава районной администрации (Аким)"},
                        {"check":false,"text":"Специалист по делам молодёжи"}
                    ]'),
                ];
            }
            Question::create($array);

    }
}
