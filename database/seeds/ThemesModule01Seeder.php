<?php

use Illuminate\Database\Seeder;
use App\Theme;

class ThemesModule01Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            $array = ['key'=>'Важность понимания темы прав человека', 'lesson_id' => 1, 'module_id' => 1, 'activity' => true];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'1 тема (' . $language . ')',
                    'content'=>'Контент 1 темы на ' . $language . ' языке',
                ];
            }
            Theme::create($array);
            $array = ['key'=>'Термин «Права человека»', 'lesson_id' => 1, 'module_id' => 1, 'activity' => true];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'2 тема (' . $language . ')',
                    'content'=>'Контент 2 темы на ' . $language . ' языке',
                ];
            }
            Theme::create($array);
            $array = ['key'=>'Историческая хронология формирования идей о правах человека', 'lesson_id' => 1, 'module_id' => 1, 'activity' => true];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'3 тема (' . $language . ')',
                    'content'=>'Контент 3 темы на ' . $language . ' языке',
                ];
            }
            Theme::create($array);
            $array = ['key'=>'Международные документы в области прав человека', 'lesson_id' => 1, 'module_id' => 1, 'activity' => true];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'4 тема (' . $language . ')',
                    'content'=>'Контент 4 темы на ' . $language . ' языке',
                ];
            }
            Theme::create($array);
            $array = ['key'=>'Конституция Киргизской Республики и права человека', 'lesson_id' => 1, 'module_id' => 1, 'activity' => true];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'5 тема (' . $language . ')',
                    'content'=>'Контент 5 темы на ' . $language . ' языке',
                ];
            }
            Theme::create($array);
            $array = ['key'=>'Ратификация международных соглашений', 'lesson_id' => 1, 'module_id' => 1, 'activity' => true];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'6 тема (' . $language . ')',
                    'content'=>'Контент 6 темы на ' . $language . ' языке',
                ];
            }
            Theme::create($array);
            $array = ['key'=>'Совет по правам человека ООН', 'lesson_id' => 1, 'module_id' => 1, 'activity' => true];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'7 тема (' . $language . ')',
                    'content'=>'Контент 7 темы на ' . $language . ' языке',
                ];
            }
            Theme::create($array);
            $array = ['key'=>'Человеческое достоинство и права человека', 'lesson_id' => 1, 'module_id' => 1, 'activity' => true];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'8 тема (' . $language . ')',
                    'content'=>'Контент 8 темы на ' . $language . ' языке',
                ];
            }
            Theme::create($array);

    }
}
