<?php

use Illuminate\Database\Seeder;
use App\Page;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            $array = ['key' => 'Главная','url' => 'home'];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Главная (' . $language . ')',
                    'content'=>'<div class="hexa-block-content"><div class="hexa-block-content-text mb-3 mce-content-body" id="mce_0" style="position: relative;" spellcheck="false"><h1 style="text-align: center;" data-mce-style="text-align: center;">Пример страницы, созданной в Билдере. Это заголовок.</h1></div></div><div class="hexa-block-content">
                    <div class="hexa-block-content-text mb-3 mce-content-body" id="mce_9" style="position: relative;" spellcheck="false"><p style="text-align: justify;" data-mce-style="text-align: justify;">Обычный абзац, выровненный по ширине. В этом абзаце отмечены следующие типы выделений текста: <strong>жирный шрифт</strong>, <em>наклонный шрифт</em> и <span style="text-decoration: underline;" data-mce-style="text-decoration: underline;">подчеркивание</span>. Кроме того, можно размещать всплывающие окна на термины, содержащиеся в глоссарии: например слово <span data-popup="popover" data-placement="bottom" data-content="в рекламе, на месте продажи – это прямоугольный или треугольный планшет из пластика, ткани или бумаги, подвешиваемый в витринах, на стенах или в проходах торгового помещения и несущий рекламные сообщения.">Баннер.</span> Достаточно на него нажать и получим всплывающее окно с объяснением данного термина. Так же возможно размещение обычных ссылок на сторонние ресурсы, например: <a data-mce-href="activemedia.ua" href="activemedia.ua">activemedia.ua</a>.&nbsp;</p></div>
                </div><div class="hexa-block-content">
                    <div class="hexa-block-content-text mb-3 mce-content-body" id="mce_11" style="position: relative;" spellcheck="false"><p style="text-align: center;" data-mce-style="text-align: center;">Выравнивание текста по центру экрана</p><p>Выравнивание текста по левому краю</p><p style="text-align: right;" data-mce-style="text-align: right;">Выравнивание текста по правому краю</p></div>
                </div><div class="hexa-block-content">
                    <div class="hexa-block-content-text mb-3 mce-content-body" id="mce_10" style="position: relative;" spellcheck="false"><h1>Заголовок 1-го уровня</h1><h2>Заголовок 2-го уровня</h2><h3>Заголовок 3-го уровня</h3><h4>Заголовок 4-го уровня</h4><h5>Заголовок 5-го уровня</h5><h6>Заголовок 6-го уровня</h6></div>
                </div><div class="hexa-block-content">
                    <div class="hexa-block-content-text mb-3 mce-content-body" id="mce_12" style="position: relative;" spellcheck="false"><p>Нумерованный и ненумерованный списки:</p><ol><li>Элемент</li><li>Элемент</li><li>Элемент</li></ol><ul><li>Элемент</li><li>Элемент</li><li>Элемент</li></ul></div>
                </div><div class="hexa-block-content warning-box warning-box-text">
                    <div class="hexa-block-content-text mb-3 mce-content-body" id="mce_13" style="position: relative;" spellcheck="false"><p>Любой блок (текстовый, аккордион или содержащий embed-объекты) может быть обернут рамкой и содержать один из 16 иконок, помогающих пользователю понять содержание контента этой области.</p></div>
                </div><div class="hexa-block-content">
                    <div class="hexa-block-content-text mb-3 mce-content-body" id="mce_14" style="position: relative;" spellcheck="false"><p>Также, возможно добавление изображений, в том числе хранящихся в медиа-библиотеке. Для этого необходимо выбрать данное изображение из списка при вставке.</p><p><img src="../../../storage/image/Android-R2D2.png" alt="Android" width="353" height="235" style="display: block; margin-left: auto; margin-right: auto;" data-mce-style="display: block; margin-left: auto; margin-right: auto;"><br></p></div>
                </div>
                <div class="hexa-block-content">
                    <div class="hexa-block-content-text mb-3 mce-content-body" id="mce_6" style="position: relative;" spellcheck="false"><p>Модуль Аккордион</p></div>
                </div>
            
                <div class="hexa-block-content">
                    
                        <div class="hexa-block-content-accordion card-group-control card-group-control-right mb-3" id="ac-1581422562190" data-child="3">
                            <div class="card mb-0">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">
                                        <a class="text-default collapsed" data-toggle="collapse" href="#ac-1581422562190-1" aria-expanded="false">Название первого элемента</a>
                                    </h6>
                                    <div class="header-elements">
                                        <div class="list-icons">
                                            <a class="list-icons-item btn-remove-accordion-element" data-action="remove"></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="ac-1581422562190-1" class="collapse" data-parent="#ac-1581422562190" style="">
                                    <div class="card-body p-3">
                                        <div class="hexa-block-content-text mce-content-body" id="mce_7" spellcheck="false" style="position: relative;"><p>Каждый элемент аккордиона содержит один текстовый молуль, внутри которого работают все функции работы с текстом</p></div>
                                    </div>
                                </div>
                            </div>

                            <div class="card mb-0">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">
                                        <a class="text-default collapsed" data-toggle="collapse" href="#ac-1581422562190-2" aria-expanded="false">Accordion Item #2</a>
                                    </h6>
                                    <div class="header-elements">
                                        <div class="list-icons">
                                            <a class="list-icons-item btn-remove-accordion-element" data-action="remove"></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="ac-1581422562190-2" class="collapse" data-parent="#ac-1581422562190" style="">
                                    <div class="card-body p-3">
                                        <div class="hexa-block-content-text mce-content-body" id="mce_8" spellcheck="false" style="position: relative;"><p>Еще один текстовый модуль</p></div>
                                    </div>
                                </div>
                            </div>

                            <div class="card mb-0">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">
                                        <a class="text-default collapsed" data-toggle="collapse" href="#ac-1581422562190-3" aria-expanded="false">Accordion Item #3</a>
                                    </h6>
                                    <div class="header-elements">
                                        <div class="list-icons">
                                            <a class="list-icons-item btn-remove-accordion-element" data-action="remove"></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="ac-1581422562190-3" class="collapse" data-parent="#ac-1581422562190" style="">
                                    <div class="card-body p-3">
                                        <div class="hexa-block-content-text mce-content-body" id="mce_9" spellcheck="false" style="position: relative;"><p>Элементы аккордиона могут быть добавлены в неограниченном количестве</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            
                <div class="hexa-block-content">
                    <div class="hexa-block-content-text mb-3 mce-content-body" id="mce_10" style="position: relative;" spellcheck="false"><p>Модуль embed-объект может содержать любые объекты, которые предоставляются указанным стандартом, а именно: видео с сайтов <strong>youtube</strong>, <strong>vimeo</strong>, презентации <strong>prezi</strong> и многое другое</p></div>
                </div>
            
                <div class="hexa-block-content">
                        <div class="embed-responsive embed-responsive-16by9 mb-3">
                            <embed class="embed-responsive-item" src="https://prezi.com/p/8wqi5_tul_3v/embed">
                        </div>
                </div>',
                ];
            }
            Page::create($array);

            $array = ['key' => 'О нас','url' => 'about'];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'О нас (' . $language . ')',
                    'content'=>'Описание страницы на ' . $language . ' языке',
                ];
            }
            Page::create($array);

            $array = ['key' => 'Контакты','url' => 'contacts'];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Контакты (' . $language . ')',
                    'content'=>'Описание страницы на ' . $language . ' языке',
                ];
            }
            Page::create($array);

            $array = ['key' => 'Вступление','url' => 'introduction-module-1'];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Вступление (' . $language . ')',
                    'content'=>'Описание страницы на ' . $language . ' языке',
                ];
            }
            Page::create($array);

            $array = ['key' => 'Заключение','url' => 'conclusion-module-1'];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Заключение (' . $language . ')',
                    'content'=>'Описание страницы на ' . $language . ' языке',
                ];
            }
            Page::create($array);

            $array = ['key' => 'Литература','url' => 'literature-module-1'];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Литература (' . $language . ')',
                    'content'=>'Описание страницы на ' . $language . ' языке',
                ];
            }
            Page::create($array);

    }
}
