<?php

use Illuminate\Database\Seeder;
use App\Lesson;

class LessonsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $array = [
                'key'=>'Урок 1',
                'module_id' => 1,
                'data'=>json_decode('{
                    "introduction_id":"0",
                    "conclusion_id":"0"
                }'),
            ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Название урока на «'.$language.'» языке',
                ];
            }
            Lesson::create($array);
    }
}
