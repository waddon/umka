<?php

use Illuminate\Database\Seeder;
use App\Course;

class Course01Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            $array = [
                'key'=>'УМК@',
                'data'=>json_decode('{
                    "point_score":"1",
                    "point_one":"1",
                    "question_count":"3",
                    "time_for_passing":30,
                    "time_for_passing_one":3,
                    "max_count_passing":50,
                    "time_between_passing":1,
                    "time_between_passing_one":4
                }'),
            ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'УМК@',
                    'messages'=>json_decode('{"welcome":"<ul><li>На прохождение экзамена вам даётся <strong>30 минут</strong> и <strong>три попытки</strong>.</li><li>Если вы не сможете пройти тест с первой попытки, следующая будет предоставлена через <strong>24 часа</strong>.</li><li>Для успешного прохождения теста вам необходимо дать <strong>100% правильных ответов (20 правильных ответов на 20 вопросов)</strong>.</li><li><strong>Если вы не сможете пройти тест за три попытки, система автоматически заблокирует ваш доступ к системе.</strong> Пожалуйста, обратитесь к вашему куратору в Государственное агентство по делам молодёжи, физической культуры и спорта при Правительстве Кыргызской Республики.</li></ul>","success":"<div class=\"text-center\"><strong>Поздравляем с успешным прохождением экзамена по курсу УМК@</strong></div>","failure":"failure text","already":"<div class=\"text-center\"><strong>Экзамен по курсу УМК@ уже успешно сдан</strong></strong></div>","exhausted":"Попытки исчерпаны","wait":"Прошло недостаточно времени после окончания предыдущей попытки прохождения. Повторное прохождение экзамена будет доступно "}'),
                ];
            }
            Course::create($array);

    }
}
