<?php

use Illuminate\Database\Seeder;
use App\Question;

class QuestionsModule05Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            $array = [
                        'key'=>'Что является целью социального проекта?',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Что является целью социального проекта?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Создание рабочих мест путём строительства цеха по производству кирпича"},
                        {"check":false,"text":"Обеспечение благосостояния членов НКО"},
                        {"check":false,"text":"Повышение прибыли коммерческой организации за счёт внедрения передовых технологий"},
                        {"check":true,"text":"Разрешение общественно значимых проблем местного сообщества или уязвимых групп"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Кто такие бенефициары?',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Кто такие бенефициары?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Люди, работающие безвозмездно в целях благотворительности – по оказанию общественно значимых услуг, проведению работ и т. п."},
                        {"check":false,"text":"Частные лица или организации, которые делают пожертвования или дают гранты"},
                        {"check":true,"text":"Лица, чья жизнь каким-то образом улучшится с помощью проекта"},
                        {"check":false,"text":"Физические или юридические лица, финансирующие какое-либо мероприятие для рекламы собственной деятельности"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Что такое жизненный цикл проекта?',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Что такое жизненный цикл проекта?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Это описание проблемы, чёткие сроки начала и окончания работ, специально выделенные ресурсы, запланированные результаты и последовательность шагов по эффективному разрешению проблемных задач"},
                        {"check":true,"text":"Логическая последовательность действий по планированию и последовательному выполнению проекта, которая состоят из следующих стадий: концептуальная фаза, разработка проекта, выполнение проекта и завершение проекта"},
                        {"check":false,"text":"Это документ, определяющий главные цели и задачи, важнейшие стратегические направления развития и основные мероприятия органов местного самоуправления, главной целью которых является устойчивое повышение качества уровня жизни местного сообщества"},
                        {"check":false,"text":"Включает описание проблемы, чёткие сроки начала и окончания работ, специально выделенные ресурсы, запланированные результаты и последовательность шагов по эффективному разрешению проблемных задач"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Что является фактором, подтверждающим законность проектной деятельности?',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Что является фактором, подтверждающим законность проектной деятельности?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Устное одобрение акима госадминистрации района"},
                        {"check":false,"text":"Благословение аксакалов"},
                        {"check":false,"text":"Свидетельство о регистрации организации"},
                        {"check":true,"text":"Договор о реализации проекта с донором"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Какой из вариантов ответов не имеет отношение к понятию «фандрайзинг»?',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Какой из вариантов ответов не имеет отношение к понятию «фандрайзинг»?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Сбор средств"},
                        {"check":true,"text":"Организация сбора рекомендаций"},
                        {"check":false,"text":"Привлечение волонтёров"},
                        {"check":false,"text":"Использование материальной базы партнёров"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Что такое государственный социальный заказ?',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Что такое государственный социальный заказ?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Добровольная деятельность граждан и юридических лиц по предоставлению государственных услуг"},
                        {"check":false,"text":"Процесс организации, планирования деятельности, направленный на эффективное выполнение государственных программ"},
                        {"check":true,"text":"Форма реализации социальных программ и проектов за счёт государственных бюджетных средств"},
                        {"check":false,"text":"Деятельность по оказанию целевой благотворительной помощи лицам с ограниченными возможностями"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Что из нижеперечисленного не является ресурсом проекта?',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Что из нижеперечисленного не является ресурсом проекта?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Нематериальные объекты (знание, опыт)"},
                        {"check":false,"text":"Специалисты проекта"},
                        {"check":true,"text":"Социально-экономическая ситуация"},
                        {"check":false,"text":"Финансовые средств"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Какой из вариантов ответов не характеризует длительность (продолжительность) проекта?',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Какой из вариантов ответов не характеризует длительность (продолжительность) проекта?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Краткосрочный"},
                        {"check":false,"text":"Долгосрочный"},
                        {"check":false,"text":"Среднесрочный"},
                        {"check":true,"text":"Регулярный"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Что не подлежит управлению в ходе реализации проекта?',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Что не подлежит управлению в ходе реализации проекта?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Объёмы и виды работ по проекту"},
                        {"check":false,"text":"Ресурсы, требуемые для осуществления проекта"},
                        {"check":true,"text":"Изменение миссии донора"},
                        {"check":false,"text":"Сроки, продолжительность"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Что не относится к функциям управления проектом?',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Что не относится к функциям управления проектом?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Администрирование"},
                        {"check":false,"text":"Бухгалтерский учёт"},
                        {"check":false,"text":"Планирование"},
                        {"check":true,"text":"Подчинение"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Обязательство организации работы с молодёжью в самоуправлениях наравне с другими сферами устанавливает:',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Обязательство организации работы с молодёжью в самоуправлениях наравне с другими сферами устанавливает:',
                    'content'=>json_decode('[
                        {"check":false,"text":"Закон КР «Об основах государственной молодёжной политики»"},
                        {"check":true,"text":"Закон КР «О местном самоуправлении»"},
                        {"check":false,"text":"Закон КР «О некоммерческих организациях»"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Что такое услуга «низкого порога» в молодёжных центрах:',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Что такое услуга «низкого порога» в молодёжных центрах:',
                    'content'=>json_decode('[
                        {"check":true,"text":"К молодым людям не предъявляется никаких требований и нет никаких критериев для того, чтобы включиться в активность или самим инициировать какое-то занятие"},
                        {"check":false,"text":"Молодые люди спрашивают совета у взрослых о существующих активностях и присоединяются к ним"},
                        {"check":false,"text":"Молодые люди приглашаются взрослыми и выполняют минимально необходимые задания в молодёжном центре"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Цель равного обучения:',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Цель равного обучения:',
                    'content'=>json_decode('[
                        {"check":false,"text":"Создание безбарьерного консультирования"},
                        {"check":true,"text":"Создание благоприятного поведения или изменение агрессивного поведения в рамках целевой группы молодёжи"},
                        {"check":false,"text":"Открытие молодёжного информационного центра"},
                        {"check":false,"text":"Подготовка молодёжной стратегии муниципалитета"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Цифровая молодёжная работа может быть:',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Цифровая молодёжная работа может быть:',
                    'content'=>json_decode('[
                        {"check":false,"text":"PR технологией"},
                        {"check":false,"text":"Методом молодёжной работы"},
                        {"check":true,"text":"Инструментом, деятельностью или содержанием в работе с молодёжью"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Что не нужно для создания молодёжного информационного центра:',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Что не нужно для создания молодёжного информационного центра:',
                    'content'=>json_decode('[
                        {"check":false,"text":"Создание доброжелательной атмосферы"},
                        {"check":false,"text":"Мобильная информационно-разъяснительная деятельность (аутрич)"},
                        {"check":false,"text":"Участие молодёжи на всех этапах молодёжной информационной работы"},
                        {"check":true,"text":"Участие взрослых на всех этапах молодёжной информационной работы"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Для того чтобы организация считалась некоммерческой необходимо соблюдение нескольких условий (уберите лишнее):',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Для того чтобы организация считалась некоммерческой необходимо соблюдение нескольких условий (уберите лишнее):',
                    'content'=>json_decode('[
                        {"check":false,"text":"Добровольность создания организации"},
                        {"check":false,"text":"Создание организации на основе общности интересов для реализации духовных потребностей либо членов организации, либо всего общества"},
                        {"check":true,"text":"Извлечение прибыли может являться основной целью деятельности"},
                        {"check":false,"text":"Прибыль не должна распределяться между членами, учредителями и должностными лицами"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'При соблюдении определённых условий некоммерческим организациям:',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'При соблюдении определённых условий некоммерческим организациям:',
                    'content'=>json_decode('[
                        {"check":true,"text":"Не запрещено заниматься деятельностью по извлечению прибыли"},
                        {"check":false,"text":"Запрещено заниматься деятельностью по извлечению прибыли"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Какие формы некоммерческих организаций в перечне созданы для общественной пользы / социального блага (выберете три формы)?',
                        'module_id'=>5,
                        'type_id'=>2,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Какие формы некоммерческих организаций в перечне созданы для общественной пользы / социального блага (выберете три формы)?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Товарищество собственников жилья (кондоминиум)"},
                        {"check":true,"text":"Общественные объединения"},
                        {"check":true,"text":"Жамааты (общинные организации)"},
                        {"check":false,"text":"Объединение работодателей"},
                        {"check":true,"text":"Общественные фонды"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Какой фактор успеха имеет самое сильное значение в социальном предпринимательстве',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Какой фактор успеха имеет самое сильное значение в социальном предпринимательстве',
                    'content'=>json_decode('[
                        {"check":false,"text":"Тиражируемость"},
                        {"check":false,"text":"Инновации"},
                        {"check":true,"text":"Социальное воздействие"},
                        {"check":false,"text":"Самоокупаемость"},
                        {"check":false,"text":"Масштабируемость "}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Рекомендуемый в Модуле 5 стиль разрешения конфликта при управлении проектом носит название:',
                        'module_id'=>5,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Рекомендуемый в Модуле 5 стиль разрешения конфликта при управлении проектом носит название:',
                    'content'=>json_decode('[
                        {"check":false,"text":"Сетка Багуа"},
                        {"check":true,"text":"Сетка Томаса – Килмена"},
                        {"check":false,"text":"Сетка Фибоначчи"}
                    ]'),
                ];
            }
            Question::create($array);

    }
}
