<?php

use Illuminate\Database\Seeder;
use App\Question;

class QuestionsModule01Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $array = [
                        'key'=>'Права человека, с точки зрения их «вертикальной концепции» – это',
                        'module_id'=>1,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Права человека, с точки зрения их «вертикальной концепции» – это',
                    'content'=>json_decode('[
                        {"check":false,"text":"исключительно те правовые нормы, которые отражены в законодательстве и регулируют правила взаимодействия людей в обществе"},
                        {"check":false,"text":"категория морали, не имеющая отношения к правовым нормам"},
                        {"check":true,"text":"нормы права и морали, регулирующие взаимоотношения государства, представителей власти с каждым отдельным человеком"},
                        {"check":false,"text":"совокупность общеопределенных норм, регулирующих общественные отношения в жизни"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Выберите из предложенных вариантов ответов два принципа ограничения прав человека',
                        'module_id'=>1,
                        'type_id'=>2,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Выберите из предложенных вариантов ответов два принципа ограничения прав человека',
                    'content'=>json_decode('[
                        {"check":true,"text":"права человека могут быть ограничены только на основании Закона"},
                        {"check":false,"text":"права человека могут быть ограничены приказом министерства"},
                        {"check":false,"text":"должны быть точными, не трактоваться широко"},
                        {"check":false,"text":"должны быть обоснованы необходимостью защиты чести и достоинства Президента"},
                        {"check":true,"text":"могут быть введены в интересах большинства населения"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Отметьте «абсолютные» права, не подлежащие ограничению',
                        'module_id'=>1,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Отметьте «абсолютные» права, не подлежащие ограничению',
                    'content'=>json_decode('[
                        {"check":false,"text":"право на самоопределение и независимость"},
                        {"check":false,"text":"право на защиту от внешнего агрессора"},
                        {"check":false,"text":"свободу слова и организацию митингов"},
                        {"check":true,"text":"право на свободу от пыток и свободу от рабства"},
                        {"check":false,"text":"право на тайну переписки"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Какой из нижеперечисленных международных договоров не ратифицирован и не обязателен для исполнения Кыргызской Республикой?',
                        'module_id'=>1,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Какой из нижеперечисленных международных договоров не ратифицирован и не обязателен для исполнения Кыргызской Республикой?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Всеобщая декларация прав человека"},
                        {"check":false,"text":"Международный пакт о гражданских и политических правах"},
                        {"check":false,"text":"Международный пакт об экономических и социальных правах"},
                        {"check":true,"text":"Конвенция о правах инвалидов (ЛОВЗ)"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Пронумеруйте (от 1 до 5) документы в порядке иерархии правовых норм начиная  наивысшей',
                        'module_id'=>1,
                        'type_id'=>3,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Пронумеруйте (от 1 до 5) документы в порядке иерархии правовых норм начиная  наивысшей',
                    'content'=>json_decode('[
                        {"check":false,"text":"Конституция Кыргызской Республики"},
                        {"check":false,"text":"Кодекс Кыргызской Республики «О детях»"},
                        {"check":false,"text":"Закон «Об образовании»"},
                        {"check":false,"text":"Указ Президента Кыргызской Республики"},
                        {"check":false,"text":"Типовое Положение об общеобразовательных учреждениях"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Выберите из списка несколько прав человека, которые относятся к экономическим, социальным и культурным правам (второе поколение)',
                        'module_id'=>1,
                        'type_id'=>2,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Выберите из списка несколько прав человека, которые относятся к экономическим, социальным и культурным правам (второе поколение)',
                    'content'=>json_decode('[
                        {"check":true,"text":"право на образование"},
                        {"check":false,"text":"право избирать и быть избранным"},
                        {"check":true,"text":"право на труд"},
                        {"check":false,"text":"свобода слова"},
                        {"check":false,"text":"свобода от пыток"},
                        {"check":true,"text":"право на охрану и поддержку семьи, материнства, детства"},
                        {"check":false,"text":"право на справедливое судебное разбирательств"},
                        {"check":true,"text":"право на наивысший достижимый уровень здоровья, его здоровья и медицинскую помощь"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Для защиты прав и свобод граждан в государстве избирается',
                        'module_id'=>1,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Для защиты прав и свобод граждан в государстве избирается',
                    'content'=>json_decode('[
                        {"check":false,"text":"судья по правам человека"},
                        {"check":true,"text":"уполномоченный по правам человека (Омбудсмен)"},
                        {"check":false,"text":"секретаря по правам человек"},
                        {"check":false,"text":"суд присяжных"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Какой день считается Международным днём Прав человека?',
                        'module_id'=>1,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Какой день считается Международным днём Прав человека?',
                    'content'=>json_decode('[
                        {"check":false,"text":"5 мая"},
                        {"check":false,"text":"12 июля"},
                        {"check":false,"text":"12 декабря"},
                        {"check":true,"text":"10 декабря"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'В зависимости от чего человек должен обладать всеми правами и свободами?',
                        'module_id'=>1,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'В зависимости от чего человек должен обладать всеми правами и свободами?',
                    'content'=>json_decode('[
                        {"check":false,"text":"от образования"},
                        {"check":false,"text":"от национальности, расы, пола, религии"},
                        {"check":false,"text":"от богатства, политических убеждений"},
                        {"check":true,"text":"ни от чего, (все должны обладать равными правами)"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'В 1948 году ООН был принят документ, в котором отражены основные права и свободы граждан. Как называется этот документ?',
                        'module_id'=>1,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'В 1948 году ООН был принят документ, в котором отражены основные права и свободы граждан. Как называется этот документ?',
                    'content'=>json_decode('[
                        {"check":false,"text":"«Декларация прав человека и гражданина»"},
                        {"check":false,"text":"«Закон о правах и законах граждан»"},
                        {"check":false,"text":"«Билль о правах человека»"},
                        {"check":true,"text":"«Всеобщая декларация прав человека»"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Какой орган занимается предупреждением пыток в Кыргызской Республике?',
                        'module_id'=>1,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Какой орган занимается предупреждением пыток в Кыргызской Республике?',
                    'content'=>json_decode('[
                        {"check":true,"text":"Национальный центр по предупреждению пыток "},
                        {"check":false,"text":"Республиканский центр по предупреждению пыток"},
                        {"check":false,"text":"Комиссия по предупреждению пыток"},
                        {"check":false,"text":"Совет по предупреждению пыток"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Какой из документов не входит в Международный Билль о правах человека?',
                        'module_id'=>1,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Какой из документов не входит в Международный Билль о правах человека?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Международный пакт о гражданских и политических правах"},
                        {"check":false,"text":"Международный пакт об экономических, социальных и культурных правах"},
                        {"check":false,"text":"Всеобщая Декларация прав человека"},
                        {"check":true,"text":"Конвенция СНГ о правах и основных свободах человека"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Какими функциями при осуществлении своей деятельности наделены органы местного самоуправления в работе с детьми и молодёжью?',
                        'module_id'=>1,
                        'type_id'=>2,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Какими функциями при осуществлении своей деятельности наделены органы местного самоуправления в работе с детьми и молодёжью?',
                    'content'=>json_decode('[
                        {"check":true,"text":"разработку, утверждение и реализацию программ местного уровня на основе целевых государственных молодёжных программ с участием молодых граждан (молодёжных организаций), включая неорганизованную молодёжь и неформальные объединения молодёжи"},
                        {"check":true,"text":"разработку и утверждение в установленном порядке нормативного правового акта, регламентирующего порядок оказания поддержки талантливой и активной молодёжи, молодых специалистов, молодых семей и молодых граждан, находящихся в трудной жизненной ситуации"},
                        {"check":true,"text":"обеспечение условий для развития физической культуры и спорта, организацию досуга, включая предоставление помещений для реализации молодёжных программ на местном уровне"},
                        {"check":true,"text":"обеспечение поддержки молодым гражданам, осуществляющим волонтёрскую деятельность"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Жалобу в Комитет ООН по правам человека можно подать после',
                        'module_id'=>1,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Жалобу в Комитет ООН по правам человека можно подать после',
                    'content'=>json_decode('[
                        {"check":false,"text":"после обращения в суд"},
                        {"check":false,"text":"обращения уполномоченному по правам человека"},
                        {"check":false,"text":"после вынесения решения судом первой инстанции"},
                        {"check":true,"text":"после исчерпания всех доступных средств защиты внутри страны"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Какой раздел Конституции КР посвящён правам и свободам человека и гражданина?',
                        'module_id'=>1,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Какой раздел Конституции КР посвящён правам и свободам человека и гражданина?',
                    'content'=>json_decode('[
                        {"check":false,"text":"первый раздел"},
                        {"check":true,"text":"второй раздел"},
                        {"check":false,"text":"третий раздел"},
                        {"check":false,"text":"четвёртый раздел"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'В каком году была принята Конвенция о правах молодёжи ООН?',
                        'module_id'=>1,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'В каком году была принята Конвенция о правах молодёжи ООН?',
                    'content'=>json_decode('[
                        {"check":false,"text":"в 1982 году"},
                        {"check":false,"text":"в 1992 году"},
                        {"check":false,"text":"в 2002 году"},
                        {"check":true,"text":"такой конвенции не существует"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Как называется раздел Конституции КР посвящённый правам человека?',
                        'module_id'=>1,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Как называется раздел Конституции КР посвящённый правам человека?',
                    'content'=>json_decode('[
                        {"check":false,"text":"права и свободы человека"},
                        {"check":true,"text":"права и свободы человека и гражданина"},
                        {"check":false,"text":"права человека"},
                        {"check":false,"text":"права человека и гражданина"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Какая категория лиц в Кыргызской Республике относиться к молодёжи согласно законодательству?',
                        'module_id'=>1,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Какая категория лиц в Кыргызской Республике относиться к молодёжи согласно законодательству?',
                    'content'=>json_decode('[
                        {"check":false,"text":"с 16 до 28 лет"},
                        {"check":false,"text":"с 16 до 30 лет"},
                        {"check":true,"text":"с 14 до 28 лет"},
                        {"check":false,"text":"с 14 до 30 лет"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Какой нормативно-правовой акт КР определяет границы возрастных ограничений молодёжи?',
                        'module_id'=>1,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Какой нормативно-правовой акт КР определяет границы возрастных ограничений молодёжи?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Конституция КР"},
                        {"check":true,"text":"Закон КР «Об основах государственной молодёжной политики»"},
                        {"check":false,"text":"Кодекс «О детях КР»"},
                        {"check":false,"text":"Семейный кодекс КР"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'В какой международный орган могут обращаться граждане КР за защитой своих нарушенных прав?',
                        'module_id'=>1,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'В какой международный орган могут обращаться граждане КР за защитой своих нарушенных прав?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Комиссию по правам человека ООН"},
                        {"check":true,"text":"Комитет по правам человека ООН"},
                        {"check":false,"text":"Совет по правам человека ООН"},
                        {"check":false,"text":"Секретариат ООН"}
                    ]'),
                ];
            }
            Question::create($array);

    }
}
