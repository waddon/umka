<?php

use Illuminate\Database\Seeder;
use App\Module;

class Module01Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            $array = [
                'key'=>'Модуль 1. Права человека - основа прав молодёжи',
                'course_id' => 1,
                'data'=>json_decode('{
                    "introduction_id":"0",
                    "conclusion_id":"0",
                    "literature_id":"0",
                    "point_score":"1",
                    "point_one":"1",
                    "question_count":"3",
                    "time_for_passing":30,
                    "time_for_passing_one":3,
                    "max_count_passing":5,
                    "time_between_passing":1,
                    "time_between_passing_one":4
                }'),
            ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Модуль 1. Права человека - основа прав молодёжи',
                    'messages'=>json_decode('{"welcome":"<ul><li>На прохождение теста вам даётся <strong>30 минут</strong> и <strong>три попытки</strong>.</li><li>Если вы не сможете пройти тест с первой попытки, следующая будет предоставлена через <strong>24 часа</strong>.</li><li>Для успешного прохождения теста вам необходимо дать <strong>100% правильных ответов (20 правильных ответов на 20 вопросов)</strong>.</li><li><strong>Если вы не сможете пройти тест за три попытки, система автоматически заблокирует ваш доступ к системе.</strong> Пожалуйста, обратитесь к вашему куратору в Государственное агентство по делам молодёжи, физической культуры и спорта при Правительстве Кыргызской Республики.</li></ul>","success":"<div class=\"text-center\"><strong>Поздравляем с успешным прохождением теста по Модулю 1. Права человека - основа прав молодёжи</strong><br>Пожалуйста, ожидайте приглашения на прохождение <strong>Модуля 2. Административные процедуры.</strong></div>","failure":"failure text","already":"<div class=\"text-center\"><strong>Тест по Модулю 1. Права человека - основа прав молодёжи уже успешно сдан</strong><br>Пожалуйста, ожидайте приглашения на прохождение <strong>Модуля 2. Административные процедуры.</strong></div>","exhausted":"Попытки исчерпаны","wait":"Прошло недостаточно времени после окончания предыдущей попытки прохождения. Повторное прохождение теста будет доступно "}'),
                ];
            }
            Module::create($array);

    }
}
