<?php

use Illuminate\Database\Seeder;
use App\Module;

class Module06Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            $array = [
                'key'=>'Модуль 6. PR. Работа с общественностью и создание общественного мнения',
                'course_id' => 1,
                'data'=>json_decode('{
                    "introduction_id":"0",
                    "conclusion_id":"0",
                    "literature_id":"0",
                    "point_score":"1",
                    "point_one":"1",
                    "question_count":"3",
                    "time_for_passing":60,
                    "time_for_passing_one":3,
                    "max_count_passing":5,
                    "time_between_passing":1,
                    "time_between_passing_one":4
                }'),
            ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Модуль 6. PR. Работа с общественностью и создание общественного мнения',
                    'messages'=>json_decode('{"welcome":"welcome text","success":"success message","failure":"failure text"}'),
                ];
            }
            Module::create($array);

    }
}
