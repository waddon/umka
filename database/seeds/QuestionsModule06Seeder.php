<?php

use Illuminate\Database\Seeder;
use App\Question;

class QuestionsModule06Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            $array = [
                        'key'=>'Каковы социальные функции PR?',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Каковы социальные функции PR?',
                    'content'=>json_decode('[
                        {"check":true,"text":"PR гармонизируют отношения в обществе"},
                        {"check":false,"text":"PR не имеют социальных функций"},
                        {"check":false,"text":"PR – чисто политическое явление"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Как соотносятся понятия «реклама» и «PR»?',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Как соотносятся понятия «реклама» и «PR»?',
                    'content'=>json_decode('[
                        {"check":true,"text":"Они не совместимы"},
                        {"check":false,"text":"PR – вид рекламы"},
                        {"check":false,"text":"PR – разновидность менеджмента, часто использует рекламу для достижения поставленных целей"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Как соотносятся понятия «маркетинг» и «PR»?',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Как соотносятся понятия «маркетинг» и «PR»?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Они идентичны"},
                        {"check":false,"text":"Не имеют точек соприкосновения"},
                        {"check":true,"text":"Очень близки по содержанию и методам, но имеют свою специфику"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Какое высказывание о пресс-релизе верно?',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Какое высказывание о пресс-релизе верно?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Пресс-релиз направляется во все организации и СМИ"},
                        {"check":true,"text":"Пресс-релиз можно отправлять только в СМИ"},
                        {"check":false,"text":"Пресс-релиз отправляется только по почте"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Соотнесите понятия «менеджмент» и «PR»',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Соотнесите понятия «менеджмент» и «PR»',
                    'content'=>json_decode('[
                        {"check":false,"text":"Это разные, несовместимые понятия"},
                        {"check":true,"text":"PR – разновидность менеджмента"},
                        {"check":false,"text":"Это одно и то же"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Согласны ли вы, что управление общественными связями приставляет собой деятельность по формированию общественного мнения и управлению им?',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Согласны ли вы, что управление общественными связями приставляет собой деятельность по формированию общественного мнения и управлению им?',
                    'content'=>json_decode('[
                        {"check":true,"text":"Да"},
                        {"check":false,"text":"Нет"},
                        {"check":false,"text":"Необходимо добавить «по изучению общественного мнения»"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Выберите верный вариант суждения по поводу презентации:',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Выберите верный вариант суждения по поводу презентации:',
                    'content'=>json_decode('[
                        {"check":false,"text":"На презентацию могут прийти все желающие"},
                        {"check":true,"text":"Журналисты должны быть приглашены на презентацию обязательно"},
                        {"check":false,"text":"Презентация проводится в основном для важных персон, от которых зависит будущее организации"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Пресс-конференция – это мероприятие, на которое независимо от темы необходимо пригласить политиков, известных людей. Это повысит авторитет организации?',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Пресс-конференция – это мероприятие, на которое независимо от темы необходимо пригласить политиков, известных людей. Это повысит авторитет организации?',
                    'content'=>json_decode('[
                        {"check":true,"text":"Нет, это неверно, т. к. пресс-конференция – это мероприятие для журналистов. На их вопросы отвечают представители организации, которая проводит пресс-конференцию"},
                        {"check":false,"text":"Да"},
                        {"check":false,"text":"Такое возможно, это решается организаторами"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Какое направление не свойственно для PR-деятельности?',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Какое направление не свойственно для PR-деятельности?',
                    'content'=>json_decode('[
                        {"check":true,"text":"Работа с широкой общественностью"},
                        {"check":false,"text":"Работа со СМИ"},
                        {"check":false,"text":"Отношения с потребителями"},
                        {"check":false,"text":"Продвижение товаров и услуг"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Каковы правовые основы PR?',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Каковы правовые основы PR?',
                    'content'=>json_decode('[
                        {"check":true,"text":"Деятельность PR не регулируется никакими официальными законодательными актами"},
                        {"check":false,"text":"Деятельность PR регулируется Законом КР «О рекламе»"},
                        {"check":false,"text":"Деятельность PR не может регулироваться законодательно в силу её специфики"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Как формируется общественное мнение?',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Как формируется общественное мнение?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Только стихийно"},
                        {"check":false,"text":"Общественного мнения фактически не существует, это условная категория"},
                        {"check":true,"text":"Общественное мнение можно сформировать целенаправленно"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Что понимается под термином «имидж организации»?',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Что понимается под термином «имидж организации»?',
                    'content'=>json_decode('[
                        {"check":true,"text":"Имидж имеют все организации"},
                        {"check":false,"text":"Имидж имеют не все организации"},
                        {"check":false,"text":"Имидж – это условная категория, не измеряемая и не уловимая"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Выраженное отношение по какому-нибудь вопросу – это:',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Выраженное отношение по какому-нибудь вопросу – это:',
                    'content'=>json_decode('[
                        {"check":true,"text":"Мнение"},
                        {"check":false,"text":"Общественное мнение"},
                        {"check":false,"text":"Интерес"},
                        {"check":false,"text":"Потребность"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'К какому типу планирования относят PR- деятельность?',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'К какому типу планирования относят PR- деятельность?',
                    'content'=>json_decode('[
                        {"check":true,"text":"Стратегическому"},
                        {"check":false,"text":"Тактическому"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Выберете определение понятию «целевая аудитория» в PR',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Выберете определение понятию «целевая аудитория» в PR',
                    'content'=>json_decode('[
                        {"check":true,"text":"Целевые аудитории выделяются на основе технологии для каждой PR-программы"},
                        {"check":false,"text":"Целевые аудитории выделяются произвольно, по вкусу специалистов и руководителей"},
                        {"check":false,"text":"Целевая аудитория может быть только одна"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Каков главный принцип взаимодействия с внешней средой в PR?',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Каков главный принцип взаимодействия с внешней средой в PR?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Обеспечение обратной связи"},
                        {"check":false,"text":"Полнота направляемой информации"},
                        {"check":true,"text":"Регулярность информирования"},
                        {"check":false,"text":"Это сугубо творческий процесс, ничем не регламентированный"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Планирование PR-деятельности',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Планирование PR-деятельности',
                    'content'=>json_decode('[
                        {"check":false,"text":"Идентично медиапланированию"},
                        {"check":true,"text":"Происходит на основе общего планирования работы организации с учётом её стратегических задач"},
                        {"check":false,"text":"Это оперативное планирование мероприятий"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Эффективность планирования PR-деятельности',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Эффективность планирования PR-деятельности',
                    'content'=>json_decode('[
                        {"check":false,"text":"Определяется количеством мероприятий со СМИ"},
                        {"check":true,"text":"Зависит от грамотной формулировки цели и предполагаемых форм контроля"},
                        {"check":false,"text":"Трудно измеряема, как и вся деятельность PR"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'Коммуникации в PR – это:',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'Коммуникации в PR – это:',
                    'content'=>json_decode('[
                        {"check":false,"text":"Прежде всего межличностные коммуникации"},
                        {"check":true,"text":"Публичные коммуникации"},
                        {"check":false,"text":"Коммуникации через СМИ"}
                    ]'),
                ];
            }
            Question::create($array);

            $array = [
                        'key'=>'От чего зависит эффективность коммуникаций в PR?',
                        'module_id'=>6,
                        'type_id'=>1,
                        'activity'=>true
                    ];
            foreach (config()->get('app.locales') as $key => $language) {
                $array[$language] = [
                    'name'=>'От чего зависит эффективность коммуникаций в PR?',
                    'content'=>json_decode('[
                        {"check":false,"text":"Главное – полнота информации"},
                        {"check":true,"text":"Главное – регулярность информирования"},
                        {"check":false,"text":"Важно учитывать цель, особенности адресата"}
                    ]'),
                ];
            }
            Question::create($array);

    }
}
