<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Permission;
use App\Role;
use App\Role_has_permission;
use App\User;
use App\Model_has_role;

use App\Group;

use App\Page;
use App\Media;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        # создаем права и пользователелей
            Permission::create([
                        'name'=>'dashboard',
                        'guard_name'=>'web',
                    ]);
            Role::create([
                        'name'=>'Administrator',
                        'guard_name'=>'web',
                    ]);
            Role::create([
                        'name'=>'Moderator',
                        'guard_name'=>'web',
                    ]);
            Role::create([
                        'name'=>'Editor',
                        'guard_name'=>'web',
                    ]);
            Role::create([
                        'name'=>'Student',
                        'guard_name'=>'web',
                    ]);
            Role_has_permission::create([
                        'permission_id'=>1,
                        'role_id'=>1,
                    ]);
            User::create([
                        'email'=>'waddon@narod.ru',
                        'name'=>'Вадим Донець',
                        'password'=>Hash::make('killer666'),
                        'activity'=>1,
                        'category_id'=>6,
                        'position'=>'Создатель сия красотень',
                        'phone'=>'0123456789',
                    ]);
            User::create([
                        'email'=>'info@activemedia.ua',
                        'name'=>'Администратор',
                        'password'=>Hash::make('U1jWJgn4MGp7Qfl0'),
                        'activity'=>1,
                        'category_id'=>6,
                        'position'=>'Администратор',
                        'phone'=>'отсутствует',
                    ]);
            Model_has_role::create([
                        'role_id'=>1,
                        'model_id'=>1,
                        'model_type'=>'App\User',
                    ]);
            Model_has_role::create([
                        'role_id'=>1,
                        'model_id'=>2,
                        'model_type'=>'App\User',
                    ]);
            
        # Cоздаем группу пользователей
            Group::create([
                        'name'=>'Полный доступ',
                    ]);
            Group::create([
                        'name'=>'Первая учебная группа',
                    ]);
        # Cоздаем опции
            $this->call(OptionsSeeder::class);

        # Cоздаем страницы
            $this->call(PagesSeeder::class);

        # Добавляем пару картинок в библиотеку
            Media::create([
                        'name' => 'Android',
                        'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe ipsa asperiores illum. Repellat nemo veniam cumque quidem repellendus officia, maxime at accusantium velit. Aut, dignissimos.',
                        'type' => 'image',
                        'size'=>8340,
                        'mimetype'=>'image/png',
                        'url' => 'image/Android-R2D2.png',
                    ]);

        # создаем глоссарий
            $this->call(GlossarySeeder::class);

        # создаем курс
            $this->call(Course01Seeder::class);

        # модули
            $this->call(Module01Seeder::class);
            $this->call(Module02Seeder::class);
            $this->call(Module03Seeder::class);
            $this->call(Module04Seeder::class);
            $this->call(Module05Seeder::class);
            $this->call(Module06Seeder::class);

        # создаем урок
            $this->call(LessonsSeeder::class);

        # темы
            $this->call(ThemesModule01Seeder::class);
            $this->call(ThemesModule02Seeder::class);

        # тесты для модулей
            $this->call(QuestionsModule01Seeder::class);
            $this->call(QuestionsModule02Seeder::class);
            $this->call(QuestionsModule03Seeder::class);
            $this->call(QuestionsModule04Seeder::class);
            $this->call(QuestionsModule05Seeder::class);
            $this->call(QuestionsModule06Seeder::class);

    }
}
