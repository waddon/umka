<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mailings', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->integer('group_id')->unsigned();
            $table->timestamp('date_send')->nullable();
            $table->boolean('sent')->default(0);
            $table->longtext('content');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mailings');
    }
}
