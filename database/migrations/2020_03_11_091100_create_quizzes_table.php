<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizzes', function(Blueprint $table) {
            $table->increments('id');
            $table->string('key')->nullable();
            $table->timestamps();
        });

        Schema::create('quiz_translations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('quiz_id')->unsigned();
            $table->string('locale')->index();
            $table->string('name')->nullable();
            $table->longtext('content')->nullable();
            $table->longtext('data')->nullable();

            $table->unique(['quiz_id', 'locale']);
            $table->foreign('quiz_id')->references('id')->on('quizzes')->onDelete('cascade');
        });

        Schema::create('group_quiz', function (Blueprint $table) {
            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
            $table->integer('quiz_id')->unsigned();
            $table->foreign('quiz_id')->references('id')->on('quizzes')->onDelete('cascade');

            $table->primary(['group_id', 'quiz_id']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_quiz');
        Schema::dropIfExists('quiz_translations');
        Schema::dropIfExists('quizzes');
    }
}
