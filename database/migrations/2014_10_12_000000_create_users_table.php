<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pin')->nullable();
            $table->string('name')->nullable();
            $table->timestamp('birthday')->nullable();
            $table->string('passport')->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->string('position')->nullable();
            $table->string('scan')->nullable();
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->string('avatar')->nullable();
            $table->boolean('activity')->default(0);
            $table->boolean('new_letter')->default(0);
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('last_action_at')->nullable();
            $table->string('password');
            $table->longtext('data')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
