<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('themes', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('lesson_id')->unsigned();
            $table->integer('module_id')->unsigned()->default(0);
            $table->string('key')->nullable();
            $table->boolean('activity')->default(0);
            $table->integer('order')->unsigned()->default(0);
            $table->longtext('data')->nullable();
            $table->timestamps();

            $table->foreign('lesson_id')->references('id')->on('lessons')->onDelete('cascade');
        });

        Schema::create('theme_translations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('theme_id')->unsigned();
            $table->string('locale')->index();
            $table->string('name')->nullable();
            $table->longtext('content')->nullable();

            $table->unique(['theme_id', 'locale']);
            $table->foreign('theme_id')->references('id')->on('themes')->onDelete('cascade');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theme_translations');
        Schema::dropIfExists('themes');
    }
}
