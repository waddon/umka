<?php
/*
 * Ключ: краткое название виджета для обращения к нему из шаблона
 * Значение: название класса виджета с пространством имен
 */
return [
    'groups' => 'App\Widgets\GroupsWidget',
    'modules' => 'App\Widgets\ModulesWidget',
    'users' => 'App\Widgets\UsersWidget',
];
