/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./custom */ "./resources/js/custom.js");

/***/ }),

/***/ "./resources/js/custom.js":
/*!********************************!*\
  !*** ./resources/js/custom.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

$('.nav-link.active').parents('.nav-item-submenu').addClass('nav-item-open');
$('.nav-link.active').parents('.nav-group-sub').show();
$('.card-sidebar-mobile').show();

function renderTemplate(name, data) {
  var template = document.getElementById(name).innerHTML;

  for (var property in data) {
    if (data.hasOwnProperty(property)) {
      var search = new RegExp('{' + property + '}', 'g');
      template = template.replace(search, data[property]);
    }
  }

  return template;
}

$.fn.singleDatePicker = function () {
  $(this).on("apply.daterangepicker", function (e, picker) {
    picker.element.val(picker.startDate.format(picker.locale.format));
  });
  return $(this).daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    autoUpdateInput: false,
    ranges: {
      'Сегодня': [moment(), moment()],
      'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
      'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
      'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
      'Прошедший месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    locale: {
      applyLabel: 'Вперед',
      cancelLabel: 'Отмена',
      startLabel: 'Начальная дата',
      endLabel: 'Конечная дата',
      customRangeLabel: 'Выбрать дату',
      daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
      monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
      firstDay: 1,
      format: 'DD.MM.YYYY'
    }
  });
};

$(".daterange-single").singleDatePicker();
/* uploader */

$('.form-control-uniform').uniform({
  fileDefaultHtml: 'Выберите файл',
  fileButtonHtml: 'Выберите &hellip;'
});
/* reload table after change data in search panel*/

$('div.search-panel').on('change', 'select', function (e) {
  tableActive.ajax.reload(null, false);
});
/* assists */

$('.assist-up').click(function (e) {
  e.preventDefault();
  var body = $('body');
  var newclass;

  if (body.hasClass('assist-medium')) {
    body.removeClass('assist-medium');
    newclass = 'assist-large';
  } else if (body.hasClass('assist-large')) {
    body.removeClass('assist-large');
    newclass = 'assist-xlarge';
  } else if (!body.hasClass('assist-xlarge')) {
    newclass = 'assist-medium';
  }

  body.addClass(newclass);
  document.cookie = "assist_size=" + newclass + "; path=/; max-age=604800";
});
$('.assist-down').click(function (e) {
  e.preventDefault();
  var body = $('body');
  var newclass = '';

  if (body.hasClass('assist-medium')) {
    body.removeClass('assist-medium');
  } else if (body.hasClass('assist-large')) {
    body.removeClass('assist-large');
    newclass = 'assist-medium';
  } else if (body.hasClass('assist-xlarge')) {
    body.removeClass('assist-xlarge');
    newclass = 'assist-large';
  }

  body.addClass(newclass);
  document.cookie = "assist_size=" + newclass + "; path=/; max-age=604800";
});
$('.assist-color').click(function (e) {
  e.preventDefault();
  var body = $('body');
  var newclass = '';

  if (body.hasClass('assist-white')) {
    body.removeClass('assist-white');
  } else {
    newclass = 'assist-white';
  }

  body.addClass(newclass);
  document.cookie = "assist_color=" + newclass + "; path=/; max-age=604800";
});
$('body').addClass(getCookie('assist_size')).addClass(getCookie('assist_color'));

function getCookie(name) {
  var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

$("#print").click(function (e) {
  e.preventDefault();
  $('#content-for-print').printMe();
});
$("#excel-export").click(function () {
  excel = new ExcelGen({
    "src_id": "stat_table",
    "show_header": true
  });
  excel.generate();
});

/***/ }),

/***/ "./resources/sass/admin.scss":
/*!***********************************!*\
  !*** ./resources/sass/admin.scss ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/sass/app.scss":
/*!*********************************!*\
  !*** ./resources/sass/app.scss ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*****************************************************************************************!*\
  !*** multi ./resources/js/app.js ./resources/sass/app.scss ./resources/sass/admin.scss ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! C:\OSPanel\domains\umka.local\resources\js\app.js */"./resources/js/app.js");
__webpack_require__(/*! C:\OSPanel\domains\umka.local\resources\sass\app.scss */"./resources/sass/app.scss");
module.exports = __webpack_require__(/*! C:\OSPanel\domains\umka.local\resources\sass\admin.scss */"./resources/sass/admin.scss");


/***/ })

/******/ });