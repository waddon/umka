(function( $ ) {

    var methods = {
        init : function(options) {

            return this.each(function() {        

                var builderId = 'builder-'+ Date.now();
                var ths = $(this).wrap('<div class="hexa-builder-wrap" id="'+builderId+'">')
                    .hide();
                var parent = ths.parent()
                    .append('<div class="hexa-blocks">');
                var blocks = parent.find('.hexa-blocks');
                var content = $( '<div>' + ths.val() + '</div>' );

                content.children().each(function(){
                    element = $(this).wrap('<p/>').parent();
                    var block = $('<div class="hexa-block"><div class="hexa-block-move"><i class="icon-grid"></i></a></div><div class="hexa-block-content-wrap">' + element.html() + '</div><div class="hexa-block-tool">' + renderTemplate('template-dropdown-menu',{}) + '</div></div>');

                    blocks.append(block);
                });

                emailBodyConfig.selector = '.hexa-builder-wrap[id='+builderId+'] .hexa-block-content-text';
                tinymce.init(emailBodyConfig);

            // Добавление кнопок
                parent.append('<div class="hexa-builder-buttons text-center mt-3"><button type="button" class="btn btn-success add-text-block" onclick="event.preventDefault();"><i class="icon-pencil5 icon-2x"></i></button> <button type="button" class="btn btn-success add-media-block"><i class="icon-film4 icon-2x"></i></button> <button type="button" class="btn btn-success add-video-block"><i class="icon-clapboard-play icon-2x"></i></button> <button type="button" class="btn btn-success add-accordion-block"><i class="icon-menu3 icon-2x"></i></button></div>');

            // Добавление редактируемости к заголовкам аккордеона
                parent.find('.card-title a').attr('contenteditable','true');
                parent.find('.hexa-block-content-text').attr('contenteditable','true');

            // Сортировка блоков
                parent.find('.hexa-blocks').sortable({
                    handle: ".hexa-block-move",
                    tolerance: 'pointer',
                    opacity: 0.6,
                    placeholder: 'sortable-placeholder',
                    start: function(e, ui){
                        ui.placeholder.height(ui.item.outerHeight());
                    }
                });
            // Удаление блока
                parent        
                    .on('click', '.hexa-block-remove', function(e) {
                        e.stopPropagation();
                        e.preventDefault();
                        $(this).closest('.hexa-block').remove();
                    });
            // Добавление текстового блока
                parent        
                    .on('click', '.add-text-block', function(e) {
                        e.stopPropagation();
                        e.preventDefault();
                        var id = 'block-'+Date.now();
                        var html = renderTemplate('template-text', {
                            blockId : id,
                        });
                        emailBodyConfig.selector = '.hexa-block[id='+id+'] .hexa-block-content-text';
                        parent.children('.hexa-blocks').append( html );
                        tinymce.init(emailBodyConfig);
                    });
            // Добавление медиа блока из библиотеки
                parent        
                    .on('click', '.add-media-block', function(e) {
                        e.stopPropagation();
                        e.preventDefault();
                        bootbox.dialog({
                            title: 'Please select video from media library',
                            message:'<form action="">'+
                                        '<select class="form-control">'+
                                            (typeof mediaOptionsList !== "undefined" ? mediaOptionsList : '---')+
                                        '</select>'+
                                    '</form>',
                            buttons: {
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn-primary',
                                    callback: function () {
                                        console.log($(this).attr('class'));
                                        var url = $(this).find('select').first().val();
                                        //var url = $('select').val();
                                        var type = $(this).find('option:selected').attr('data-type');
                                        // var type = $('option:selected').attr('data-type');
                                        var html = renderTemplate('template-media', {
                                            url : url,
                                            type : type,
                                        });
                                        parent.children('.hexa-blocks').append( html );
                                    }
                                },
                                cancel: {
                                    label: 'Cancel',
                                    className: 'btn-link'
                                }
                            }
                        });
                    });
            // добавление EMBED блока
                parent        
                    .on('click', '.add-video-block', function(e) {
                        e.stopPropagation();
                        e.preventDefault();
                        bootbox.prompt({
                            title: 'Please enter embed url',
                            buttons: {
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn-primary'
                                },
                                cancel: {
                                    label: 'Cancel',
                                    className: 'btn-link'
                                }
                            },
                            callback: function (result) {
                                if (result === null) {                                             
                                } else {
                                    var html = renderTemplate('template-video', {
                                        url : result,
                                    });
                                    parent.children('.hexa-blocks').append( html );
                                }
                            }
                        });
                    });
            // добавление аккордеона
                parent        
                    .on('click', '.add-accordion-block', function(e) {
                        e.stopPropagation();
                        e.preventDefault();
                        var id = 'block-'+Date.now();
                        var html = renderTemplate('template-accordion', {
                            blockId : id,
                            id: 'ac-'+ Date.now(),
                        });
                        emailBodyConfig.selector = '.hexa-block[id='+id+'] .hexa-block-content-text';
                        parent.children('.hexa-blocks').append( html );
                        tinymce.init(emailBodyConfig);
                    });

            // Добавление пустого аккордиона
                parent        
                    .on('click', '.hexa-block-accordion-element', function(e) {
                        e.stopPropagation();
                        e.preventDefault();
                        var acc = $(this).closest('.hexa-block').find('.hexa-block-content-accordion').first();
                        var id = acc.attr('id');
                        var no = parseInt(acc.attr('data-child')) + 1;
                        var html = renderTemplate('template-accordion-element', {
                            id: id,
                            no: no,
                        });
                        acc.attr('data-child', no);
                        emailBodyConfig.selector = 'div[id='+id+'-'+no+'] .hexa-block-content-text';
                        acc.append( html );
                        tinymce.init(emailBodyConfig);
                    });
            // Удаление элемента аккордиона
                parent        
                    .on('click', '.btn-remove-accordion-element', function(e) {
                        e.stopPropagation();
                        e.preventDefault();
                        $(this).closest('.card').remove();
                    });

            // Warning Block
                parent        
                    .on('click', '.hexa-block-warning', function(e) {
                        e.stopPropagation();
                        e.preventDefault();
                        var icon = $(this).attr('data-icon');
                        var block = $(this).closest('.hexa-block').find('.hexa-block-content');
                        warningRemoveClass($(this).closest('.hexa-block').find('.hexa-block-content'));
                        block.addClass( "warning-box" );
                        if (icon == 'text') block.addClass( "warning-box-text" );
                        if (icon == 'important') block.addClass( "warning-box-important" );
                        if (icon == 'webinar') block.addClass( "warning-box-webinar" );
                        if (icon == 'video') block.addClass( "warning-box-video" );
                        if (icon == 'tasks') block.addClass( "warning-box-tasks" );
                        if (icon == 'history') block.addClass( "warning-box-history" );
                        if (icon == 'pictures') block.addClass( "warning-box-pictures" );
                        if (icon == 'tools') block.addClass( "warning-box-tools" );
                        if (icon == 'literature') block.addClass( "warning-box-literature" );
                        if (icon == 'selfeducation') block.addClass( "warning-box-selfeducation" );
                        if (icon == 'fulltime') block.addClass( "warning-box-fulltime" );
                        if (icon == 'example') block.addClass( "warning-box-example" );
                        if (icon == 'expert') block.addClass( "warning-box-expert" );
                        if (icon == 'quotes') block.addClass( "warning-box-quotes" );
                        if (icon == 'checklist') block.addClass( "warning-box-checklist" );
                        if (icon == 'exam') block.addClass( "warning-box-exam" );
                    });

            // очистка Warning Block
                parent        
                    .on('click', '.hexa-block-warning-clear', function(e) {
                        e.stopPropagation();
                        e.preventDefault();
                        warningRemoveClass($(this).closest('.hexa-block').find('.hexa-block-content'));
                        $(this).closest('.hexa-block').find('.hexa-block-content').removeClass( "warning-box" );        
                    });

            // функция очистки всех Warning
                function warningRemoveClass(element) {
                    console.log('---');
                    element.removeClass( "warning-box-text warning-box-important warning-box-webinar warning-box-video warning-box-tasks warning-box-history warning-box-pictures warning-box-tools warning-box-literature warning-box-selfeducation warning-box-fulltime warning-box-example warning-box-expert warning-box-quotes warning-box-checklist warning-box-exam" );
                }

            // Создание темплейта
                function renderTemplate(name, data) {
                    var template = document.getElementById(name).innerHTML;
                    for (var property in data) {
                        if (data.hasOwnProperty(property)) {
                            var search = new RegExp('{' + property + '}', 'g');
                            template = template.replace(search, data[property]);
                        }
                    }
                    return template;
                }

            });

        },
        update : function( content) {
            return this.each(function() {
                var ths = $(this);
                var parent = ths.siblings('.hexa-blocks');
                var result = '';
                parent.find('.hexa-block-content-text').removeAttr('contenteditable');
                parent.find('.card-title a').removeAttr('contenteditable');
                parent.find('.hexa-block-content-wrap').each(function(){
                    $(this).find('input').remove();
                    result += $(this).html();
                });
                ths.val(result);
                parent.find('.hexa-block-content-text').attr('contenteditable','true');
                parent.find('.card-title a').attr('contenteditable','true');
                console.log(result);
            });
        }
    };




    $.fn.hexabuilder = function( methodOrOptions ) {
  
        if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on jquery.hexabuilder' );
        }

    };
})(jQuery);


