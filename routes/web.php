<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('setlocale/{lang}', function ($lang) {

    $referer = Redirect::back()->getTargetUrl();
    $parse_url = parse_url($referer, PHP_URL_PATH);
    $segments = explode('/', $parse_url);
    if (in_array($segments[1], config()->get('app.locales')) || $segments[1]=='') {
        unset($segments[1]);
    } 
    if ($lang != config()->get('app.locale')){ 
        array_splice($segments, 1, 0, $lang); 
    }
    $url = Request::root().implode("/", $segments);
    if(parse_url($referer, PHP_URL_QUERY)){    
        $url = $url.'?'. parse_url($referer, PHP_URL_QUERY);
    }
    return redirect($url);
})->name('setlocale');


Route::group(['prefix' => App\Http\Middleware\LocaleMiddleware::getLocale(), 'middleware'=>'offline' ], function(){

    Route::get('/', ['as'=>'home', 'uses'=>'GeneralController@index']);

    Route::group(['prefix' => 'dashboard', 'namespace' => 'Dashboard', 'middleware' => ['auth', 'isAdmin']], function() {
        Route::get('/', ['as' => 'dashboard', 'uses' => 'AdminController@index']);
        Route::match(['get', 'post'],'users/message/{id}',    ['as'=>'users.message', 'uses'=>'UserController@message']);
        Route::get('users/download/{id}', ['as' => 'users.download', 'uses' => 'UserController@download']);
        Route::get('users/quiz/{id}', ['as' => 'users.quiz', 'uses' => 'UserController@quiz']);
        Route::get('users/removetestsprogress/{id}', ['as' => 'users.removetestsprogress', 'uses' => 'UserController@removeTestsProgress']);
        Route::resource('users' , 'UserController');
        Route::post('groups/detail', ['as' => 'groups.detail', 'uses' => 'GroupController@detail']);
        Route::get('groups/stat/{id}', ['as' => 'groups.stat', 'uses' => 'GroupController@stat']);
        Route::get('groups/webquestions/{id}', ['as' => 'groups.webquestions', 'uses' => 'GroupController@webquestions']);
        Route::resource('groups', 'GroupController');
        Route::resource('roles' , 'RoleController', ['except' => 'show']);
        Route::resource('permissions', 'PermissionController', ['except' => 'show']);
        Route::resource('pages', 'PageController');

        Route::post( 'medias/upload', ['as'=>'medias.upload', 'uses'=>'MediaController@upload'] );
        Route::resource('medias', 'MediaController');
        Route::resource('glossaries', 'GlossaryController');
        Route::resource('mailings', 'MailingController');

        Route::resource('courses', 'CourseController');
        Route::resource('modules', 'ModuleController');
        Route::resource('lessons', 'LessonController');
        Route::resource('themes', 'ThemeController');
        Route::resource('questions', 'QuestionController');
        Route::resource('quizzes', 'QuizController');

        Route::post('/run', ['as' => 'run', 'uses' => 'AdminController@run']);
        Route::match(['get', 'post'],'/settings',  ['as'=>'settings', 'uses'=>'AdminController@settings']);
        Route::match(['get', 'post'],'/tracks',    ['as'=>'tracks',   'uses'=>'AdminController@tracks']);
        Route::post('/tracklist', ['as' => 'tracklist', 'uses' => 'AdminController@tracklist']);
        Route::get('/phpinfo', ['as' => 'phpinfo', 'uses' => 'AdminController@phpinfo']);
        // Route::get('/offline', ['as' => 'offline', 'uses' => 'AdminController@offline']);
        // Route::get('/offline/offlineCopy', ['as' => 'offlineCopy', 'uses' => 'AdminController@offlineCopy']);
    });

    # роуты для студентов

        Route::match(['get', 'post'], '/profile', ['as' => 'profile', 'middleware' => ['auth'],'uses' => 'UserController@profile']);
        Route::match(['get', 'post'], '/message', ['as' => 'message', 'middleware' => ['auth'],'uses' => 'UserController@message']);

        Route::group(['middleware' => ['auth', 'isActive', 'activity']], function() {
            Route::get('/module/{id}', ['as' => 'moduleshow', 'middleware' => ['isOpenModule'],'uses' => 'UserController@moduleshow']);
            Route::get('/theme/{id}', ['as' => 'themeshow', 'middleware' => ['isOpenTheme'],'uses' => 'UserController@themeshow']);
            Route::post('/open-next-theme', ['as' => 'opennexttheme', 'uses' => 'UserController@openNextTheme']);
            Route::get('/exam/{id}', ['as' => 'examshow', 'middleware' => [],'uses' => 'UserController@examshow']);
            Route::post('/examcheck/{id}', ['as' => 'examcheck', 'middleware' => [],'uses' => 'UserController@examcheck']);
            Route::get('/test/{id}', ['as' => 'testshow', 'middleware' => ['isOpenModule'],'uses' => 'UserController@testshow']);
            Route::post('/testcheck/{id}', ['as' => 'testcheck', 'middleware' => ['isOpenModule'],'uses' => 'UserController@testcheck']);
            Route::match(['get', 'post'], '/webquestions/{id?}', ['as' => 'webquestions', 'middleware' => [],'uses' => 'UserController@webquestions']);
            Route::match(['get', 'post'], '/homework/{id?}', ['as' => 'homework', 'middleware' => [],'uses' => 'UserController@homework']);
            Route::match(['get', 'post'], '/quiz/{id?}', ['as' => 'quizshow', 'middleware' => [],'uses' => 'UserController@quizshow']);
            Route::get('/pdf/{id}', ['as' => 'pdf', 'uses' => 'UserController@pdf']);

        });

        Route::post('/pushcounter', ['as' => 'pushcounter', 'uses' => 'UserController@pushcounter']);

    # регистрации и авторизации

        Auth::routes();
        Route::post( '/register', ['as'=>'register', 'uses'=>'GeneralController@postRegister'] );
        Route::post( '/logout',  ['as'=>'logout', 'uses'=>'GeneralController@logout'] );

    # различные страницы

        Route::get('/glossary/{character?}', ['as' => 'glossary', 'uses' => 'GeneralController@glossary']);
        Route::group(['prefix' => 'page'], function() {
            Route::get('/{page_slug}', ['as'=>'page', 'uses'=>'GeneralController@pageshow']);
        });
});

