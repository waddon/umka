<?php

return [

    'Remember Me' => 'Запомнить меня',

    'AdminController-run'            => 'Запуск модуля <strong>«:moduleName»</strong> для группы <strong>«:groupName»</strong> с <strong>:date1</strong> до <strong>:date2</strong>',
    'AdminController-settings'       => 'Сохранение настроек',
    'AdminController-tracks'         => 'Очистка журнала',

    'UserController-index'           => 'Открытие списка пользователей',
    'UserController-create'          => 'Открытие формы для создания нового пользователя',
    'UserController-store'           => 'Создание нового пользователя <strong>«:name»</strong> (id=«:id»)',
    'UserController-show'            => 'Авторизация в качестве пользователя <strong>«:name»</strong> (id=«:id»)',
    'UserController-edit'            => 'Открытие формы для изменения данных о пользователе <strong>«:name»</strong> (id=«:id»)',
    'UserController-update'          => 'Сохранеиие внесенных изменений в данные о пользователе <strong>«:name»</strong> (id=«:id»)',
    'UserController-destroy'         => 'Удаление пользователя <strong>«:name»</strong> (id=«:id»)',
    'UserController-setactivity'     => 'Изменение активности пользователя <strong>«:name»</strong> (id=«:id»)',
    'UserController-message'         => 'Отправка сообщения пользователю <strong>«:name»</strong> (id=«:id»)',

    'GroupController-index'          => 'Открытие списка групп',
    'GroupController-create'         => 'Открытие формы для создания новой группы',
    'GroupController-store'          => 'Создание новой группы <strong>«:name»</strong> (id=«:id»)',
    'GroupController-show'           => 'Детальный просмотр группы <strong>«:name»</strong> (id=«:id»)',
    'GroupController-edit'           => 'Открытие формы для изменения данных о группе <strong>«:name»</strong> (id=«:id»)',
    'GroupController-update'         => 'Сохранеиие внесенных изменений в данные о группе <strong>«:name»</strong> (id=«:id»)',
    'GroupController-destroy'        => 'Удаление группы <strong>«:name»</strong> (id=«:id»)',

    'PageController-index'           => 'Открытие списка страниц',
    'PageController-create'          => 'Открытие формы для создания новой страницы',
    'PageController-store'           => 'Создание новой страницы <strong>«:name»</strong> (id=«:id»)',
    'PageController-show'            => 'Просмотр страницы <strong>«:name»</strong> (id=«:id»)',
    'PageController-edit'            => 'Открытие формы для изменения страницы <strong>«:name»</strong> (id=«:id»)',
    'PageController-update'          => 'Сохранеиие внесенных изменений для страницы <strong>«:name»</strong> (id=«:id»)',
    'PageController-destroy'         => 'Удаление страницы <strong>«:name»</strong> (id=«:id»)',

    'MediaController-index'          => 'Открытие списка медиафайлов',
    'MediaController-update'         => 'Сохранеиие внесенных изменений для медиафайла <strong>«:name»</strong> (id=«:id»)',
    'MediaController-destroy'        => 'Удаление медиафайла <strong>«:name»</strong> (id=«:id»)',
    'MediaController-upload'         => 'На сервер загружен медиафайл <strong>«:name»</strong> (id=«:id»)',

    'GlossaryController-index'       => 'Открытие списка элементов глоссария',
    'GlossaryController-create'      => 'Открытие формы для создания нового элемента глоссария',
    'GlossaryController-store'       => 'Создание нового элемента глоссария <strong>«:name»</strong> (id=«:id»)',
    'GlossaryController-edit'        => 'Открытие формы для изменения элемента глоссария <strong>«:name»</strong> (id=«:id»)',
    'GlossaryController-update'      => 'Сохранеиие внесенных изменений для элемента глоссария <strong>«:name»</strong> (id=«:id»)',
    'GlossaryController-destroy'     => 'Удаление элемента глоссария <strong>«:name»</strong> (id=«:id»)',
    'GlossaryController-setactivity' => 'Изменение активности элемента глоссария <strong>«:name»</strong> (id=«:id»)',

    'CourseController-index'         => 'Открытие списка курсов',
    'CourseController-create'        => 'Открытие формы для создания нового курса',
    'CourseController-store'         => 'Создание нового курса <strong>«:name»</strong> (id=«:id»)',
    'CourseController-show'          => 'Просмотр курса <strong>«:name»</strong> (id=«:id»)',
    'CourseController-edit'          => 'Открытие формы для изменения параметров курса <strong>«:name»</strong> (id=«:id»)',
    'CourseController-update'        => 'Сохранеиие внесенных изменений в параметры курса <strong>«:name»</strong> (id=«:id»)',
    'CourseController-destroy'       => 'Удаление курса <strong>«:name»</strong> (id=«:id»)',

    'ModuleController-index'         => 'Открытие списка модулей',
    'ModuleController-create'        => 'Открытие формы для создания нового модуля',
    'ModuleController-store'         => 'Создание нового модуля <strong>«:name»</strong> (id=«:id»)',
    'ModuleController-show'          => 'Просмотр модуля <strong>«:name»</strong> (id=«:id»)',
    'ModuleController-edit'          => 'Открытие формы для изменения модуля <strong>«:name»</strong> (id=«:id»)',
    'ModuleController-update'        => 'Сохранеиие внесенных изменений в данные о модуле <strong>«:name»</strong> (id=«:id»)',
    'ModuleController-destroy'       => 'Удаление модуля <strong>«:name»</strong> (id=«:id»)',

    'ThemeController-index'          => 'Открытие списка тем',
    'ThemeController-create'         => 'Открытие формы для создания новой темы',
    'ThemeController-store'          => 'Создание новой темы <strong>«:name»</strong> (id=«:id»)',
    'ThemeController-show'           => 'Просмотр темы <strong>«:name»</strong> (id=«:id»)',
    'ThemeController-edit'           => 'Открытие формы для изменения в тему <strong>«:name»</strong> (id=«:id»)',
    'ThemeController-update'         => 'Сохранеиие внесенных изменений в тему <strong>«:name»</strong> (id=«:id»)',
    'ThemeController-destroy'        => 'Удаление темы <strong>«:name»</strong> (id=«:id»)',
    'ThemeController-moveup'         => 'Передвижение темы <strong>«:name»</strong> (id=«:id») ближе к началу',
    'ThemeController-movedown'       => 'Передвижение темы <strong>«:name»</strong> (id=«:id») дальше от начала',
    'ThemeController-setactivity'    => 'Изменение активности темы <strong>«:name»</strong> (id=«:id»)',

    'QuestionController-index'       => 'Открытие списка вопросов',
    'QuestionController-create'      => 'Открытие формы для создания нового вопроса',
    'QuestionController-store'       => 'Создание нового вопроса <strong>«:name»</strong> (id=«:id»)',
    'QuestionController-show'        => 'Просмотр вопроса <strong>«:name»</strong> (id=«:id»)',
    'QuestionController-edit'        => 'Открытие формы для изменения в вопрос <strong>«:name»</strong> (id=«:id»)',
    'QuestionController-update'      => 'Сохранеиие внесенных изменений в вопрос <strong>«:name»</strong> (id=«:id»)',
    'QuestionController-destroy'     => 'Удаление вопроса <strong>«:name»</strong> (id=«:id»)',
    'QuestionController-setactivity' => 'Изменение активности вопроса <strong>«:name»</strong> (id=«:id»)',

    'RoleController-index'           => 'Открытие списка ролей',
    'RoleController-create'          => 'Открытие формы для создания новой роли',
    'RoleController-store'           => 'Создание новой роли <strong>«:name»</strong> (id=«:id»)',
    'RoleController-edit'            => 'Открытие формы для изменения в роль <strong>«:name»</strong> (id=«:id»)',
    'RoleController-update'          => 'Сохранеиие внесенных изменений в роль <strong>«:name»</strong> (id=«:id»)',
    'RoleController-destroy'         => 'Удаление роли <strong>«:name»</strong> (id=«:id»)',

    'PermissionController-index'     => 'Открытие списка разрешений',
    'PermissionController-create'    => 'Открытие формы для создания нового разрешения',
    'PermissionController-store'     => 'Создание нового разрешения <strong>«:name»</strong> (id=«:id»)',
    'PermissionController-edit'      => 'Открытие формы для изменения разрешения <strong>«:name»</strong> (id=«:id»)',
    'PermissionController-update'    => 'Сохранеиие внесенных изменений в разрешение <strong>«:name»</strong> (id=«:id»)',
    'PermissionController-destroy'   => 'Удаление разрешения <strong>«:name»</strong> (id=«:id»)',

    'UserController-moduleshow'      => 'Просмотр модуля <strong>«:name»</strong> (id=«:id»)',
    'UserController-themeshow'       => 'Просмотр темы <strong>«:name»</strong> (id=«:id»)',
    'UserController-testshow'        => 'Прохождение теста по модулю <strong>«:name»</strong> (id=«:id»)',
    'UserController-examshow'        => 'Сдача экзамена по курсу <strong>«:name»</strong> (id=«:id»)',

    'CheckModules-checkModules'      => 'Закрыто модулей - <strong>:modulesStopCount</strong>, уведомлено пользователей - <strong>:modulesStopMails</strong>, заблокировано пользователей - <strong>:modulesStopUsersBlocked</strong>. Предполагается закрытие <strong>:modulesBeforeCount</strong> модулей в ближайшее время. Проинформировано <strong>:modulesBeforeMails</strong> пользователей, не прошедших данные модули.',
    'CheckUsers-checkUsers'          => 'Заблокировано <strong>:usersBlocked</strong> пользователей из-за превышения предела неактивности',

    /* Mails */
    'registerTitle'   => 'Уважаемый(-ая) :user',
    'registerContent' => 'Вы успешно зарегистрированы в системе «Диджитал УМК@». Для входа в систему импользуйте следующие реквизиты:',
    'runModuleMessage' => 'Модуль <strong>«:module »</strong> успешно запущен. Все материалы по данному модулю будут доступны для пользователей с <span style="font-style:italic;text-decoration:underline;">:date_start</span> до <span style="font-style:italic;text-decoration:underline;">:date_finish</span>.',

    /* Cron */
    'process-busy'      => 'Процесс :name не запущен. Работает ранее запущенный процесс.',
    'process-started'   => 'Процесс :name запущен',
    'process-finished'  => 'Работа процесса :name завершена',

];
