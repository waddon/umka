<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom Button titles
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom messages for views using the
    | convention "project.<name>" to name the lines. This makes it quick to
    | specify a specific custom language line for a views.
    |
    */

    'btn-cancel' => 'Отмена',

    /*
    |--------------------------------------------------------------------------
    | Language Names
    |--------------------------------------------------------------------------
    */

    'lang' => [
        'ru' => 'Русский',
        'en' => 'English',
        'ky' => 'Кыргызча‎',
    ],

];
