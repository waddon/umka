$('.nav-link.active').parents('.nav-item-submenu').addClass('nav-item-open');
$('.nav-link.active').parents('.nav-group-sub').show();
$('.card-sidebar-mobile').show();

function renderTemplate(name, data) {
    var template = document.getElementById(name).innerHTML;
 
    for (var property in data) {
        if (data.hasOwnProperty(property)) {
            var search = new RegExp('{' + property + '}', 'g');
            template = template.replace(search, data[property]);
        }
    }
    return template;
}


    $.fn.singleDatePicker = function() {
        $(this).on("apply.daterangepicker", function(e, picker) {
            picker.element.val(picker.startDate.format(picker.locale.format));
        });
        return $(this).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            ranges: {
                'Сегодня': [moment(), moment()],
                'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
                'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
                'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
                'Прошедший месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                applyLabel: 'Вперед',
                cancelLabel: 'Отмена',
                startLabel: 'Начальная дата',
                endLabel: 'Конечная дата',
                customRangeLabel: 'Выбрать дату',
                daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт','Сб'],
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                firstDay: 1,
                format: 'DD.MM.YYYY'
            }
        });
    };

$(".daterange-single").singleDatePicker();

    /* uploader */
    $('.form-control-uniform').uniform({
        fileDefaultHtml: 'Выберите файл',
        fileButtonHtml: 'Выберите &hellip;'
    });


/* reload table after change data in search panel*/
$('div.search-panel').on('change','select',function(e){
    tableActive.ajax.reload(null, false);
})

/* assists */
$('.assist-up').click(function(e){
    e.preventDefault();
    var body = $('body');
    var newclass;
    if(body.hasClass('assist-medium')) {
        body.removeClass('assist-medium');
        newclass = 'assist-large';
    } else if (body.hasClass('assist-large')){
        body.removeClass('assist-large');
        newclass = 'assist-xlarge';
    } else if (!body.hasClass('assist-xlarge')){
        newclass = 'assist-medium';
    }
    body.addClass(newclass);
    document.cookie = "assist_size="+newclass+"; path=/; max-age=604800";
});
$('.assist-down').click(function(e){
    e.preventDefault();
    var body = $('body');
    var newclass = '';
    if(body.hasClass('assist-medium')) {
        body.removeClass('assist-medium');
    } else if (body.hasClass('assist-large')){
        body.removeClass('assist-large');
        newclass = 'assist-medium';
    } else if (body.hasClass('assist-xlarge')){
        body.removeClass('assist-xlarge');
        newclass = 'assist-large';
    }
    body.addClass(newclass);
    document.cookie = "assist_size="+newclass+"; path=/; max-age=604800";
});
$('.assist-color').click(function(e){
    e.preventDefault();
    var body = $('body');
    var newclass = '';
    if(body.hasClass('assist-white')) {
        body.removeClass('assist-white');
    } else {
        newclass = 'assist-white';
    }
    body.addClass(newclass);
    document.cookie = "assist_color="+newclass+"; path=/; max-age=604800";
});

$('body').addClass(getCookie('assist_size')).addClass(getCookie('assist_color'));

function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

$("#print").click(function(e){
    e.preventDefault();
    $('#content-for-print').printMe();
});

$("#excel-export").click(function(){
    excel = new ExcelGen({
        "src_id": "stat_table",
        "show_header": true
    });
    excel.generate();
});
