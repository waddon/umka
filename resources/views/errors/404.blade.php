
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Forbidden</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <!-- Styles -->
<style>
body {
    background-color: #f5f5f5;
    min-height: 100vh;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    -ms-flex: 1;
    flex: 1;
}
.content {
    padding: 1.25rem 1.25rem;
    -ms-flex-positive: 1;
    flex-grow: 1;
}
.page-content {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-positive: 1;
    flex-grow: 1;
}
.content-wrapper {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    -ms-flex: 1;
    flex: 1;
    overflow: auto;
}
.flex-fill {
    -ms-flex: 1 1 auto!important;
    flex: 1 1 auto!important;
}
@media (min-width: 576px){
    .error-title {
        font-size: 12.5rem;
    }    
}
.error-title {
    color: #fff;
    font-size: 8.125rem;
    line-height: 1;
    margin-bottom: 2.5rem;
    font-weight: 300;
    text-stroke: 1px transparent;
    display: block;
    text-shadow: 0 1px 0 #ccc, 0 2px 0 #c9c9c9, 0 3px 0 #bbb, 0 4px 0 #b9b9b9, 0 5px 0 #aaa, 0 6px 1px rgba(0,0,0,.1), 0 0 5px rgba(0,0,0,.1), 0 1px 3px rgba(0,0,0,.3), 0 3px 5px rgba(0,0,0,.2), 0 5px 10px rgba(0,0,0,.25), 0 10px 10px rgba(0,0,0,.2), 0 20px 20px rgba(0,0,0,.15);
}

</style>
</head>
<body>

    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">

                <!-- Container -->
                <div class="flex-fill">

                    <!-- Error title -->
                    <div class="text-center mb-3">
                        <h1 class="error-title">404</h1>
                        <h5 class="mb-5">@lang('Oops, an error has occurred. Page Not Found!')</h5>
                        <button class="btn btn-primary" onclick="window.history.back();">@lang('Back to Previous Page')</button>
                    </div>
                    <!-- /error title -->

                </div>
                <!-- /container -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
</body>
</html>
