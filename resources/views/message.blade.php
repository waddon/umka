@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body content-area">
            @if(isset($data->icon))
                <div class="text-center mb-3">
                    {!! $data->icon !!}
                </div>
            @endif
            @if(isset($data->message))
                <div class="mb-3">
                    {!! $data->message !!}
                </div>
            @endif
            @if(isset($data->button_url))
                <div class="text-center mb-3">
                    <a class="btn btn-success" href="{{$data->button_url}}">@lang('Homework')</a>
                </div>
            @endif
        </div>
    </div>
@endsection
