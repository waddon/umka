<div class="card bg-orange-400 text-white text-center p-3">
    <div>
        <a href="{{route('groups.index')}}" class="btn btn-lg btn-icon mb-3 mt-1 btn-outline text-white border-white bg-white rounded-round border-2"><i class="icon-comment-discussion"></i></a>
    </div>
    <blockquote class="blockquote mb-0">
        <h3>@lang('Groups')</h3>
        <footer class="text-white">
            @if($data->all)
                <table class="w-100">
                    <tr>
                        <td class="text-left">@lang('All')</td>
                        <td class="text-right">{{$data->all}}</td>
                    </tr>
                </table>
                <hr>
            @endif
            @if($data->list)
                <table class="w-100">
                @foreach($data->list as $key => $value)
                    <tr>
                        <td class="text-left">{{$key}}</td>
                        <td class="text-right">{{$value}}</td>
                    </tr>
                @endforeach
                </table>
            @endif
        </footer>
    </blockquote>
</div>