@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body content-area">
            {!!$model->content!!}
        </div>
    </div>
@endsection
