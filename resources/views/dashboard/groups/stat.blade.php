@extends('layouts.admin-layout')

@section('content')
<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card">
            <div class="card-body"><button class="btn btn-success" id="excel-export">@lang('Export to Excel')</button></div>
            <div class="card-body w-100" style="overflow-x: scroll;">
                <table class="table table-bordered" id="stat_table">
                    <thead>
                        <tr>
                            <td class="text-center">@lang('Name')</td>
                            <td class="text-center">@lang('Email')</td>
                            <td class="text-center">@lang('Last Activity Date')</td>
                            @foreach($courses as $keycourse => $course)
                                @foreach($course->modules as $keymodule => $module)
                                    {{--<td class="text-center {{ !($keymodule % 2) ? 'bg-grey-300' : ''}}">{{$module->name}}</td>--}}
                                    <td class="text-center {{ !($keymodule % 2) ? 'bg-grey-300' : ''}}">@lang('Module'){{$module->id}} - Всего тем</td>
                                    <td class="text-center {{ !($keymodule % 2) ? 'bg-grey-300' : ''}}">@lang('Module'){{$module->id}} - Пройдено тем</td>
                                    <td class="text-center {{ !($keymodule % 2) ? 'bg-grey-300' : ''}}">@lang('Module'){{$module->id}} - Нажатий на ссылки</td>
                                    <td class="text-center {{ !($keymodule % 2) ? 'bg-grey-300' : ''}}">@lang('Module'){{$module->id}} - Тест по модулю</td>
                                    <td class="text-center {{ !($keymodule % 2) ? 'bg-grey-300' : ''}}">@lang('Module'){{$module->id}} - Потыток сдачи</td>
                                    <td class="text-center {{ !($keymodule % 2) ? 'bg-grey-300' : ''}}">@lang('Module'){{$module->id}} - Дата сдачи</td>
                                @endforeach
                                <td class="text-center bg-green-300">Экзамен</td>
                                <td class="text-center bg-green-300">Потыток сдачи</td>
                                <td class="text-center bg-green-300">Дата сдачи</td>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($model->users as $key => $user)
                        <tr>
                            <td>{{$user->name ?? ''}}</td>
                            <td>{{$user->email ?? ''}}</td>
                            <td class="text-center">{{$user->last_action_at ? $user->last_action_at->format('d.m.Y H:i:s') : ''}}</td>
                            @foreach($modules as $keymodule => $module)
                                <td class="text-center {{ !($keymodule % 2) ? 'bg-grey-300' : ''}}">{{$module->activeThemes->count()}}</td>
                                <td class="text-center {{ !($keymodule % 2) ? 'bg-grey-300' : ''}}">{{$user->themesPassedCount($module->id)}}</td>
                                <td class="text-center {{ !($keymodule % 2) ? 'bg-grey-300' : ''}}">{{$user->data->modules->{$module->id}->push ?? '-'}}</td>
                                <td class="text-center {{ !($keymodule % 2) ? 'bg-grey-300' : ''}}">{{isset($user->data->modules->{$module->id}->passed) ? __('Passed') : ''}}</td>
                                <td class="text-center {{ !($keymodule % 2) ? 'bg-grey-300' : ''}}">{{$user->data->modules->{$module->id}->passing_count ?? '-'}}</td>
                                <td class="text-center {{ !($keymodule % 2) ? 'bg-grey-300' : ''}}">{{$user->data->modules->{$module->id}->last_passing_date ?? '-'}}</td>
                            @endforeach
                            <td class="text-center bg-green-300">{{isset($user->data->courses->{$course->id}->passed) ? __('Passed') : ''}}</td>
                            <td class="text-center bg-green-300">{{$user->data->courses->{$course->id}->passing_count ?? '-'}}</td>
                            <td class="text-center bg-green-300">{{$user->data->courses->{$course->id}->last_passing_date ?? '-'}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Right sidebar component -->
    {{--<div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('Navigation')</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>
                            <span class="small text-muted font-italic">@lang('Created')</span>
                            <span class="float-right">{{$model->created_at}}</span>
                        </p>
                        <p>
                            <span class="small text-muted font-italic">@lang('Updated')</span>
                            <span class="float-right">{{$model->updated_at}}</span>
                        </p>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <a href="{{ route('groups.edit', $model->id) }}" class="btn btn-light w-50" title="{{trans('cms.btn-edit')}}"><i class="icon-pencil7"></i></a>
                            <a href="{{ route('groups.index') }}" class="btn btn-danger w-50" title="@lang('Exit')"><i class="icon-esc"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>--}}
    <!-- /right sidebar component -->

</div>
@endsection

@section('scripts')
<script type="text/javascript" src="/js/plugins/Table-To-Excel-Export-Plugin-jQuery-ExcelGen/external/jszip.min.js"></script>
<script type="text/javascript" src="/js/plugins/Table-To-Excel-Export-Plugin-jQuery-ExcelGen/external/FileSaver.min.js"></script>
<script type="text/javascript" src="/js/plugins/Table-To-Excel-Export-Plugin-jQuery-ExcelGen/scripts/excel-gen.js"></script>

{{--<script src="/js/plugins/filesaver/FileSaver.min.js"></script>--}}
{{--<script src="/js/plugins/jspdf/jspdf.min.js"></script>--}}
@endsection
