@extends('layouts.admin-layout')

@section('content')
<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <!-- Modules -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">@lang('Modules')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="table-responsive pt-3">
                <table class="table datatable-basic table-striped dataTable no-footer w-100">
                    <thead>
                        <tr>
                            <th>@lang('Name')</th>
                            <th>@lang('Date start')</th>
                            <th>@lang('Date finish')</th>
                            <th>@lang('Actions')</th>
                        </tr>
                    </thead>
                    <tbody class="modules-list">
                        @foreach($modules as $key => $module)
                            <tr data-module-id="{{$module->id}}">
                                <td data-name="name">{{$module->name}}</td>
                                <td data-name="date_start" class="{{$model->ifModuleActive($module->id) ? 'text-success' : ''}}">{{$model->getDateStart($module->id)}}</td>
                                <td data-name="date_finish" class="{{$model->ifModuleActive($module->id) ? 'text-success' : ''}}">{{$model->getDateFinish($module->id)}}</td>
                                <td data-name="actions" class="text-center">@if(!$model->ifModuleActive($module->id) && !$model->ifModuleFuture($module->id))<a href="{{route('run')}}" class="btn btn-success btn-run-module" data-toggle="modal" data-target="#modal_form_horizontal" data-id="{{$module->id}}">@lang('Activate')</a>@else<a href="{{route('run')}}" class="btn btn-warning btn-run-module" data-toggle="modal" data-target="#modal_form_horizontal" data-id="{{$module->id}}">@lang('Change')</a>@endif</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /modules-->
        <!-- Quizzes -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">@lang('Quizzes')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="table-responsive pt-3">
                <table class="table datatable-basic table-striped dataTable no-footer w-100">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('Name')</th>
                            <th class="text-center">@lang('Status')</th>
                            <th class="text-center">@lang('Actions')</th>
                        </tr>
                    </thead>
                    <tbody class="modules-list">
                        @foreach($quizzes as $key => $quiz)
                            <tr data-quiz-id="{{$quiz->id}}">
                                <td data-name="name">{{$quiz->name}}</td>
                                <td data-name="status">{{$model->ifQuizActive($quiz->id) ? __('Activated') : ''}}</td>
                                <td data-name="actions" class="text-center">@if(!$model->ifQuizActive($quiz->id) )<a href="{{route('groups.setactivity',['id' => $model->id])}}" class="btn btn-success btn-setactivity" data-quiz="{{$quiz->id}}">@lang('Activate')</a>@else<a href="{{route('groups.setactivity',['id' =>  $model->id])}}" class="btn btn-warning btn-setactivity" data-quiz="{{$quiz->id}}">@lang('Deactivate')</a>@endif</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /quizzes -->

        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">@lang('Users in the Group')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>

            <div class="pl-3 pr-3">
                <div class="row search-panel">
                    <div class="col-lg-6">
                        <div class="form-group row mb-0">
                            <label for="role_id" class="col-form-label col-sm-3">@lang('Courses')</label>
                            <div class="col-sm-9">
                                <select id="course_id" class="form-control">
                                    @foreach(\App\Course::get() as $key => $course)
                                        <option value="{{$course->id}}">{{$course->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group row mb-0">
                            <label for="activity" class="col-form-label col-sm-3">@lang('Modules')</label>
                            <div class="col-sm-9">
                                <select id="module_id2" name="module_id2" class="form-control">
                                    <option value="0">@lang('All Modules')</option>
                                    @foreach($modules as $key => $module)
                                        <option value="{{$module->id}}">{{$module->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                    <div class="table-responsive pt-3">
                        <table class="table datatable-basic table-striped dataTable no-footer w-100" id="datatableActive">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('Avatar')</th>
                                    <th>@lang('Name')</th>
                                    <th>@lang('Activity')</th>
                                    <th>@lang('Progress')</th>
                                    <th>@lang('Actions')</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
        </div>

    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('Navigation')</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>
                            <span class="small text-muted font-italic">@lang('Created')</span>
                            <span class="float-right">{{$model->created_at}}</span>
                        </p>
                        <p>
                            <span class="small text-muted font-italic">@lang('Updated')</span>
                            <span class="float-right">{{$model->updated_at}}</span>
                        </p>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <a href="{{ route('groups.edit', $model->id) }}" class="btn btn-light w-50" title="{{trans('cms.btn-edit')}}"><i class="icon-pencil7"></i></a>
                            <a href="{{ route('groups.index') }}" class="btn btn-danger w-50" title="@lang('Exit')"><i class="icon-esc"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>
@endsection

@section('scripts')
<script>
    var language = {"url": "/assets/js/plugins/tables/datatables/languages/{{app()->getLocale()}}.json"};
    var pageLength = 10;
    var columns = [
            {"data":"id"},
            {"data":"avatar","searchable":false,"orderable":false},
            {"data":"name"},
            {"data":"activity"},
            {"data":"progress"},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 1, "className": "text-center", },
            { "targets": 3, "className": "text-center", },
            { "targets": 4, "className": "text-center", },
            { "targets": 5, "className": "text-center", },
        ];


    var tableActive = $("#datatableActive").DataTable({
        "autoWidth" : true,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "pageLength": pageLength,
        // "bSortCellsTop": true,
        // "order": [[ 0, "desc" ]],
        "ajax": {
            "url": "{{route('groups.detail')}}",
            "dataType": "json",
            "type": "POST",
            "data":
            {
                "_token" : "{{ csrf_token() }}",
                "table" : "active",
                "group_id" : {{$model->id}},
                "course_id" :  function() {
                    return $('#course_id').val();
                },
                "module_id" : function() {
                    return $('#module_id2').val();
                },
            },
        },
        "columns":columns,
        'columnDefs': columnDefs,
        "language": language,
        dom: '<"datatable-header"frl><"datatable-scroll"t><"datatable-footer pr-3 pl-3"ip>',
    });

    $('div.search-panel').on('change','select',function(e){
        tableActive.ajax.reload(null, false);
    })

    // $(document).on('click','.ajax-action',function(e){
    //     e.preventDefault();
    //     $.ajax({
    //         url: $(this).attr('href'),
    //         type: 'GET',
    //         dataType : "json",
    //         success: function (data) {
    //             tableActive.ajax.reload(null, false);
    //         },
    //         error: function(){
    //             console.log('Ajax error');
    //         }
    //     });
    // });

    $(document).on('click','.btn-setactivity',function(e){
        e.preventDefault();
        let element = $(this);
        $.ajax({
            url: $(this).attr('href'),
            type: 'GET',
            dataType : "json",
            data: {quiz_id:$(this).attr('data-quiz')},
            success: function (data) {
                console.log(data);
                if (data==true) {
                    element.text('@lang('Deactivate')');
                    element.toggleClass('btn-success btn-warning');
                    element.closest('tr').find('[data-name=status]').text('@lang('Activated')');
                } else {
                    element.text('@lang('Activate')');
                    element.toggleClass('btn-success btn-warning');
                    element.closest('tr').find('[data-name=status]').text('');
                }
            },
            error: function(){
                console.log('Ajax error');
            }
        });
    });

</script>
@endsection
@section('modals')
@include('template-parts.run-module')
@endsection
