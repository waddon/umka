@extends('layouts.admin-layout')

@section('content')
<style>
.question{margin-top: 10px; border: 1px solid #f5f5f5;position: relative;}
.question-element{padding: 15px;}
.question-move{cursor: move;}
.question-content{flex: 1 1 1px;}
.question-remove{cursor: pointer;}
.question[data-type=label]:before{content: "@lang('Label')";position: absolute;top:-10px;right: 10px;color:blue;}
.question[data-type=radio]:before{content: "@lang('Radio')";position: absolute;top:-10px;right: 10px;color:blue;}
.question[data-type=text]:before{content: "@lang('Text')";position: absolute;top:-10px;right: 10px;color:blue;}
.question[data-type=select-yes]:before{content: "@lang('Select with fiels for yes')";position: absolute;top:-10px;right: 10px;color:blue;}
.question[data-type=select-no]:before{content: "@lang('Select with fiels for no')";position: absolute;top:-10px;right: 10px;color:blue;}
.question[data-type=select-yes-no]:before{content: "@lang('Select with fiels for both answer')";position: absolute;top:-10px;right: 10px;color:blue;}
</style>
{{ Form::model($model, array('url' => $model->id ? route('quizzes.update', $model->id) : route('quizzes.store'), 'method' => $model->id ? 'PUT' : 'POST')) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header bg-transparent header-elements-inline">
                <h5 class="card-title">@lang('Parameters')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    {{ Form::label('key', __('Key'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('key', null, array('class' => 'field form-control')) }}
                    </div>
                </div>

                @if(count(config()->get('app.locales'))>1)
                    <ul class="nav nav-tabs nav-tabs-highlight">
                        @foreach(config()->get('app.locales') as $key => $language)
                            <li class="nav-item"><a href="#tab-{{$language}}" class="nav-link @if($key==0) active @endif" data-toggle="tab"><img src="/assets/img/lang/{{$language}}.png" alt="" class="img-flag mr-2"> @lang($language)</a></li>
                        @endforeach
                    </ul>
                @endif
                <div class="tab-content">
                    @foreach(config()->get('app.locales') as $key => $language)
                        <div class="tab-pane fade @if($key==0) show active @endif" id="tab-{{$language}}">
                            <div class="form-group row">
                                {{ Form::label('name-'.$language, __('Name'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                                <div class="col-sm-8">
                                    {{ Form::text('name-'.$language, (isset($model->translate($language)->name) ? $model->translate($language)->name : null), ['class' => 'field form-control']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('content-'.$language, __('Content'), ['class'=>'small text-muted font-italic col-form-label']) }}
                                {{ Form::textarea('content-'.$language, (isset($model->translate($language)->content) ? $model->translate($language)->content : null), ['class' => 'editor-full field form-control', 'id' => 'editor-'.$language]) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('data-'.$language, __('Questions'), ['class'=>'small text-muted font-italic col-form-label']) }}
                                <div class="questions">
                                    {{ Form::hidden('data-'.$language) }}
                                    <div class="question-list">
                                        @if(isset($model->translate($language)->data) && is_array($model->translate($language)->data))
                                            @foreach( $model->translate($language)->data as $key_anwser => $question)
                                                <div class="question d-flex align-items-center" data-id="{{$question->id ?? ''}}" data-type="{{$question->type ?? ''}}">
                                                    <div class="question-element question-move"><i class="icon-grid"></i></div>
                                                    <div class="question-element question-content" contenteditable="true">{{$question->text ?? ''}}</div>
                                                    <div class="question-element list-icons">
                                                        <div class="list-icons-item dropdown">
                                                            <a href="#" class="list-icons-item caret-0 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                <i class="icon-menu9"></i>
                                                            </a>
                                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-sm" x-placement="bottom-end">
                                                                <a href="#" class="dropdown-item question-type" data-type="label">@lang('Label')</a>
                                                                <a href="#" class="dropdown-item question-type" data-type="radio">@lang('Radio')</a>
                                                                <a href="#" class="dropdown-item question-type" data-type="text">@lang('Text')</a>
                                                                <a href="#" class="dropdown-item question-type" data-type="select-yes">@lang('Select with fiels for yes')</a>
                                                                <a href="#" class="dropdown-item question-type" data-type="select-no">@lang('Select with fiels for no')</a>
                                                                <a href="#" class="dropdown-item question-type" data-type="select-yes-no">@lang('Select with fiels for both answer')</a>
                                                                <div class="dropdown-divider"></div>
                                                                <a href="#" class="dropdown-item question-remove"><i class="icon-bin"></i> @lang('Delete')</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="answers-buttons text-center mt-3">
                                        <button type="button" class="btn btn-success question-add"><i class="icon-plus22"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>

    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('Navigation')</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>
                            <span class="small text-muted font-italic">@lang('Created')</span>
                            <span class="float-right">{{$model->created_at}}</span>
                        </p>
                        <p>
                            <span class="small text-muted font-italic">@lang('Updated')</span>
                            <span class="float-right">{{$model->updated_at}}</span>
                        </p>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="@lang('Save')"><i class="icon-floppy-disk"></i></button>
                            <a href="{{ route('quizzes.index') }}" class="btn btn-danger w-50" title="@lang('Exit')"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

{{ Form::close() }}

<template id="question-empty">
    <div class="question d-flex align-items-center" data-id="{id}" data-type="label">
        <div class="question-element question-move"><i class="icon-grid"></i></div>
        <div class="question-element question-content" contenteditable="true"></div>
        <div class="question-element list-icons">
            <div class="list-icons-item dropdown">
                <a href="#" class="list-icons-item caret-0 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="icon-menu9"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-sm" x-placement="bottom-end">
                    <a href="#" class="dropdown-item question-type" data-type="label">@lang('Label')</a>
                    <a href="#" class="dropdown-item question-type" data-type="radio">@lang('Radio')</a>
                    <a href="#" class="dropdown-item question-type" data-type="text">@lang('Text')</a>
                    <a href="#" class="dropdown-item question-type" data-type="select-yes">@lang('Select with fiels for yes')</a>
                    <a href="#" class="dropdown-item question-type" data-type="select-no">@lang('Select with fiels for no')</a>
                    <a href="#" class="dropdown-item question-type" data-type="select-yes-no">@lang('Select with fiels for both answer')</a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item question-remove"><i class="icon-bin"></i> @lang('Delete')</a>
                </div>
            </div>
        </div>
    </div>
</template>
@endsection

@section('scripts')
@include('template-parts.save_ajax_form')
<script src="/js/plugins/notifications/sweet_alert.min.js""></script>
<script src="/js/plugins/editors/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

    CKEDITOR.disableAutoInline = true;
    $('.editor-full').each(function(e){
        CKEDITOR.replace(this.id, {
            height: 400,
            extraPlugins: 'forms'
        });
    });    

    $('form').on('submit', function(e){
        e.preventDefault();
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        $('.questions').each(function(){
            composeQuestions($(this));
        });
        save_ajax_form( $(this), "{!! route('quizzes.index') !!}");
    });


    $('.question-list').sortable({
        handle: ".question-move",
        tolerance: 'pointer',
        opacity: 0.6,
        placeholder: 'sortable-placeholder',
        start: function(e, ui){
            ui.placeholder.height(ui.item.outerHeight()+10);
        }
    });

    $('.questions').on('click','.question-remove',function(e) {
        e.preventDefault();
        $(this).closest('.question').remove();
    });

    $('.questions').on('click','.question-type',function(e) {
        e.preventDefault();
        $(this).closest('.question').attr('data-type',$(this).attr('data-type'));
    });

    $('.questions').on('click','.question-add',function() {
        $(this).closest('.questions').find('.question-list').append( renderTemplate('question-empty',{id:Date.now()}) );
    });

    function composeQuestions(element){
        var result = [];
        element.find('.question').each(function(){
            var question = {}
            question.id = $(this).attr('data-id');
            question.type = $(this).attr('data-type');
            question.text = $(this).find('.question-content').first().text();
            result.push( question );
        });
        element.find('input[type=hidden]').val( JSON.stringify(result) );
        return;
    }

function renderTemplate(name, data) {
    var template = document.getElementById(name).innerHTML;

    for (var property in data) {
        if (data.hasOwnProperty(property)) {
            var search = new RegExp('{' + property + '}', 'g');
            template = template.replace(search, data[property]);
        }
    }

    return template;
}
</script>
@endsection
