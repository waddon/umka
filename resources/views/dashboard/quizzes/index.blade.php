@extends('layouts.admin-layout')

@section('content')

    <div class="card mb-3">

        <div class="tab-content">
            <div class="tab-pane fade show active" id="tab-active">
                <div class="table-responsive">
                    <table class="table datatable-basic table-striped dataTable no-footer w-100" id="datatableActive">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('Name')</th>
                                <th>@lang('Questions')</th>
                                <th>@lang('Actions')</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
<script>
    var language = {"url": "/js/plugins/tables/datatables/languages/{{app()->getLocale()}}.json"};
    var pageLength = {{\App\Option::getOption('rowCount',10)}};
    var columns = [
            {"data":"id"},
            {"data":"name"},
            {"data":"questions","searchable":false,"orderable":false},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 2, "className": "text-center", },
            { "targets": 3, "className": "text-center", },
        ];


    var tableActive = $("#datatableActive").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        // "bSortCellsTop": true,
        // "order": [[ 0, "desc" ]],
        ajax: {
            "url": "{{route('quizzes.datatable')}}",
            "dataType": "json",
            "type": "POST",
            "data": function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.table = "active";
            }            
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
        dom: '<"datatable-header"Bfrl><"datatable-scroll"t><"datatable-footer pr-3 pl-3"ip>',
            buttons: [
                {
                    text: '<i class="icon-file-plus"></i>',
                    className: 'ml-3 btn btn-success',
                    titleAttr: '@lang('Create')',
                    action: function(e, dt, node, config) {
                        window.location.href = '{{ route('quizzes.create') }}';
                    }
                }
            ],
    });

</script>
@endsection
@section('modals')
@include('template-parts.modal-ajax-destroy')
@endsection

