@extends('layouts.admin-layout')

@section('content')
<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">

        <div class="card content-area">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">{{ $model->name }}</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @if(is_array($model->content))
                    @foreach($model->content as $key2 => $answer)
                        <div class="mb-1 {{$answer->check ? 'bg-success-300' : ''}}">{{$answer->text}}</div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('Navigation')</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>
                            <span class="small text-muted font-italic">@lang('Created')</span>
                            <span class="float-right">{{$model->created_at}}</span>
                        </p>
                        <p>
                            <span class="small text-muted font-italic">@lang('Updated')</span>
                            <span class="float-right">{{$model->updated_at}}</span>
                        </p>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <a href="{{ route('questions.edit', $model->id) }}" class="btn btn-light w-50" title="{{trans('cms.btn-edit')}}"><i class="icon-pencil7"></i></a>
                            <a href="{{ route('questions.index') }}" class="btn btn-danger w-50" title="@lang('Exit')"><i class="icon-esc"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>
@endsection

