@extends('layouts.admin-layout')

@section('content')
<style>
.answer-element{padding: 5px;}
.answer-move{cursor: move;}
.answer-text{flex: 1 1 1px;}
.answer-remove{cursor: pointer;}
.custom-control-label{position: absolute;}
</style>
{{ Form::model($model, array('url' => $model->id ? route('questions.update', $model->id) : route('questions.store'), 'method' => $model->id ? 'PUT' : 'POST')) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header bg-transparent header-elements-inline">
                <h5 class="card-title">@lang('Parameters')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    {{ Form::label('key', __('Key'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('key', null, array('class' => 'field form-control')) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('module_id', __('Module'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::select('module_id', $modules, null, array('class' => 'field form-control')) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('type_id', __('Type'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::select('type_id', $types, null, array('class' => 'field form-control')) }}
                    </div>
                </div>

                @if(count(config()->get('app.locales'))>1)
                    <ul class="nav nav-tabs nav-tabs-highlight">
                        @foreach(config()->get('app.locales') as $key => $language)
                            <li class="nav-item"><a href="#tab-{{$language}}" class="nav-link @if($key==0) active @endif" data-toggle="tab"><img src="/assets/img/lang/{{$language}}.png" alt="" class="img-flag mr-2"> @lang($language)</a></li>
                        @endforeach
                    </ul>
                @endif
                <div class="tab-content">
                    @foreach(config()->get('app.locales') as $key => $language)
                        <div class="tab-pane fade @if($key==0) show active @endif" id="tab-{{$language}}">
                            <div class="form-group row">
                                {{ Form::label('name-'.$language, __('Name'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                                <div class="col-sm-8">
                                    {{ Form::text('name-'.$language, (isset($model->translate($language)->name) ? $model->translate($language)->name : null), ['class' => 'field form-control']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('content-'.$language, __('Answers'), ['class'=>'small text-muted font-italic col-form-label']) }}
                                <div class="answers">
                                    {{ Form::hidden('content-'.$language) }}
                                    <div class="answers-list">
                                        @if(isset($model->translate($language)->content) && is_array($model->translate($language)->content))
                                            @foreach( $model->translate($language)->content as $key_anwser => $answer)
                                                <div class="answer d-flex align-items-center">
                                                    <div class="answer-element answer-move"><i class="icon-grid"></i></div>
                                                    <div class="answer-element answer-check">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="{{$language}}-{{$key_anwser}}" {{ $answer->check ? 'checked' : '' }}>
                                                            <label class="custom-control-label" for="{{$language}}-{{$key_anwser}}"></label>
                                                        </div>
                                                    </div>
                                                    <div class="answer-element answer-text" contenteditable="true">{{$answer->text}}</div>
                                                    <div class="answer-element answer-remove"><i class="icon-bin"></i></div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="answers-buttons text-center">
                                        <button type="button" class="btn btn-success answer-add"><i class="icon-plus22"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>

    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('Navigation')</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>
                            <span class="small text-muted font-italic">@lang('Created')</span>
                            <span class="float-right">{{$model->created_at}}</span>
                        </p>
                        <p>
                            <span class="small text-muted font-italic">@lang('Updated')</span>
                            <span class="float-right">{{$model->updated_at}}</span>
                        </p>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="@lang('Save')"><i class="icon-floppy-disk"></i></button>
                            <a href="{{ route('questions.index') }}" class="btn btn-danger w-50" title="@lang('Exit')"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

{{ Form::close() }}

<template id="answer-empty">
    <div class="answer d-flex align-items-center">
        <div class="answer-element answer-move"><i class="icon-grid"></i></div>
        <div class="answer-element answer-check">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="{id}">
                <label class="custom-control-label" for="{id}"></label>
            </div>
        </div>
        <div class="answer-element answer-text" contenteditable="true"></div>
        <div class="answer-element answer-remove"><i class="icon-bin"></i></div>
    </div>
</template>
@endsection

@section('scripts')
@include('template-parts.save_ajax_form')
<script src="/js/plugins/notifications/sweet_alert.min.js""></script>

<script type="text/javascript">
$(document).ready(function(){
    $('form').on('submit', function(e){
        e.preventDefault();
        $('.answers').each(function(){
            composeAnswers($(this));
        });
        save_ajax_form( $(this), "{!! route('questions.index') !!}");
    });


    $('.answers-list').sortable({
        handle: ".answer-move",
        tolerance: 'pointer',
        opacity: 0.6,
        placeholder: 'sortable-placeholder',
        start: function(e, ui){
            ui.placeholder.height(ui.item.outerHeight());
        }
    });

    $('.answers').on('click','.answer-remove',function() {
        $(this).closest('.answer').remove();
    });

    $('.answers').on('click','.answer-add',function() {
        $(this).closest('.answers').find('.answers-list').append( renderTemplate('answer-empty',{id:Date.now()}) );
    });

    function composeAnswers(element){
        var result = [];
        var loop = 1;
        element.find('.answer').each(function(){
            var answer = {}
            answer.id = loop;
            answer.check = $(this).find('.answer-check input[type=checkbox]').first().prop('checked');
            answer.text = $(this).find('.answer-text').first().text();
            result.push( answer );
            loop++;
        });
        element.find('input[type=hidden]').val( JSON.stringify(result) );
        return;
    }

});    
</script>
@endsection
