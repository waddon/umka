@extends('layouts.admin-layout')

@section('content')

    <div class="d-flex align-items-start flex-column flex-md-row">

        <!-- Left content -->
        <div class="order-2 order-md-1 w-100">
            <div class="card" id="release">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">@lang('Parameters')</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-3"><img class="rounded-circle w-100" src="{{ $user->getAvatar() }}" alt=""></div>
                        <div class="col-xl-9">
                                <div class="row">
                                    <div class="col-lg-2 small text-muted font-italic mb-2">@lang('Pin'):</div>
                                    <div class="col-lg-4 mb-2">{{$user->pin}}</div>
                                    <div class="col-lg-2 small text-muted font-italic mb-2">@lang('Name'):</div>
                                    <div class="col-lg-4 mb-2">{{$user->name}}</div>
                                    <div class="col-lg-2 small text-muted font-italic mb-2">@lang('Birthday'):</div>
                                    <div class="col-lg-4 mb-2">{{$user->birthday ? $user->birthday->format('d.m.Y') : ''}}</div>
                                    <div class="col-lg-2 small text-muted font-italic mb-2">@lang('Passport'):</div>
                                    <div class="col-lg-4 mb-2">{{$user->passport}}</div>
                                    <div class="col-lg-2 small text-muted font-italic mb-2">@lang('Category'):</div>
                                    <div class="col-lg-4 mb-2">{{$categories[$user->category_id]}}</div>
                                    <div class="col-lg-2 small text-muted font-italic mb-2">@lang('Position'):</div>
                                    <div class="col-lg-4 mb-2">{{$user->position}}</div>
                                    <div class="col-lg-2 small text-muted font-italic mb-2">@lang('Email'):</div>
                                    <div class="col-lg-4 mb-2">{{$user->email}}</div>
                                    <div class="col-lg-2 small text-muted font-italic mb-2">@lang('Phone'):</div>
                                    <div class="col-lg-4 mb-2">{{$user->phone}}</div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-lg-4 small text-muted font-italic mb-2">@lang('Groups'):</div>
                                    <div class="col-lg-8 mb-2">{{$user->groups->implode('name', ', ')}}</div>
                                    <div class="col-lg-4 small text-muted font-italic mb-2">@lang('Roles'):</div>
                                    <div class="col-lg-8 mb-2">{{$user->roles->implode('name', ', ')}}</div>
                                    @if($user->scan)
                                        <div class="col-lg-4 small text-muted font-italic mb-2">@lang('Confirmation document'):</div>
                                        <div class="col-lg-8 mb-2"><a href="#" id="download">@lang('Download')</a></div>
                                    @endif
                                </div>
                        </div>
                    </div>
                    <table class="w-100 table table-striped mt-3">
                        <thead class="bg-grey">
                            <tr>
                                <td class="p-1 text-center">Название</td>
                                <td class="p-1 text-center">Всего тем</td>
                                <td class="p-1 text-center">Пройдено тем</td>
                                <td class="p-1 text-center">Количество прохождений теста</td>
                                <td class="p-1 text-center">Дата теста</td>
                                <td class="p-1 text-center">Модуль пройден</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($user->tableModules() as $key => $module)
                            <tr>
                                <td class="p-1 {{ isset($module->course) ? 'bg-green' : '' }}">{{$module->name}}</td>
                                <td class="p-1 {{ isset($module->course) ? 'bg-green' : '' }} text-center">{{$module->themecount}}</td>
                                <td class="p-1 {{ isset($module->course) ? 'bg-green' : '' }} text-center">{{$module->themepassed}}</td>
                                <td class="p-1 {{ isset($module->course) ? 'bg-green' : '' }} text-center">{{$module->passing_count}}</td>
                                <td class="p-1 {{ isset($module->course) ? 'bg-green' : '' }} text-center">{{$module->last_passing_date}}</td>
                                <td class="p-1 {{ isset($module->course) ? 'bg-green' : '' }} text-center">{{$module->passed}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /left content -->

        <!-- Right sidebar component -->
        <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
            <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
                <div class="sidebar-content">
                    <div class="card">
                        <div class="card-header bg-transparent header-elements-inline">
                            <span class="text-uppercase font-size-sm font-weight-semibold">@lang('Navigation')</span>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                                <div class="btn-group w-100">
                                    <a href="{{ route('users.index') }}" class="btn btn-danger w-100" title="Exit"><i class="icon-esc"></i></a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /right sidebar component -->

    </div>

@endsection

@section('scripts')
<script>
    $('#download').click(function(e){
        e.preventDefault();
        window.location = "{{route('users.download',$user->id)}}";
    });
</script>
@endsection
