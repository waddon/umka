@extends('layouts.admin-layout')

@section('content')

    <div class="d-flex align-items-start flex-column flex-md-row">

        <!-- Left content -->
        <div class="order-2 order-md-1 w-100">
            <div class="card" id="release">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">@lang('Quizzes')</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @if(isset($model->data->quizzes))
                        <ul class="nav nav-tabs nav-tabs-highlight">
                            @foreach($model->data->quizzes as $key => $quiz)
                                <li class="nav-item"><a href="#tab-{{$key}}" class="nav-link" data-toggle="tab">{{$quizzes[$key]['name'] ?? '---'}}</a></li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach($model->data->quizzes as $key => $quiz)
                                <div class="tab-pane fade" id="tab-{{$key}}">
                                    <table class="table">
                                    @foreach($quiz as $key2 => $question)
                                        @if($key2!='_token')
                                        <tr>
                                            <td class="w-50">{!!$quizzes[$key]['data']->{$key2} ?? '<span class="pl-5">' . __('Details') . '</span>'!!}</td>
                                            <td class="w-50">{{$question}}</td>
                                        </tr>
                                        @endif
                                    @endforeach
                                    </table>
                                    {{--@php(dump($quiz))--}}
                                </div>
                            @endforeach
                        </div>
                    @endif

                </div>
            </div>
        </div>
        <!-- /left content -->

        <!-- Right sidebar component -->
        <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
            <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
                <div class="sidebar-content">
                    <div class="card">
                        <div class="card-header bg-transparent header-elements-inline">
                            <span class="text-uppercase font-size-sm font-weight-semibold">@lang('Navigation')</span>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                                <div class="btn-group w-100">
                                    <a href="{{ route('users.index') }}" class="btn btn-danger w-100" title="Exit"><i class="icon-esc"></i></a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /right sidebar component -->

    </div>

@endsection
