@extends('layouts.admin-layout')

@section('content')

{{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT')) }}

    <div class="d-flex align-items-start flex-column flex-md-row">

        <!-- Left content -->
        <div class="order-2 order-md-1 w-100">
            <div class="card" id="release">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">@lang('Parameters')</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6 form-group {{ (isset($errors) && $errors->has('pin')) ? 'has-danger' : '' }}">
                                        {{ Form::label('pin', __('Pin'), ['class'=>'small text-muted font-italic']) }}
                                        {{ Form::text('pin', $user->pin, array('class' => 'form-control'. ( (isset($errors) && $errors->has('pin')) ? ' is-invalid' : '' ))) }}
                                        @if(isset($errors) && $errors->has('pin'))
                                            @foreach ($errors->get('pin') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-6 form-group {{ (isset($errors) && $errors->has('name')) ? 'has-danger' : '' }}">
                                        {{ Form::label('name', __('Name'), ['class'=>'small text-muted font-italic']) }}
                                        {{ Form::text('name', $user->name, array('class' => 'form-control'. ( (isset($errors) && $errors->has('name')) ? ' is-invalid' : '' ))) }}
                                        @if( isset($errors) && $errors->has('name'))
                                            @foreach ($errors->get('name') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-6 form-group {{ (isset($errors) && $errors->has('birthday')) ? 'has-danger' : '' }}">
                                        {{ Form::label('birthday', __('Birthday'), ['class'=>'small text-muted font-italic']) }}
                                        {{ Form::date('birthday', $user->birthday, array('class' => 'form-control'. ( (isset($errors) && $errors->has('birthday')) ? ' is-invalid' : '' ))) }}
                                        @if(isset($errors) && $errors->has('birthday'))
                                            @foreach ($errors->get('birthday') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-6 form-group {{ (isset($errors) && $errors->has('passport')) ? 'has-danger' : '' }}">
                                        {{ Form::label('passport', __('Passport'), ['class'=>'small text-muted font-italic']) }}
                                        {{ Form::text('passport', $user->passport, array('class' => 'form-control'. ( (isset($errors) && $errors->has('passport')) ? ' is-invalid' : '' ))) }}
                                        @if(isset($errors) && $errors->has('passport'))
                                            @foreach ($errors->get('passport') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-6 form-group">
                                        {{ Form::label('category_id', __('Category'), ['class'=>'small text-muted font-italic']) }}<br>
                                        {{ Form::select('category_id', $categories, null, array('class' => 'form-control'. ((isset($errors) && $errors->has('category_id')) ? ' is-invalid' : ''))) }}
                                        @if(isset($errors) && $errors->has('category_id'))
                                            @foreach ($errors->get('category_id') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-6 form-group {{ (isset($errors) && $errors->has('position')) ? 'has-danger' : '' }}">
                                        {{ Form::label('position', __('Position'), ['class'=>'small text-muted font-italic']) }}
                                        {{ Form::text('position', $user->position, array('class' => 'form-control'. ( (isset($errors) && $errors->has('position')) ? ' is-invalid' : '' ))) }}
                                        @if(isset($errors) && $errors->has('position'))
                                            @foreach ($errors->get('position') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-6 form-group {{ (isset($errors) && $errors->has('email')) ? 'has-danger' : '' }}">
                                        {{ Form::label('email', __('Email'), ['class'=>'small text-muted font-italic']) }}
                                        {{ Form::email('email', $user->email, array('class' => 'form-control'. ((isset($errors) && $errors->has('email')) ? ' is-invalid' : '' ))) }}
                                        @if(isset($errors) && $errors->has('email'))
                                            @foreach ($errors->get('email') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-6 form-group {{ (isset($errors) && $errors->has('phone')) ? 'has-danger' : '' }}">
                                        {{ Form::label('phone', __('Phone'), ['class'=>'small text-muted font-italic']) }}
                                        {{ Form::text('phone', $user->phone, array('class' => 'form-control'. ((isset($errors) && $errors->has('phone')) ? ' is-invalid' : '' ))) }}
                                        @if(isset($errors) && $errors->has('phone'))
                                            @foreach ($errors->get('phone') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
            </div>
        </div>
        <!-- /left content -->

        <!-- Right sidebar component -->
        <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
            <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
                <div class="sidebar-content">
                    <div class="card">
                        <div class="card-header bg-transparent header-elements-inline">
                            <span class="text-uppercase font-size-sm font-weight-semibold">@lang('Navigation')</span>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                                <div class="btn-group w-100">
                                    <button type="submit" id="submit" class="btn btn-success w-50" title="Save"><i class="icon-floppy-disk"></i></button>
                                    <a href="{{ route('users.index') }}" class="btn btn-danger w-50" title="Exit"><i class="icon-esc"></i></a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>

                        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
                            <div class="sidebar-content">
                                <div class="card">
                                    <div class="card-header bg-transparent header-elements-inline">
                                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('Roles')</span>
                                        <div class="header-elements">
                                            <div class="list-icons">
                                                <a class="list-icons-item" data-action="collapse"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        @foreach ($roles as $role)
                                            <div class="custom-control custom-checkbox">
                                                {{ Form::checkbox('roles[]',  $role->id , null, ['id' => $role->name, 'class' => 'custom-control-input'. ((isset($errors) && $errors->has('roles')) ? ' is-invalid' : '' )]) }}
                                                {{ Form::label($role->name, ucfirst($role->name), ['class' => 'custom-control-label']) }}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
                            <div class="sidebar-content">
                                <div class="card">
                                    <div class="card-header bg-transparent header-elements-inline">
                                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('Groups')</span>
                                        <div class="header-elements">
                                            <div class="list-icons">
                                                <a class="list-icons-item" data-action="collapse"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        @foreach ($groups as $group)
                                            <div class="custom-control custom-checkbox">
                                                {{ Form::checkbox('groups[]',  $group->id , null, ['id' => $group->name, 'class' => 'custom-control-input'. ((isset($errors) && $errors->has('groups')) ? ' is-invalid' : '' )]) }}
                                                {{ Form::label($group->name, ucfirst($group->name), ['class' => 'custom-control-label']) }}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>
        <!-- /right sidebar component -->

    </div>

{{ Form::close() }}

@endsection

