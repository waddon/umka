@extends('layouts.admin-layout')

@section('content')
    <div class="card mb-3">

        <div class="pl-3 pt-3 pr-3">
            <div class="row search-panel">
                <div class="col-lg-4">
                    <div class="form-group row mb-0">
                        <label for="role_id" class="col-form-label col-sm-3">@lang('Roles')</label>
                        <div class="col-sm-9">
                            <select id="role_id" class="form-control">
                                <option value="0">@lang('All Roles')</option>
                                @foreach(\App\Role::all() as $key => $role)
                                    <option value="{{$role->id}}">@lang($role->name)</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group row mb-0">
                        <label for="activity" class="col-form-label col-sm-3">@lang('Activity')</label>
                        <div class="col-sm-9">
                            <select id="activity" name="activity" class="form-control">
                                <option value="0">@lang('All Types')</option>
                                <option value="1">@lang('Blocked')</option>
                                <option value="2">@lang('Active')</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group row mb-0">
                        <label for="newletter" class="col-form-label col-sm-3">@lang('New Letter')</label>
                        <div class="col-sm-9">
                            <select id="newletter" name="newletter" class="form-control">
                                <option value="0">@lang('All Types')</option>
                                <option value="1">@lang('New Letter not Exists')</option>
                                <option value="2">@lang('New Letter Exists')</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--<div class="card-body">--}}
            {{--<ul class="nav nav-tabs nav-tabs-highlight">
                <li class="nav-item"><a href="#tab-active" class="nav-link active" data-toggle="tab"><i class="icon-menu7 mr-2"></i> @lang('Active')</a></li>
            </ul>--}}
            <div class="tab-content">
                {{ Form::hidden('user_id', $user_id, ['id'=>'user_id']) }}
                <div class="tab-pane fade show active" id="tab-active">
                    <div class="table-responsive">
                        <table class="table datatable-basic table-striped dataTable no-footer mb-3 w-100" id="datatableActive">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('Avatar')</th>
                                    <th>@lang('Name')</th>
                                    <th>@lang('Email')</th>
                                    <th>@lang('Roles')</th>
                                    <th>@lang('Activity')</th>
                                    <th>@lang('New Letter')</th>
                                    <th>@lang('Created')</th>
                                    <th>@lang('Actions')</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

        {{--</div>--}}
    </div>

@endsection

@section('scripts')
<script>
    var language = {"url": "/js/plugins/tables/datatables/languages/{{app()->getLocale()}}.json"};
    var pageLength = {{\App\Option::getOption('rowCount',10)}};
    var columns = [
            {"data":"id"},
            {"data":"avatar","searchable":false,"orderable":false},
            {"data":"name"},
            {"data":"email"},
            {"data":"roles","searchable":false,"orderable":false},
            {"data":"activity"},
            {"data":"newletter"},
            {"data":"created"},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 1, "className": "text-center", },
            { "targets": 5, "className": "text-center", },
            { "targets": 6, "className": "text-center", },
            { "targets": 7, "className": "text-center", },
            { "targets": 8, "className": "text-center", },
        ];


    var tableActive = $("#datatableActive").DataTable({
        "autoWidth" : true,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "pageLength": pageLength,
        // "bSortCellsTop": true,
        // "order": [[ 0, "desc" ]],
        "ajax": {
            "url": "{{route('users.datatable')}}",
            "dataType": "json",
            "type": "POST",
            "data":
            {
                "_token" : "{{ csrf_token() }}",
                "table" : "active",
                "role_id" :  function() {
                    return $('#role_id').val();
                },
                "activity" : function() {
                    return $('#activity').val();
                },
                "newletter" : function() {
                    return $('#newletter').val();
                },
                "user_id" : $('#user_id').val(),
            },
        },
        "columns":columns,
        'columnDefs': columnDefs,
        "language": language,
        dom: '<"pr-3 pl-3 mb-3"Bfrl><"datatable-scroll-wrap"t><"pr-3 pl-3"ip>',
            buttons: [
                {
                    text: '<i class="icon-file-plus"></i>',
                    className: 'ml-3 btn btn-success',
                    titleAttr: '@lang('Create')',
                    action: function(e, dt, node, config) {
                        window.location.href = '{{ route('users.create') }}';
                    }
                }
            ]
    });

    $('div.search-panel').on('change','select',function(e){
        //alert($(this).val());
        tableActive.ajax.reload(null, false);
    })

    $(document).on('click','.ajax-action',function(e){
        e.preventDefault();
        $.ajax({
            url: $(this).attr('href'),
            type: 'GET',
            dataType : "json",
            success: function (data) {
                tableActive.ajax.reload(null, false);
            },
            error: function(){
                console.log('Ajax error');
            }
        });
    });    

</script>
@endsection
@section('modals')
@include('template-parts.modal-ajax-destroy')
@endsection
