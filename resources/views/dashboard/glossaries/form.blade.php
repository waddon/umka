@extends('layouts.admin-layout')

@section('content')

{{ Form::model($model, array('url' => $model->id ? route('glossaries.update', $model->id) : route('glossaries.store'), 'method' => $model->id ? 'PUT' : 'POST')) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header bg-transparent header-elements-inline">
                <h5 class="card-title">@lang('Parameters')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    {{ Form::label('name', __('Name'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('name', null, array('class' => 'field form-control')) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('description', __('Content'), ['class'=>'small text-muted font-italic col-form-label']) }}
                    {{ Form::textarea('description', $model->description, ['class' => 'editor-full field form-control']) }}
                </div>

            </div>
        </div>

    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('Navigation')</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>
                            <span class="small text-muted font-italic">@lang('Created')</span>
                            <span class="float-right">{{$model->created_at}}</span>
                        </p>
                        <p>
                            <span class="small text-muted font-italic">@lang('Updated')</span>
                            <span class="float-right">{{$model->updated_at}}</span>
                        </p>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="@lang('Save')"><i class="icon-floppy-disk"></i></button>
                            <a href="{{ route('glossaries.index') }}" class="btn btn-danger w-50" title="@lang('Exit')"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

{{ Form::close() }}

@endsection

@section('scripts')
@include('template-parts.save_ajax_form')
<script src="/js/plugins/notifications/sweet_alert.min.js""></script>
<script src="/js/plugins/editors/ckeditor/ckeditor.js""></script>

<script type="text/javascript">
$(document).ready(function(){
    CKEDITOR.replaceAll('editor-full', {
        height: 400,
        extraPlugins: 'forms'
    });

    $('form').on('submit', function(e){
        e.preventDefault();
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        save_ajax_form( $(this), "{!! route('glossaries.index') !!}");
    });
});    
</script>
@endsection
