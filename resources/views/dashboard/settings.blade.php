@extends('layouts.admin-layout')

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">@lang('Settings')</h6>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form method="POST">
                @csrf
                <!-- Dashboard Lists -->
                    <h3>@lang('Dashboard Lists')</h3>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Key or Translated Name')</label>
                                <div class="col-sm-8">
                                    <select name="keyOrName" id="keyOrName" class="form-control">
                                        <option value="0" @if(isset($options['keyOrName']) and $options['keyOrName']==0) selected @endif>@lang('Key')</option>
                                        <option value="1" @if(isset($options['keyOrName']) and $options['keyOrName']==1) selected @endif>@lang('Translated Name')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Row Count in Tables')</label>
                                <div class="col-sm-8">
                                    <select name="rowCount" id="rowCount" class="form-control">
                                        <option value="10" @if(isset($options['rowCount']) and $options['rowCount']==10) selected @endif>10</option>
                                        <option value="25" @if(isset($options['rowCount']) and $options['rowCount']==25) selected @endif>25</option>
                                        <option value="50" @if(isset($options['rowCount']) and $options['rowCount']==50) selected @endif>50</option>
                                        <option value="100" @if(isset($options['rowCount']) and $options['rowCount']==100) selected @endif>100</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- /dashboard lists -->
                <!-- Functioning -->
                    <hr>
                    <h3>@lang('Functioning')</h3>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Count of themes while opening module')</label>
                                <div class="col-sm-8">
                                    <select name="themeCount" id="themeCount" class="form-control">
                                        <option value="0" @if(isset($options['themeCount']) and $options['themeCount']==0) selected @endif>@lang('One')</option>
                                        <option value="1" @if(isset($options['themeCount']) and $options['themeCount']==1) selected @endif>@lang('All')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Time before Opening New Theme')</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" name="openNewThemeTime" id="openNewThemeTime" value="{{isset($options['openNewThemeTime']) ? $options['openNewThemeTime'] : '180'}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Guide')</label>
                                <div class="col-sm-8">
                                    <select name="guide" id="guide" class="form-control">
                                        <option value="0">@lang('Select')</option>
                                        @foreach($documents as $key => $document)
                                            <option value="{{$document->id}}" @if(isset($options['guide']) and $options['guide']==$document->id) selected @endif>{{$document->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Copyright')</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="copyright" id="copyright" value="{{isset($options['copyright']) ? $options['copyright'] : ''}}" maxlength="191">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Tick Passed Themes')</label>
                                <div class="col-sm-8">
                                    <select name="tickPassedThemes" id="tickPassedThemes" class="form-control">
                                        <option value="0" @if(isset($options['tickPassedThemes']) and $options['tickPassedThemes']==0) selected @endif>@lang('No')</option>
                                        <option value="1" @if(isset($options['tickPassedThemes']) and $options['tickPassedThemes']==1) selected @endif>@lang('Yes')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Shuffle Questions and Answers')</label>
                                <div class="col-sm-8">
                                    <select name="shuffleQuestions" id="shuffleQuestions" class="form-control">
                                        <option value="0" @if(isset($options['shuffleQuestions']) and $options['shuffleQuestions']==0) selected @endif>@lang('No')</option>
                                        <option value="1" @if(isset($options['shuffleQuestions']) and $options['shuffleQuestions']==1) selected @endif>@lang('Yes')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- /functioning -->
                <!-- Notifications -->
                    <hr>
                    <h3>@lang('Notifications')</h3>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Registration Email')</label>
                                <div class="col-sm-8">
                                    <select name="mailRegistration" id="mailRegistration" class="form-control">
                                        <option value="0" @if(isset($options['mailRegistration']) and $options['mailRegistration']==0) selected @endif>@lang('No')</option>
                                        <option value="1" @if(isset($options['mailRegistration']) and $options['mailRegistration']==1) selected @endif>@lang('Yes')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Message Email')</label>
                                <div class="col-sm-8">
                                    <select name="mailMessage" id="mailMessage" class="form-control">
                                        <option value="0" @if(isset($options['mailMessage']) and $options['mailMessage']==0) selected @endif>@lang('No')</option>
                                        <option value="1" @if(isset($options['mailMessage']) and $options['mailMessage']==1) selected @endif>@lang('Yes')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Module Start Email')</label>
                                <div class="col-sm-8">
                                    <select name="mailModuleStart" id="mailModuleStart" class="form-control">
                                        <option value="0" @if(isset($options['mailModuleStart']) and $options['mailModuleStart']==0) selected @endif>@lang('No')</option>
                                        <option value="1" @if(isset($options['mailModuleStart']) and $options['mailModuleStart']==1) selected @endif>@lang('Yes')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Module Stop Email')</label>
                                <div class="col-sm-8">
                                    <select name="mailModuleStop" id="mailModuleStop" class="form-control">
                                        <option value="0" @if(isset($options['mailModuleStop']) and $options['mailModuleStop']==0) selected @endif>@lang('No')</option>
                                        <option value="1" @if(isset($options['mailModuleStop']) and $options['mailModuleStop']==1) selected @endif>@lang('Yes')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Module Before Closing Email')</label>
                                <div class="col-sm-8">
                                    <select name="mailModuleBeforeClosing" id="mailModuleBeforeClosing" class="form-control">
                                        <option value="0" @if(isset($options['mailModuleBeforeClosing']) and $options['mailModuleBeforeClosing']==0) selected @endif>@lang('No')</option>
                                        <option value="1" @if(isset($options['mailModuleBeforeClosing']) and $options['mailModuleBeforeClosing']==1) selected @endif>@lang('Yes')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Module Before Closing Time (hours)')</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" name="timeModuleBeforeClosing" id="timeModuleBeforeClosing" value="{{isset($options['timeModuleBeforeClosing']) ? $options['timeModuleBeforeClosing'] : '72'}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('User Inactivity Email')</label>
                                <div class="col-sm-8">
                                    <select name="mailUserInactivity" id="mailUserInactivity" class="form-control">
                                        <option value="0" @if(isset($options['mailUserInactivity']) and $options['mailUserInactivity']==0) selected @endif>@lang('No')</option>
                                        <option value="1" @if(isset($options['mailUserInactivity']) and $options['mailUserInactivity']==1) selected @endif>@lang('Yes')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('User Inactivity Time (hours)')</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" name="timeUserInactivity" id="timeUserInactivity" value="{{isset($options['timeUserInactivity']) ? $options['timeUserInactivity'] : '144'}}">
                                </div>
                            </div>
                        </div>

                    </div>
                <!-- /notifications -->

                <button type="submit" class="btn btn-primary">@lang('Save')</button>
            </form>
        </div>
    </div>
@endsection
