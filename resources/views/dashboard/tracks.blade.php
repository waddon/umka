@extends('layouts.admin-layout')

@section('content')
    <div class="card mb-3">

            <div class="tab-content">
                <div class="tab-pane fade show active" id="tab-active">
                    <div class="table-responsive">
                        <table class="table datatable-basic table-striped dataTable no-footer w-100" id="datatableActive">
                            <thead>
                                <tr>
                                    <th>@lang('Date')</th>
                                    <th>@lang('User')</th>
                                    <th>@lang('IP')</th>
                                    <th>@lang('Description')</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

    </div>

@endsection

@section('scripts')
<script>
    var language = {"url": "/js/plugins/tables/datatables/languages/{{app()->getLocale()}}.json"};
    var pageLength = {{\App\Option::getOption('rowCount',10)}};
    var columns = [
            {"data":"date"},
            {"data":"user"},
            {"data":"ip"},
            {"data":"name"}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 2, "className": "text-center", },
        ];


    var tableActive = $("#datatableActive").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        "bSortCellsTop": true,
        "order": [[ 0, "desc" ]],
        ajax: {
            "url": "{{route('tracklist')}}",
            "dataType": "json",
            "type": "POST",
            "data": {"_token":"{{ csrf_token() }}", "table":"active"},
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
        dom: '<"datatable-header"Bfrl><"datatable-scroll"t><"datatable-footer pr-3 pl-3"ip>',
            buttons: [
                {
                    text: '<i class="icon-eraser2"></i> @lang('Clear') @lang('Event Log')',
                    className: 'ml-3 btn btn-success',
                    titleAttr: '@lang('Clear') @lang('Event Log')',
                    action: function(e, dt, node, config) {
                        $('#clear-event-list').submit();
                    }
                }
            ]
    });

</script>
@endsection

@section('modals')
<form action="{{route('tracks')}}" method="POST" id="clear-event-list">@csrf</form>
@endsection
