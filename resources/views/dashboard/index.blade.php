@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col-md-4">
            @widget('users')
        </div>
        <div class="col-md-4">
            @widget('groups')
        </div>
        <div class="col-md-4">
            @widget('modules')
        </div>
    </div>
@endsection

