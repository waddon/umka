@extends('layouts.admin-layout')

@section('content')
    <div class="card mb-3">

        {{--<div class="pl-3 pt-3 pr-3">
            <div class="row search-panel">
                <div class="col-lg-6"></div>
                <div class="col-lg-6">
                    <div class="form-group row mb-0">
                        <label for="" class="col-form-label col-sm-2">@lang('Course')</label>
                        <div class="col-sm-10">
                            <select id="course_id" class="form-control">
                                <option value="0">@lang('All Courses')</option>
                                @foreach($courses as $key => $course)
                                    <option value="{{$key}}">{{$course}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>--}}

            <div class="tab-content">
                <div class="tab-pane fade show active" id="tab-active">
                    <div class="table-responsive">
                        <table class="table datatable-basic table-striped dataTable no-footer w-100" id="datatableActive">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('Name')</th>
                                    <th>@lang('Themes')</th>
                                    <th>@lang('Active Themes')</th>
                                    <th>@lang('Groups')</th>
                                    <th>@lang('Actions')</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

    </div>

@endsection

@section('scripts')
<script>
    var language = {"url": "/js/plugins/tables/datatables/languages/{{app()->getLocale()}}.json"};
    var pageLength = {{\App\Option::getOption('rowCount',10)}};
    var columns = [
            {"data":"id"},
            {"data":"name"},
            {"data":"themes"},
            {"data":"activethemes"},
            {"data":"runs"},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 2, "className": "text-center", },
            { "targets": 3, "className": "text-center", },
            { "targets": 4, "className": "text-center", },
            { "targets": 5, "className": "text-right", },
        ];


    var tableActive = $("#datatableActive").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        // "bSortCellsTop": true,
        // "order": [[ 0, "desc" ]],
        ajax: {
            "url": "{{route('modules.datatable')}}",
            "dataType": "json",
            "type": "POST",
            //"data": {"_token":"{{ csrf_token() }}", "table":"active"},
            "data": function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.course_id = $('#course_id').val();
                d.table = "active";
            }            
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
        dom: '<"datatable-header"Bfrl><"datatable-scroll"t><"datatable-footer pr-3 pl-3"ip>',
            buttons: [
                {
                    text: '<i class="icon-file-plus"></i>',
                    className: 'ml-3 btn btn-success',
                    titleAttr: '@lang('Create')',
                    action: function(e, dt, node, config) {
                        window.location.href = '{{ route('modules.create') }}';
                    }
                }
            ]
    });

    $('div.search-panel').on('change','select',function(e){
        tableActive.ajax.reload(null, false);
    })

</script>
@endsection
@section('modals')
@include('template-parts.modal-ajax-destroy')
@include('template-parts.run-module')
@endsection
