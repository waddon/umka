@extends('layouts.admin-layout')

@section('content')

{{ Form::model($model, array('url' => $model->id ? route('modules.update', $model->id) : route('modules.store'), 'method' => $model->id ? 'PUT' : 'POST')) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header bg-transparent header-elements-inline">
                <h5 class="card-title">@lang('Parameters')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <ul class="nav nav-tabs nav-tabs-highlight">
                    <li class="nav-item"><a href="#general" class="nav-link active" data-toggle="tab">@lang('General')</a></li>
                    <li class="nav-item"><a href="#materials" class="nav-link" data-toggle="tab">@lang('Materials & Tasks')</a></li>
                    <li class="nav-item"><a href="#testing" class="nav-link" data-toggle="tab">@lang('Testing')</a></li>
                </ul>
                <div class="tab-content">
                    <!-- General tab -->
                    <div class="tab-pane fade show active" id="general">
                        <div class="form-group row">
                            {{ Form::label('key', __('Key'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::text('key', null, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('course_id', __('Course'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::select('course_id', $courses, null, array('class' => 'field form-control')) }}
                            </div>
                        </div>

                        @if(count(config()->get('app.locales'))>1)
                            <ul class="nav nav-tabs nav-tabs-highlight">
                                @foreach(config()->get('app.locales') as $key => $language)
                                    <li class="nav-item"><a href="#tab-{{$language}}" class="nav-link @if($key==0) active @endif" data-toggle="tab"><img src="/assets/img/lang/{{$language}}.png" alt="" class="img-flag mr-2"> @lang($language)</a></li>
                                @endforeach
                            </ul>
                        @endif
                        <div class="tab-content">
                            @foreach(config()->get('app.locales') as $key => $language)
                                <div class="tab-pane fade @if($key==0) show active @endif" id="tab-{{$language}}">
                                    <div class="form-group row">
                                        {{ Form::label('name-'.$language, __('Name'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                                        <div class="col-sm-8">
                                            {{ Form::text('name-'.$language, (isset($model->translate($language)->name) ? $model->translate($language)->name : null), ['class' => 'field form-control']) }}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- /general tab -->
                    <!-- Materials & Tasks tab -->
                    <div class="tab-pane fade" id="materials">

                        <div class="form-group row">
                            {{ Form::label('introduction_id', __('Introduction'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::select('introduction_id', $pages, isset($model->data->introduction_id) ? $model->data->introduction_id : 0, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('conclusion_id', __('Conclusion'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::select('conclusion_id', $pages, isset($model->data->conclusion_id) ? $model->data->conclusion_id : 0, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('literature_id', __('Literature'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::select('literature_id', $pages, isset($model->data->literature_id) ? $model->data->literature_id : 0, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('webquestions', __('Questions to the Webinar'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::select('webquestions', [__('No'),__('Yes')], isset($model->data->webquestions) ? $model->data->webquestions : 0, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('homework', __('Homework'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::select('homework', [__('No'),__('Yes')], isset($model->data->homework) ? $model->data->homework : 0, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('homework_id', __('Homework Page'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::select('homework_id', $pages, isset($model->data->homework_id) ? $model->data->homework_id : 0, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                    </div>
                    <!-- /materials & tasks tab -->
                    <!-- Testing tab -->
                    <div class="tab-pane fade" id="testing">
                        <div class="form-group row">
                            {{ Form::label('required_webquestions', __('Required Webquestions'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::select('required_webquestions', [__('No'),__('Yes')], isset($model->data->required_webquestions) ? $model->data->required_webquestions : 0, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('question_count', __('Maximum question count'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::number('question_count', isset($model->data->question_count) ? $model->data->question_count : 0, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('point_score', __('Passing Score'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-4">
                                {{ Form::number('point_score', isset($model->data->point_score) ? $model->data->point_score : 0, array('class' => 'field form-control')) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::select('point_one', \App\Module::point_ones(), isset($model->data->point_one) ? $model->data->point_one : 0, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('time_for_passing', __('Passing Time'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-4">
                                {{ Form::number('time_for_passing', isset($model->data->time_for_passing) ? $model->data->time_for_passing : 0, array('class' => 'field form-control')) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::select('time_for_passing_one', \App\Module::time_ones(), isset($model->data->time_for_passing_one) ? $model->data->time_for_passing_one : 0, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('time_between_passing', __('Time between Passing'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-4">
                                {{ Form::number('time_between_passing', isset($model->data->time_between_passing) ? $model->data->time_between_passing : 0, array('class' => 'field form-control')) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::select('time_between_passing_one', \App\Module::time_ones(), isset($model->data->time_between_passing_one) ? $model->data->time_between_passing_one : 0, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('max_count_passing', __('Maximum count passing'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::number('max_count_passing', isset($model->data->max_count_passing) ? $model->data->max_count_passing : 0, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('exam_question_count', __('Exam Count Question'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::number('exam_question_count', isset($model->data->exam_question_count) ? $model->data->exam_question_count : 0, array('class' => 'field form-control')) }}
                            </div>
                        </div>

                        @if(count(config()->get('app.locales'))>1)
                            <ul class="nav nav-tabs nav-tabs-highlight">
                                @foreach(config()->get('app.locales') as $key => $language)
                                    <li class="nav-item"><a href="#mess-{{$language}}" class="nav-link @if($key==0) active @endif" data-toggle="tab"><img src="/assets/img/lang/{{$language}}.png" alt="" class="img-flag mr-2"> @lang($language)</a></li>
                                @endforeach
                            </ul>
                        @endif
                        <div class="tab-content">
                            @foreach(config()->get('app.locales') as $key => $language)
                                <div class="tab-pane fade @if($key==0) show active @endif" id="mess-{{$language}}">
                                    <div class="form-group row">
                                        {{ Form::label('welcome-'.$language, __('Welcome message'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                                        <div class="col-md-8">
                                            {{ Form::textarea('welcome-'.$language, (isset($model->translate($language)->messages->welcome) ? $model->translate($language)->messages->welcome : null), ['class' => 'editor-full field form-control', 'rows' => 5]) }}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{ Form::label('success-'.$language, __('Success message'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                                        <div class="col-md-8">
                                            {{ Form::textarea('success-'.$language, (isset($model->translate($language)->messages->success) ? $model->translate($language)->messages->success : null), ['class' => 'editor-full field form-control', 'rows' => 5]) }}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{ Form::label('failure-'.$language, __('Failure message'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                                        <div class="col-md-8">
                                            {{ Form::textarea('failure-'.$language, (isset($model->translate($language)->messages->failure) ? $model->translate($language)->messages->failure : null), ['class' => 'editor-full field form-control', 'rows' => 5]) }}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{ Form::label('already-'.$language, __('Already message'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                                        <div class="col-md-8">
                                            {{ Form::textarea('already-'.$language, (isset($model->translate($language)->messages->already) ? $model->translate($language)->messages->already : null), ['class' => 'editor-full field form-control', 'rows' => 5]) }}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{ Form::label('exhausted-'.$language, __('Exhausted message'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                                        <div class="col-md-8">
                                            {{ Form::textarea('exhausted-'.$language, (isset($model->translate($language)->messages->exhausted) ? $model->translate($language)->messages->exhausted : null), ['class' => 'editor-full field form-control', 'rows' => 5]) }}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{ Form::label('wait-'.$language, __('Wait message'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                                        <div class="col-md-8">
                                            {{ Form::textarea('wait-'.$language, (isset($model->translate($language)->messages->wait) ? $model->translate($language)->messages->wait : null), ['class' => 'editor-full field form-control', 'rows' => 5]) }}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>
                    <!-- /testing tab -->
                </div>

            </div>
        </div>

    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('Navigation')</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>
                            <span class="small text-muted font-italic">@lang('Created')</span>
                            <span class="float-right">{{$model->created_at}}</span>
                        </p>
                        <p>
                            <span class="small text-muted font-italic">@lang('Updated')</span>
                            <span class="float-right">{{$model->updated_at}}</span>
                        </p>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="@lang('Save')"><i class="icon-floppy-disk"></i></button>
                            <a href="{{ route('modules.index') }}" class="btn btn-danger w-50" title="@lang('Exit')"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

{{ Form::close() }}

@endsection

@section('scripts')
@include('template-parts.save_ajax_form')
<script src="/js/plugins/notifications/sweet_alert.min.js""></script>
<script src="/js/plugins/editors/ckeditor2019/ckeditor.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $('.editor-full').each(function(e){
        CKEDITOR.replace(this.id, {
            height: 200,
            //extraPlugins: 'forms',

        });
    });    
    $('form').on('submit', function(e){
        e.preventDefault();
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        save_ajax_form( $(this), "{!! route('modules.index') !!}");
    });
});    
</script>
@endsection
