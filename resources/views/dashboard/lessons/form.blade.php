@extends('layouts.admin-layout')

@section('content')

{{ Form::model($model, array('url' => $model->id ? route('lessons.update', $model->id) : route('lessons.store'), 'method' => $model->id ? 'PUT' : 'POST')) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header bg-transparent header-elements-inline">
                <h5 class="card-title">@lang('Parameters')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <div class="tab-content">
                    <!-- General tab -->
                    <div class="tab-pane fade show active" id="general">
                        <div class="form-group row">
                            {{ Form::label('key', __('Key'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::text('key', null, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('module_id', __('Module'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::select('module_id', $modules, null, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('introduction_id', __('Introduction'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::select('introduction_id', $pages, isset($model->data->introduction_id) ? $model->data->introduction_id : 0, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('conclusion_id', __('Conclusion'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::select('conclusion_id', $pages, isset($model->data->conclusion_id) ? $model->data->conclusion_id : 0, array('class' => 'field form-control')) }}
                            </div>
                        </div>

                        @if(count(config()->get('app.locales'))>1)
                            <ul class="nav nav-tabs nav-tabs-highlight">
                                @foreach(config()->get('app.locales') as $key => $language)
                                    <li class="nav-item"><a href="#tab-{{$language}}" class="nav-link @if($key==0) active @endif" data-toggle="tab"><img src="/assets/img/lang/{{$language}}.png" alt="" class="img-flag mr-2"> @lang($language)</a></li>
                                @endforeach
                            </ul>
                        @endif
                        <div class="tab-content">
                            @foreach(config()->get('app.locales') as $key => $language)
                                <div class="tab-pane fade @if($key==0) show active @endif" id="tab-{{$language}}">
                                    <div class="form-group row">
                                        {{ Form::label('name-'.$language, __('Name'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                                        <div class="col-sm-8">
                                            {{ Form::text('name-'.$language, (isset($model->translate($language)->name) ? $model->translate($language)->name : null), ['class' => 'field form-control']) }}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- /general tab -->
                </div>

            </div>
        </div>

    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('Navigation')</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>
                            <span class="small text-muted font-italic">@lang('Created')</span>
                            <span class="float-right">{{$model->created_at}}</span>
                        </p>
                        <p>
                            <span class="small text-muted font-italic">@lang('Updated')</span>
                            <span class="float-right">{{$model->updated_at}}</span>
                        </p>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="@lang('Save')"><i class="icon-floppy-disk"></i></button>
                            <a href="{{ route('lessons.index') }}" class="btn btn-danger w-50" title="@lang('Exit')"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

{{ Form::close() }}

@endsection

@section('scripts')
@include('template-parts.save_ajax_form')
<script src="/js/plugins/notifications/sweet_alert.min.js""></script>

<script type="text/javascript">
$(document).ready(function(){
    $('form').on('submit', function(e){
        e.preventDefault();
        save_ajax_form( $(this), "{!! route('lessons.index') !!}");
    });
});    
</script>
@endsection
