@extends('layouts.admin-layout')

@section('content')

    {{--<div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        @lang('Warning! Adding a new theme, making changes to a theme, activating a theme or changing the order in which themes are displayed will deactivate the module and reset the user’s progress on this module.')
    </div>--}}

    <div class="card mb-3">

        <div class="pl-3 pt-3 pr-3">
            <div class="row search-panel">
                <div class="col-lg-6">
                    <div class="form-group row mb-0">
                        <label for="" class="col-form-label col-sm-2">@lang('Module')</label>
                        <div class="col-sm-10">
                            <select id="module_id" class="form-control">
                                <option value="0">@lang('All Modules')</option>
                                @foreach($modules as $key => $module)
                                    <option value="{{$key}}">{{$module}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group row mb-0">
                        <label for="" class="col-form-label col-sm-2">@lang('Lesson')</label>
                        <div class="col-sm-10">
                            <select id="lesson_id" class="form-control">
                                <option value="0">@lang('All Lessons')</option>
                                @foreach($lessons as $key => $lesson)
                                    <option value="{{$key}}">{{$lesson}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>

        <div class="tab-content">
            <div class="tab-pane fade show active" id="tab-active">
                <div class="table-responsive">
                    <table class="table datatable-basic table-striped dataTable no-footer w-100" id="datatableActive">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('Order in Module')</th>
                                <th>@lang('Name')</th>
                                <th>@lang('Module')</th>
                                <th>@lang('Activity')</th>
                                <th>@lang('Actions')</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
<script>
    var language = {"url": "/js/plugins/tables/datatables/languages/{{app()->getLocale()}}.json"};
    var pageLength = {{\App\Option::getOption('rowCount',10)}};
    var columns = [
            {"data":"id"},
            {"data":"order"},
            {"data":"name"},
            {"data":"module"},
            {"data":"activity"},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 1, "className": "text-center", },
            { "targets": 4, "className": "text-center", },
            { "targets": 5, "className": "text-right", },
        ];


    var tableActive = $("#datatableActive").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        // "bSortCellsTop": true,
        // "order": [[ 0, "desc" ]],
        ajax: {
            "url": "{{route('themes.datatable')}}",
            "dataType": "json",
            "type": "POST",
            "data": function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.lesson_id = $('#lesson_id').val();
                d.module_id = $('#module_id').val();
                d.table = "active";
            }            
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
        dom: '<"datatable-header"Bfrl><"datatable-scroll"t><"datatable-footer pr-3 pl-3"ip>',
            buttons: [
                {
                    text: '<i class="icon-file-plus"></i>',
                    className: 'ml-3 btn btn-success',
                    titleAttr: '@lang('Create')',
                    action: function(e, dt, node, config) {
                        window.location.href = '{{ route('themes.create') }}';
                    }
                }
            ],
    });

    $('div.search-panel').on('change','select',function(e){
        console.log($('#module_id').val());
        tableActive.ajax.reload(null, false);
    })

    $(document).on('click','.ajax-action',function(e){
        e.preventDefault();
        $.ajax({
            url: $(this).attr('href'),
            type: 'GET',
            dataType : "json",
            success: function (data) {
                tableActive.ajax.reload(null, false);
            },
            error: function(){
                console.log('Ajax error');
            }
        });
    });    

</script>
@endsection
@section('modals')
@include('template-parts.modal-ajax-destroy')
@endsection

