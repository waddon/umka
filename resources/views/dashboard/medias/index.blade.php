@extends('layouts.admin-layout')

@section('content')
    <div class="card mb-3">

        <div class="pl-3 pt-3 pr-3">
            <div class="row search-panel">
                <div class="col-lg-6"></div>
                <div class="col-lg-6">
                    <div class="form-group row mb-0">
                        <label for="" class="col-form-label col-sm-2">@lang('Types')</label>
                        <div class="col-sm-10">
                            <select id="type" class="form-control">
                                <option value="0">@lang('All types')</option>
                                @foreach($types as $key => $type)
                                    <option value="{{$type['type']}}">{{$type['type']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

            <div class="tab-content">
                <div class="tab-pane fade show active" id="tab-active">
                    <div class="table-responsive">
                        <table class="table datatable-basic table-striped dataTable no-footer w-100" id="datatableActive">
                            <thead>
                                <tr>
                                    <th>@lang('Preview')</th>
                                    <th>@lang('Name')</th>
                                    {{--<th>@lang('Description')</th>--}}
                                    <th>@lang('File info')</th>
                                    <th>@lang('Created')</th>
                                    <th>@lang('Actions')</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
    </div>


       <div class="card mb-3 card-collapsed">
            <div class="card-header bg-transparent header-elements-inline">
                <h5 class="card-title">@lang('Dropdown Area')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="{{route('medias.upload')}}" method="POST" class="dropzone" id="dropzone_multiple">@csrf</form>
            </div>
        </div>

@endsection

@section('scripts')
<script src="/js/plugins/media/fancybox.min.js"></script>
<script>
    var language = {"url": "/js/plugins/tables/datatables/languages/{{app()->getLocale()}}.json"};
    var pageLength = {{\App\Option::getOption('rowCount',10)}};
    var columns = [
            {"data":"preview","searchable":false,"orderable":false},
            {"data":"name"},
            //{"data":"description"},
            {"data":"fileinfo","searchable":false,"orderable":false},
            {"data":"created"},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 3, "className": "text-center", },
            { "targets": 4, "className": "text-right", },
        ];


    var tableActive = $("#datatableActive").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        "bSortCellsTop": true,
        "order": [[ 3, "desc" ]],
        ajax: {
            "url": "{{route('medias.datatable')}}",
            "dataType": "json",
            "type": "POST",
            // "data": {"_token":"{{ csrf_token() }}", "table":"active"},
            "data": function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.type = $('#type').val();
                d.table = "active";
            }            

        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
        dom: '<"datatable-header"frl><"datatable-scroll"t><"datatable-footer pr-3 pl-3"ip>',
    }).on( 'draw.dt', function () {
        $(document).unbind('click.fb-start');
        $('[data-popup="lightbox"]').fancybox({padding: 3});
    });

    var DropzoneUploader = function() {
        var _componentDropzone = function() {
            if (typeof Dropzone == 'undefined') {
                console.warn('Warning - dropzone.min.js is not loaded.');
                return;
            }
            Dropzone.options.dropzoneMultiple = {
                paramName: "file", // The name that will be used to transfer the file
                dictDefaultMessage: "@lang('Drop files to upload <span>or CLICK</span>')",
                addRemoveLinks: true,
                init: function() {
                    this.on("complete", function(file) { 
                        //alert("Сomplete file.");
                        tableActive.ajax.reload(null, false); 
                    });
                }
            };
        };
        return {
            init: function() {
                _componentDropzone();
            }
        }
    }();
    DropzoneUploader.init();

</script>
@endsection

@section('modals')
@include('template-parts.modal-ajax-destroy')
@include('template-parts.modal-ajax-edit')
@endsection
