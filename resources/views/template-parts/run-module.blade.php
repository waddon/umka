<div id="modal_form_horizontal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h6 class="modal-title">@lang('Run module')</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form action="{{route('run')}}" class="form-horizontal" id="form-modal-run-module">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="module_id" id="module_id">
                    <input type="hidden" name="group_id" id="group_id" value="{{$model->id ?? 0}}">
                    {{--<div class="form-group row">
                        <label class="col-form-label col-sm-3">@lang('Module')</label>
                        <div class="col-sm-9">
                            <select name="module_id" id="module_id" class="form-control" required="">
                                @if(isset($modules))
                                    @foreach($modules as $key => $module)
                                        <option value="{{$module->id}}">{{$module->translate(app()->getLocale())->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-sm-3">@lang('Group')</label>
                        <div class="col-sm-9">
                            <select name="group_id" id="group_id" class="form-control" required="">
                                @if(isset($model->id))
                                    <option value="{{$model->id}}">{{$model->name}}</option>
                                @endif
                                @if(isset($groups))
                                    @foreach($groups as $key => $group)
                                        <option value="{{$group->id}}">{{$group->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>--}}

                    <div class="form-group row">
                        <label class="col-form-label col-sm-3">@lang('Date start')</label>
                        <div class="col-sm-9">
                            <input type="date" name="date_start" id="date_start" class="form-control" required="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-sm-3">@lang('Date finish')</label>
                        <div class="col-sm-9">
                            <input type="date" name="date_finish" id="date_finish" class="form-control" required="">
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">@lang('Cancel')</button>
                    <button type="submit" class="btn bg-success">@lang('Run')</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click','.btn-run-module',function(e){
            $('#module_id').val($(this).attr('data-id')).change();
        });

        $('#form-modal-run-module').submit(function(e){
            e.preventDefault();
            console.log($('#form-modal-run-module').serialize());
            $('#modal_form_horizontal').modal('toggle');
            $.ajax({
                url: $('#form-modal-run-module').attr('action'),
                type: 'POST',
                dataType : "json",
                data: $('#form-modal-run-module').serialize(),
                success: function (data) {
                    // tableActive.ajax.reload(null, false);
                    // document.location.reload(true);
                    var element = $('.modules-list [data-module-id='+data.module_id+']');
                    el1 = element.find('[data-name=date_start]').html(data.date_start);
                    el2 = element.find('[data-name=date_finish]').html(data.date_finish);
                    if (data.active){
                        el1.addClass('text-success');
                        el2.addClass('text-success');
                    } else {
                        el1.removeClass('text-success');
                        el2.removeClass('text-success');
                    }
                    if(data.date_start!='' && data.date_finish!=''){
                        element.find('[data-name=actions]').html('<a href="{{route('run')}}" class="btn btn-warning btn-run-module" data-toggle="modal" data-target="#modal_form_horizontal" data-id="'+data.module_id+'">@lang('Change')</a>');
                    } else {
                        element.find('[data-name=actions]').html('<a href="{{route('run')}}" class="btn btn-success btn-run-module" data-toggle="modal" data-target="#modal_form_horizontal" data-id="'+data.module_id+'">@lang('Activate')</a>');
                    }
                },
                error: function(){
                    console.log('Ajax error');
                }
            });
        });

    })
</script>
