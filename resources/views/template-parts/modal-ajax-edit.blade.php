<div id="modal-ajax-edit" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h6 class="modal-title">@lang('Change element')</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form id="form-modal-ajax-edit" action="{{ route('logout') }}" method="POST">@csrf
                <input name="_method" type="hidden" value="PUT">
                <div class="modal-body" id="remove-modal-body">
                    <div class="form-group">
                        <label for="name">@lang('Name')</label>
                        <input type="text" class="form-control" name="name" id="name">
                    </div>
                    <div class="form-group">
                        <label for="description">@lang('Description')</label>
                        <input type="text" class="form-control" name="description" id="description">
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">@lang('Cancel')</button>
                <button type="button" class="btn bg-primary" data-dismiss="modal" id="modal-ajax-edit-submit">@lang('Change')</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click','.btn-ajax-edit',function(){
            $('#form-modal-ajax-edit').attr('action',$(this).attr('href'));
            $('#form-modal-ajax-edit').find('[name=name]').val($(this).attr('data-name'));
            $('#form-modal-ajax-edit').find('[name=description]').val($(this).attr('data-description'));
        });

        $('#modal-ajax-edit-submit').click(function(){
            console.log($('#form-modal-ajax-edit').serialize());
            $.ajax({
                url: $('#form-modal-ajax-edit').attr('action'),
                type: 'POST',
                dataType : "json",
                data: $('#form-modal-ajax-edit').serialize(),
                success: function (data) {
                    tableActive.ajax.reload(null, false);
                    console.log(data);
                },
                error: function(){
                    console.log('Ajax error');
                }
            });
        });

    })
</script>
