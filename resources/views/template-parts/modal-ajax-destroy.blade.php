<div id="modal-ajax-destroy" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title">@lang('Delete element')</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="remove-modal-body">
                @lang('Are you sure you want to permanently delete the item? Recovery of the item will be impossible.')
            </div>
            <form id="form-modal-ajax-destroy" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf
            <input name="_method" type="hidden" value="DELETE"></form>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">@lang('Cancel')</button>
                <button type="button" class="btn bg-danger" data-dismiss="modal" id="modal-ajax-destroy-submit">@lang('Delete')</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click','.btn-ajax-destroy',function(){
            $('#form-modal-ajax-destroy').attr('action',$(this).attr('href'));
        });

        $('#modal-ajax-destroy-submit').click(function(){
            $.ajax({
                url: $('#form-modal-ajax-destroy').attr('action'),
                type: 'POST',
                dataType : "json",
                data: $('#form-modal-ajax-destroy').serialize(),
                success: function (data) {
                    tableActive.ajax.reload(null, false);
                    //tableTrash.ajax.reload(null, false);
                },
                error: function(){
                    console.log('Ajax error');
                }
            });
        });

    })
</script>
