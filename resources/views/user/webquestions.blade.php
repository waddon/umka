@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body content-area">
            <h1 class="text-center">@lang('Questions to the Webinar')</h1>

            @if(isset(auth()->user()->data->webquestions) && is_array(auth()->user()->data->webquestions))
                <table class="table mb-5">
                    <thead><tr><td>@lang('Question')</td><td>@lang('Module')</td></tr></thead>
                    @foreach(auth()->user()->data->webquestions as $key => $webquestion)
                        <tr><td>{{$webquestion->text}}</td><td>{{$webquestion->module}}</td></tr>
                    @endforeach
                </table>
            @endif
            <form action="{{$route}}" method="POST">
                @csrf
                <textarea name="content" class="form-control mb-3" rows="3" cols="1" placeholder="@lang('Enter your question...')" maxlength="191"></textarea>
                <div class="d-flex align-items-center">
                    <button type="submit" class="btn bg-success btn-labeled btn-labeled-right ml-auto"><b><i class="icon-paperplane"></i></b> @lang('Send')</button>
                </div>
            </form>
        </div>
    </div>
@endsection
