@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body content-area">
            <h1 class="text-center">@lang('Profile')</h1>
            <form action="{{route('profile')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-xl-3">
                        <label for="loadavatar" class="rounded-circle avatar"><img id="user-photo" src="{{ auth()->user()->getAvatar() }}" alt=""></label>
                        <input type="file" name="loadavatar" id="loadavatar">
                        @foreach($courses as $key => $course)
                            @if (isset($model->data->courses->{$course->id}->passed))
                                @if ( ($url = $course->getCertificateUrl()) != '#')
                                    <a href="{{$url}}" target="_blank" class="btn btn-outline-warning w-100 mt-3" style="white-space: normal;"><i class="icon-download4 mr-2"></i> @lang('Download Certificate')</a>
                                @endif
                            {{--<button type="button" class="btn btn-outline-warning w-100 mt-3" style="white-space: normal;"><i class="icon-download4 mr-2"></i> @lang('Download Certificate')</button>--}}
                            @endif
                        @endforeach
                    </div>
                    <div class="col-xl-9">
                                <div class="row">
                                    <div class="col-lg-6 form-group {{ $errors->has('pin') ? 'has-danger' : '' }}">
                                        {{ Form::label('pin', __('Pin'), ['class'=>'small text-muted font-italic']) }}
                                        {{ Form::text('pin', $model->pin, array('class' => 'form-control disabled'. ($errors->has('pin') ? ' is-invalid' : '' ), 'disabled' => 'disabled')) }}
                                        @if($errors->has('pin'))
                                            @foreach ($errors->get('pin') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-6 form-group {{ $errors->has('name') ? 'has-danger' : '' }}">
                                        {{ Form::label('name', __('Name'), ['class'=>'small text-muted font-italic']) }}
                                        {{ Form::text('name', $model->name, array('class' => 'form-control disabled'. ($errors->has('name') ? ' is-invalid' : '' ), 'disabled' => 'disabled')) }}
                                        @if($errors->has('name'))
                                            @foreach ($errors->get('name') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-6 form-group {{ $errors->has('birthday') ? 'has-danger' : '' }}">
                                        {{ Form::label('birthday', __('Birthday'), ['class'=>'small text-muted font-italic']) }}
                                        {{ Form::date('birthday', $model->birthday, array('class' => 'form-control disabled'. ($errors->has('birthday') ? ' is-invalid' : '' ), 'disabled' => 'disabled')) }}
                                        @if($errors->has('birthday'))
                                            @foreach ($errors->get('birthday') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-6 form-group {{ $errors->has('passport') ? 'has-danger' : '' }}">
                                        {{ Form::label('passport', __('Passport'), ['class'=>'small text-muted font-italic']) }}
                                        {{ Form::text('passport', $model->passport, array('class' => 'form-control disabled'. ($errors->has('passport') ? ' is-invalid' : '' ), 'disabled' => 'disabled' )) }}
                                        @if($errors->has('passport'))
                                            @foreach ($errors->get('passport') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-6 form-group">
                                        {{ Form::label('category_id', __('Category'), ['class'=>'small text-muted font-italic']) }}<br>
                                        {{ Form::select('category_id', $model->getCategories(), $model->category_id, array('class' => 'form-control'. ($errors->has('category_id') ? ' is-invalid' : ''))) }}
                                        @if($errors->has('category_id'))
                                            @foreach ($errors->get('category_id') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-6 form-group {{ $errors->has('position') ? 'has-danger' : '' }}">
                                        {{ Form::label('position', __('Position'), ['class'=>'small text-muted font-italic']) }}
                                        {{ Form::text('position', $model->position, array('class' => 'form-control'. ($errors->has('position') ? ' is-invalid' : '' ))) }}
                                        @if($errors->has('position'))
                                            @foreach ($errors->get('position') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-6 form-group {{ $errors->has('email') ? 'has-danger' : '' }}">
                                        {{ Form::label('email', __('Email'), ['class'=>'small text-muted font-italic']) }}
                                        {{ Form::email('email', $model->email, array('class' => 'form-control'. ($errors->has('email') ? ' is-invalid' : '' ))) }}
                                        @if($errors->has('email'))
                                            @foreach ($errors->get('email') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-6 form-group {{ $errors->has('phone') ? 'has-danger' : '' }}">
                                        {{ Form::label('phone', __('Phone'), ['class'=>'small text-muted font-italic']) }}
                                        {{ Form::text('phone', $model->phone, array('class' => 'form-control'. ($errors->has('phone') ? ' is-invalid' : '' ))) }}
                                        @if($errors->has('phone'))
                                            @foreach ($errors->get('phone') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>

                                    <div class="col-lg-6 form-group {{ $errors->has('password') ? 'has-danger' : '' }}">
                                        {{ Form::label('password', __('New Password'), ['class'=>'small text-muted font-italic']) }}
                                        {{ Form::password('password', array('class' => 'form-control'. ($errors->has('password') ? ' is-invalid' : '' ))) }}
                                        @if($errors->has('password'))
                                            @foreach ($errors->get('password') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-6 form-group {{ $errors->has('password_confirmation') ? 'has-danger' : '' }}">
                                        {{ Form::label('password_confirmation', __('Confirm Password'), ['class'=>'small text-muted font-italic']) }}
                                        {{ Form::password('password_confirmation', array('class' => 'form-control'. ($errors->has('password_confirmation') ? ' is-invalid' : '' ))) }}
                                        @if($errors->has('password_confirmation'))
                                            @foreach ($errors->get('password_confirmation') as $message)
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @endforeach
                                        @endif
                                    </div>

                                </div>
                    </div>
                </div>
                <div class="text-right">
                    <button class="btn btn-success">@lang('Save')</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function(){

    $('input[type=file]').change(function(){
        files = this.files;
        readURL(this);
    });

    /* отображаем загруженную картинку */
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#user-photo').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

});
</script>
@endsection
