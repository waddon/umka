@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body content-area">
            <h1 class="text-center">@lang('Message List')</h1>
            <ul class="media-list media-chat mb-3">
                @foreach(\App\Message::getMessages(auth()->user()->id) as $key => $message)
                    @if($message->sender_id != 0)
                        <li class="media media-chat-item-reverse">
                            <div class="media-body">
                                <div class="media-chat-item">{{$message->content}}</div>
                                <div class="font-size-sm text-muted mt-2">{{$message->created_at->format('d.m.Y H:i')}}</div>
                            </div>
                            <div class="ml-3">
                                <img src="{{auth()->user()->getAvatar()}}" class="rounded-circle" width="40" height="40" alt="">
                            </div>
                        </li>
                    @else
                        <li class="media">
                            <div class="mr-3">
                                <img src="/assets/img/logo.png" class="rounded-circle" width="40" height="40" alt="">
                            </div>
                            <div class="media-body">
                                <div class="media-chat-item">{{$message->content}}</div>
                                <div class="font-size-sm text-muted mt-2">{{$message->created_at->format('d.m.Y H:i')}}</div>
                            </div>
                        </li>
                    @endif
                @endforeach
            </ul>
            <form action="{{route('message')}}" method="POST">
                @csrf
                <textarea name="content" class="form-control mb-3" rows="3" cols="1" placeholder="@lang('Enter your message...')" maxlength="191"></textarea>
                <div class="d-flex align-items-center">
                    <button type="submit" class="btn bg-success btn-labeled btn-labeled-right ml-auto"><b><i class="icon-paperplane"></i></b> @lang('Send')</button>
                </div>
            </form>
        </div>
    </div>
@endsection
