@extends('layouts.app')

@section('content')
<style>
    .glossary a{
        font-size: 1.5em;
        color: #333;
        display: inline-block;
        border-bottom: 1px solid #F4511E;
        padding: 0 5px;
    }
    .glossary a.active, .glossary a:hover{
        color: #F4511E;
    }
</style>
    <div class="card glossary">
        <div class="card-body">
            <h2 class="text-center">@lang('Glossary')</h2>
            <div class="text-center">
                @foreach (range(chr(0xC0), chr(0xDF)) as $key => $char1)
                    <a href="{{route('glossary','cyr'.$key)}}" class="{{ iconv('CP1251', 'UTF-8', $char1)===$character ? 'active' : '' }}">{{iconv('CP1251', 'UTF-8', $char1)}}</a>
                    {{--<a href="{{route('glossary',iconv('CP1251', 'UTF-8', $char1))}}" class="{{ iconv('CP1251', 'UTF-8', $char1)===$character ? 'active' : '' }}">{{iconv('CP1251', 'UTF-8', $char1)}}</a>--}}
                @endforeach
            </div>
            <div class="text-center">
                @foreach (range('A', 'Z') as $char2)
                    <a href="{{route('glossary',$char2)}}" class="{{$char2 === $character ? 'active' : ''}}">{{$char2}}</a>
                @endforeach
            </div>
            <div class="text-center mb-5">
                @foreach (range('0', '9') as $char3)
                    <a href="{{route('glossary',$char3)}}" class="{{$char3 === $character ? 'active' : ''}}">{{$char3}}</a>
                @endforeach
            </div>
            @foreach($glossaries as $key => $glossary)
                <div class="mb-3"><strong>{!!$glossary->name!!}</strong> {!! strip_tags($glossary->description,'<span>,</span>,<strong>,</strong>') !!}</div>
            @endforeach
        </div>
    </div>
@endsection
