<div class="list-icons">
    <div class="list-icons-item dropdown">
        <a href="#" class="list-icons-item caret-0 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="icon-menu9"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right dropdown-menu-sm" x-placement="bottom-end">
            <div class="dropdown-submenu dropdown-submenu-left">
                <a href="#" class="dropdown-item"><i class="icon-warning22"></i> @lang('Alerts')</a>
                <div class="dropdown-menu dropdown-menu-lg">

                    <div class="dropdown-submenu dropdown-submenu-left">
                        <a href="#" class="dropdown-item">@lang('Block') 1</a>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="text">@lang('Text')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="important">@lang('Important')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="webinar">@lang('Webinar')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="video">@lang('Video')</a>
                        </div>
                    </div>

                    <div class="dropdown-submenu dropdown-submenu-left">
                        <a href="#" class="dropdown-item">@lang('Block') 2</a>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="tasks">@lang('Tasks')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="history">@lang('History')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="pictures">@lang('Pictures')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="tools">@lang('Tools')</a>
                        </div>
                    </div>

                    <div class="dropdown-submenu dropdown-submenu-left">
                        <a href="#" class="dropdown-item">@lang('Block') 3</a>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="literature">@lang('Literature')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="selfeducation">@lang('Selfeducation')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="fulltime">@lang('Fulltime')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="example">@lang('Example')</a>
                        </div>
                    </div>

                    <div class="dropdown-submenu dropdown-submenu-left">
                        <a href="#" class="dropdown-item">@lang('Block') 4</a>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="expert">@lang('Expert')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="quotes">@lang('Quotes')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="checklist">@lang('Checklist')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="exam">@lang('Exam')</a>
                        </div>
                    </div>

                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item hexa-block-warning-clear"><i class="icon-eraser2"></i> @lang('Clear')</a>
                </div>
            </div>

            <a href="#" class="dropdown-item hexa-block-accordion-element"><i class="icon-add-to-list"></i> @lang('Append Accordion')</a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item hexa-block-remove"><i class="icon-bin"></i> @lang('Delete')</a>
        </div>
    </div>
</div>
