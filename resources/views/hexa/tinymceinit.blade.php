<script>
    var emailBodyConfig = {
        selector: '.hexa-block-content-text',
        menubar: false,
        inline: true,
        plugins: [
            'link',
            'lists',
            'autolink',
            'image', 'table', 
            'example',
            'code'
        ],
        toolbar: [
            'undo redo | bold italic underline strikethrough| fontselect fontsizeselect formatselect',
            'forecolor backcolor | alignleft aligncenter alignright alignjustify alignfull | numlist bullist outdent indent | image table example code',
        ],
        valid_elements: 'div[style|data*],p[style],strong,em,span[style],a[href|target],ul,ol[start],li,h1[style],h2[style],h3[style],h4[style],h5[style],h6[style],img[src|alt|width|height|style|class|title],table[style|width],th[style|width|colspan|rowspan],tr[style|width],td[style|width|colspan|rowspan],br',
        valid_styles: {
            '*': 'font-size,font-family,color,text-decoration,text-align,display,margin,margin-left,margin-right,margin-top,margin-bottom,padding,padding-left,padding-right,padding-top,padding-bottom,float,background,background-color,border,border-color,border-style,border-width',
        },
        invalid_elements : "input",
        powerpaste_word_import: 'clean',
        powerpaste_html_import: 'clean',
        forced_root_block : 'p', // оборачивающий тег
        image_list: [
            @foreach(\App\Media::where('type','=','image')->get() as $key => $media)
                    { title: '{{$media->name}}', value: '/storage/{{$media->url}}' },
            @endforeach
        ],
    };

    var emailBodyConfigAll = {
        selector: '.hexa-block-content-text',
        menubar: false,
        inline: true,
        plugins: [
            'link',
            'lists',
            'autolink',
            'image', 'table', 
            'example'
        ],
        toolbar: [
            'undo redo | bold italic underline | fontselect fontsizeselect formatselect',
            'forecolor backcolor | alignleft aligncenter alignright alignjustify alignfull | numlist bullist outdent indent | image table example',
        ],
        valid_elements: 'div[style|data*],p[style],strong,em,span[style],a[href],ul,ol,li,h1[style],h2[style],h3[style],img[src|alt|width|height|style],table[style|width],th,tr,td,br',
        valid_styles: {
            '*': 'font-size,font-family,color,text-decoration,text-align,display,margin-left,margin-right, float'
        },
        invalid_elements : "input",
        powerpaste_word_import: 'clean',
        powerpaste_html_import: 'clean',
        forced_root_block : 'p', // оборачивающий тег
        image_list: [
            @foreach(\App\Media::get() as $key => $media)
                    { title: '{{$media->name}}', value: '/storage/{{$media->url}}' },
            @endforeach
        ],
    };
    //tinymce.init(emailBodyConfig);
</script>