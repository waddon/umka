<template id="template-accordion">
        <div class="hexa-block" id="{blockId}">
            <div class="hexa-block-move"><i class="icon-grid"></i></div>
            <div class="hexa-block-content-wrap">
                <div class="hexa-block-content">
                    
                        <div class="hexa-block-content-accordion card-group-control card-group-control-right mb-3" id="{id}" data-child="3">
                            <div class="card mb-0">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">
                                        <a class="text-default collapsed" data-toggle="collapse" href="#{id}-1" aria-expanded="false"  contenteditable="true">Accordion Item #1</a>
                                    </h6>
                                    <div class="header-elements">
                                        <div class="list-icons">
                                            <a class="list-icons-item btn-remove-accordion-element" data-action="remove"></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="{id}-1" class="collapse" data-parent="#{id}">
                                    <div class="card-body p-3">
                                        <div class="hexa-block-content-text"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="card mb-0">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">
                                        <a class="text-default collapsed" data-toggle="collapse" href="#{id}-2" aria-expanded="false"  contenteditable="true">Accordion Item #2</a>
                                    </h6>
                                    <div class="header-elements">
                                        <div class="list-icons">
                                            <a class="list-icons-item btn-remove-accordion-element" data-action="remove"></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="{id}-2" class="collapse" data-parent="#{id}">
                                    <div class="card-body p-3">
                                        <div class="hexa-block-content-text"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="card mb-0">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">
                                        <a class="text-default collapsed" data-toggle="collapse" href="#{id}-3" aria-expanded="false"  contenteditable="true">Accordion Item #3</a>
                                    </h6>
                                    <div class="header-elements">
                                        <div class="list-icons">
                                            <a class="list-icons-item btn-remove-accordion-element" data-action="remove"></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="{id}-3" class="collapse" data-parent="#{id}">
                                    <div class="card-body p-3">
                                        <div class="hexa-block-content-text"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="hexa-block-tool">@include('hexa.dropdown-text')</div>
        </div>
</template>

<template id="template-accordion-element">
    <div class="card mb-0">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">
                <a class="text-default collapsed" data-toggle="collapse" href="#{id}-{no}" aria-expanded="false"  contenteditable="true">Accordion New Item</a>
            </h6>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item btn-remove-accordion-element" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div id="{id}-{no}" class="collapse" data-parent="#{id}">
            <div class="card-body p-3">
                <div class="hexa-block-content-text"></div>
            </div>
        </div>
    </div>
</template>

<template id="template-text">
        <div class="hexa-block" id="{blockId}">
            <div class="hexa-block-move"><i class="icon-grid"></i></div>
            <div class="hexa-block-content-wrap">
                <div class="hexa-block-content">
                    <div class="hexa-block-content-text mb-3"></div>
                </div>
            </div>
            <div class="hexa-block-tool">@include('hexa.dropdown-text')</div>
        </div>
</template>

<template id="template-video">
        <div class="hexa-block" id="{blockId}">
            <div class="hexa-block-move"><i class="icon-grid"></i></div>
            <div class="hexa-block-content-wrap">
                <div class="hexa-block-content">
                        <div class="embed-responsive embed-responsive-16by9 mb-3">
                            <embed class="embed-responsive-item" src="{url}">
                        </div>
                </div>
            </div>
            <div class="hexa-block-tool">@include('hexa.dropdown-text')</div>
        </div>
</template>

<template id="template-dropdown-menu">
<div class="list-icons">
    <div class="list-icons-item dropdown">
        <a href="#" class="list-icons-item caret-0 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="icon-menu9"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right dropdown-menu-sm" x-placement="bottom-end">
            <div class="dropdown-submenu dropdown-submenu-left">
                <a href="#" class="dropdown-item"><i class="icon-warning22"></i> @lang('Alerts')</a>
                <div class="dropdown-menu dropdown-menu-lg">

                    <div class="dropdown-submenu dropdown-submenu-left">
                        <a href="#" class="dropdown-item">@lang('Block') 1</a>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="text">@lang('Text')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="important">@lang('Important')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="webinar">@lang('Webinar')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="video">@lang('Video')</a>
                        </div>
                    </div>

                    <div class="dropdown-submenu dropdown-submenu-left">
                        <a href="#" class="dropdown-item">@lang('Block') 2</a>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="tasks">@lang('Tasks')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="history">@lang('History')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="pictures">@lang('Pictures')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="tools">@lang('Tools')</a>
                        </div>
                    </div>

                    <div class="dropdown-submenu dropdown-submenu-left">
                        <a href="#" class="dropdown-item">@lang('Block') 3</a>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="literature">@lang('Literature')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="selfeducation">@lang('Selfeducation')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="fulltime">@lang('Fulltime')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="example">@lang('Example')</a>
                        </div>
                    </div>

                    <div class="dropdown-submenu dropdown-submenu-left">
                        <a href="#" class="dropdown-item">@lang('Block') 4</a>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="expert">@lang('Expert')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="quotes">@lang('Quotes')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="checklist">@lang('Checklist')</a>
                            <a href="#" class="dropdown-item hexa-block-warning" data-icon="exam">@lang('Exam')</a>
                        </div>
                    </div>

                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item hexa-block-warning-clear"><i class="icon-eraser2"></i> @lang('Clear')</a>
                </div>
            </div>

            <a href="#" class="dropdown-item hexa-block-accordion-element"><i class="icon-add-to-list"></i> @lang('Append Accordion')</a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item hexa-block-remove"><i class="icon-bin"></i> @lang('Delete')</a>
        </div>
    </div>
</div>    
</template>

<template id="template-media">
    <div class="hexa-block" id="{blockId}">
        <div class="hexa-block-move"><i class="icon-grid"></i></div>
        <div class="hexa-block-content-wrap">
            <div class="hexa-block-content">
                <video class="w-100" controls>
                    <source src="{url}" type="{type}">
                </video>
            </div>
        </div>
        <div class="hexa-block-tool">@include('hexa.dropdown-text')</div>
    </div>
</template>