<script>
tinymce.PluginManager.add('example', function(editor, url) {
  var openDialog = function () {
    return editor.windowManager.open({
      title: '@lang("Create popup from Glossary")',
      body: {
        type: 'panel',
        items: [
          {
            type: 'input',
            name: 'title',
            label: '@lang("Enter text for popup")'
          },
          {
            type: 'selectbox', // component type
            name: 'SelectA', // identifier
            label: '@lang("Select Glossary for popup")',
            size: 1, // number of visible values (optional)
            items: [
            	@foreach(\App\Glossary::orderBy('id','DESC')->get() as $key => $glossary)
              		{ value: '{{html_entity_decode(str_replace(array("\r\n", "\r", "\n"), '', $glossary->description))}}', text: '{{$glossary->name}}' },
            	@endforeach
            ]
          }
        ]
      },
      buttons: [
        {
          type: 'cancel',
          text: 'Close'
        },
        {
          type: 'submit',
          text: 'Save',
          primary: true
        }
      ],
      onSubmit: function (api) {
        var data = api.getData();
        // Insert content when the window form is submitted
        //editor.insertContent('Title: ' + data.title);
        editor.insertContent('<span data-popup="popover" data-html="true" data-placement="bottom" data-content="' + data.SelectA + '">' + data.title + '</span>');
        api.close();
      }
    });
  };

  // Add a button that opens a window
  editor.ui.registry.addButton('example', {
    text: 'Popup',
    onAction: function () {
      // Open window
      openDialog();
    }
  });

  // Adds a menu item, which can then be included in any menu via the menu/menubar configuration
  editor.ui.registry.addMenuItem('example', {
    text: 'Popup plugin',
    onAction: function() {
      // Open window
      openDialog();
    }
  });

  return {
    getMetadata: function () {
      return  {
        name: "Popoup plugin",
        url: "http://exampleplugindocsurl.com"
      };
    }
  };
});	
</script>