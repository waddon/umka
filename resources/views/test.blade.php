@extends('layouts.app')

@section('content')
    <div class="card">
        <form id="testform" action="{{route('testcheck',$module->id)}}" method="POST">
            @csrf
            <div class="card-body content-area">
                <h1 class="text-center">@lang('Module Test')</h1>
                <h2 class="text-center">{{$module->name}}</h2>
                <h3 class="text-center mb-5" id="left-time">00:00</h3>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="welcome">
                        @if(isset($module->messages->welcome))
                            <div class="mb-5">{!! $module->messages->welcome !!}</div>
                        @endif
                        <div class="text-center">
                            <button type="button" class="btn btn-success" id="starttest">@lang('Start')</button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="test">
                        @foreach($module->getTestQuestions() as $key1 => $question)
                            <div class="mb-3">
                                <h3>{{ $key1+1 }}. {{ $question['text'] }}</h3>
                                <div class="answers">
                                    @if($question['type']==1)
                                        @foreach($question['answers'] as $key2 => $answer)
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input answer" name="{{$question['id']}}" id="{{$question['id']}}-{{$key2}}" value="{{$answer->id}}">
                                                <label class="custom-control-label" for="{{$question['id']}}-{{$key2}}">{{$answer->text}}</label>
                                            </div>
                                        @endforeach
                                    @endif
                                    @if($question['type']==2)
                                        @foreach($question['answers'] as $key2 => $answer)
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input answer" name="{{$question['id']}}[]" id="{{$question['id']}}-{{$key2}}" value="{{$answer->id}}">
                                                <label class="custom-control-label" for="{{$question['id']}}-{{$key2}}">{{$answer->text}}</label>
                                            </div>
                                        @endforeach
                                    @endif
                                    @if($question['type']==3)
                                        <div class="answers-list">
                                            @php($array=[])
                                            @foreach($question['answers'] as $key2 => $answer)
                                                <div class="answers-element">
                                                    <i class="icon-grid mr-2 answer-move"></i><label class="custom-control-sort" data-id="{{$answer->id}}">{{$answer->text}}</label>
                                                </div>
                                                @php($array[]=$answer->id)
                                            @endforeach
                                            {{--<input type="hidden" class="answer" value="{{json_encode($question['answers'],JSON_UNESCAPED_UNICODE)}}" name="{{$question['id']}}">--}}
                                            <input type="hidden" class="answer" value="{{json_encode($array,JSON_UNESCAPED_UNICODE)}}" name="{{$question['id']}}">
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                        <div class="text-center mb-5">
                            <button class="btn btn-success send" disabled>@lang('Send Answers')</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function(){
    var startTime = {{$module->getTimeForPassing()}}*1000;
    $('#left-time').text(beautifulTime(startTime));

    $('input.answer').change(function(){
        if (validate()) {
            $('.send').removeAttr('disabled');
        } else {
            $('.send').attr('disabled','disabled');
        };
    });

    $('.answers-list').sortable({
        handle: ".answer-move",
        tolerance: 'pointer',
        opacity: 0.6,
        placeholder: 'sortable-placeholder',
        start: function(e, ui){
            ui.placeholder.height(ui.item.outerHeight());
        },
        stop : function(e,ui){
            var array = [];
            var answerslist = $(this);
            answerslist.find('.custom-control-sort').each(function(){
                //array.push($(this).text());
                array.push($(this).attr('data-id'));
            });
            answerslist.find('input[type=hidden]').val(JSON.stringify(array));
        }
    });

    function validate(){
        result = true;
        z = $(document).find("[type='radio'], [type='checkbox']");
        for (i = 0; i < z.length; i++) {
            if ($(document).find("[name='" + z[i].name + "']:checked").length == 0) {
                result = false;
            }
        }
        return result;
    }

    $('#starttest').click(function(){
        $('#welcome').removeClass('show active');
        $('#test').addClass('show active');
        let time=startTime;
        let timerId = setTimeout(function tick() {
            time = time - 1000;
            $('#left-time').text(beautifulTime(time));
            console.log('tick');
            if (time>0) timerId = setTimeout(tick, 1000);
            else {
                console.log('stop');
                $('#testform').submit();
            }
        }, 1000);
    });

    function beautifulTime(time) {
        let hours, minutes, seconds;
        let sec_count = time/1000;
        hours = Math.trunc(sec_count/(60*60));
        minutes = Math.trunc((sec_count - (hours*60*60))/60);
        seconds = sec_count - hours*60*60 - minutes*60;
        return (hours > 9 ? hours : '0' + hours) + ':' + (minutes > 9 ? minutes : '0' + minutes) + ':' + (seconds > 9 ? seconds : '0' + seconds);
    }
});
</script>
@endsection
