@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body content-area">
            @if( !is_array($results) )
                {{ $results }}
            @else
                @if($results['available']>=$results['need'])
                    Пройдено
                @else
                    Не пройдено
                @endif
                @php( dump($results) )
            @endif
        </div>
    </div>
@endsection
