<ul class="nav nav-sidebar" data-nav-type="accordion">
    <li class="nav-item">
        <a href="{{route('dashboard')}}" class="nav-link {!! \Request::url() == route('dashboard') ? 'active' : '' !!}">
            <i class="icon-equalizer3"></i><span>@lang('Dashboard')</span>
        </a>
    </li>
    @if(Gate::check('admin') || Gate::check('users') || Gate::check('groups'))
        <li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link"><i class="icon-users"></i> <span>@lang('Users')</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="@lang('Users')">
                @if(Gate::check('admin') || Gate::check('users'))
                    <li class="nav-item"><a href="{{ route('users.index') }}" class="nav-link {!! \Request::url() == route('users.index') ? 'active' : '' !!}">@lang('User list')</a></li>
                @endif
                @if(Gate::check('admin') || Gate::check('groups'))
                    <li class="nav-item"><a href="{{ route('groups.index') }}" class="nav-link {!! \Request::url() == route('groups.index') ? 'active' : '' !!}">@lang('Groups')</a></li>
                @endif
            </ul>
        </li>
    @endif
    @if(Gate::check('admin') || Gate::check('pages') || Gate::check('medias') || Gate::check('glossaries'))
        <li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link"><i class="icon-display"></i> <span>@lang('Settings')</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="@lang('Settings')">
                @if(Gate::check('admin') || Gate::check('pages'))
                    <li class="nav-item"><a href="{{ route('pages.index') }}" class="nav-link {!! \Request::url() == route('pages.index') ? 'active' : '' !!}">@lang('Pages')</a></li>
                @endif
                @if(Gate::check('admin') || Gate::check('medias'))
                    <li class="nav-item"><a href="{{ route('medias.index') }}" class="nav-link {!! \Request::url() == route('medias.index') ? 'active' : '' !!}">@lang('Media')</a></li>
                @endif
                @if(Gate::check('admin') || Gate::check('glossaries'))
                    <li class="nav-item"><a href="{{ route('glossaries.index') }}" class="nav-link {!! \Request::url() == route('glossaries.index') ? 'active' : '' !!}">@lang('Glossary')</a></li>
                @endif
                @if(Gate::check('admin') || Gate::check('mailings'))
                    <li class="nav-item"><a href="{{ route('mailings.index') }}" class="nav-link {!! \Request::url() == route('mailings.index') ? 'active' : '' !!}">@lang('Mailings')</a></li>
                @endif
            </ul>
        </li>
    @endif
    @if(Gate::check('admin') || Gate::check('courses') || Gate::check('modules') || Gate::check('lessons') || Gate::check('themes') || Gate::check('questions') || Gate::check('quizzes'))
        <li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link"><i class="icon-tree7"></i> <span>@lang('Modules') & @lang('Themes')</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="@lang('Modules') & @lang('Themes')">
                @if(Gate::check('admin') || Gate::check('courses'))
                    <li class="nav-item"><a href="{{ route('courses.index') }}" class="nav-link {!! \Request::url() == route('courses.index') ? 'active' : '' !!}">@lang('Courses')</a></li>
                @endif
                @if(Gate::check('admin') || Gate::check('modules'))
                    <li class="nav-item"><a href="{{ route('modules.index') }}" class="nav-link {!! \Request::url() == route('modules.index') ? 'active' : '' !!}">@lang('Modules')</a></li>
                @endif
                @if(Gate::check('admin') || Gate::check('lessons'))
                    <li class="nav-item"><a href="{{ route('lessons.index') }}" class="nav-link {!! \Request::url() == route('lessons.index') ? 'active' : '' !!}">@lang('Lessons')</a></li>
                @endif
                @if(Gate::check('admin') || Gate::check('themes'))
                    <li class="nav-item"><a href="{{ route('themes.index') }}" class="nav-link {!! \Request::url() == route('themes.index') ? 'active' : '' !!}">@lang('Themes')</a></li>
                @endif
                @if(Gate::check('admin') || Gate::check('questions'))
                    <li class="nav-item"><a href="{{ route('questions.index') }}" class="nav-link {!! \Request::url() == route('questions.index') ? 'active' : '' !!}">@lang('Questions')</a></li>
                @endif
                @if(Gate::check('admin') || Gate::check('quizzes'))
                    <li class="nav-item"><a href="{{ route('quizzes.index') }}" class="nav-link {!! \Request::url() == route('quizzes.index') ? 'active' : '' !!}">@lang('Quizzes')</a></li>
                @endif
            </ul>
        </li>
    @endif
    @if(Gate::check('admin') || Gate::check('offline'))
        <li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link"><i class="icon-cog"></i> <span>@lang('Options')</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="@lang('Options')">
                @if(Gate::check('admin'))
                    <li class="nav-item"><a href="{{ route('roles.index') }}" class="nav-link {!! \Request::url() == route('roles.index') ? 'active' : '' !!}">@lang('Roles')</a></li>
                    <li class="nav-item"><a href="{{ route('permissions.index') }}" class="nav-link {!! \Request::url() == route('permissions.index') ? 'active' : '' !!}">@lang('Permissions')</a></li>
                    <li class="nav-item"><a href="{{ route('settings') }}" class="nav-link {!! \Request::url() == route('settings') ? 'active' : '' !!}">@lang('Settings')</a></li>
                    <li class="nav-item"><a href="{{ route('tracks') }}" class="nav-link {!! \Request::url() == route('tracks') ? 'active' : '' !!}">@lang('Event Log')</a></li>
                    <li class="nav-item"><a href="{{ route('phpinfo') }}" class="nav-link {!! \Request::url() == route('phpinfo') ? 'active' : '' !!}">@lang('About System')</a></li>
                @endif
                @if(Gate::check('admin') || Gate::check('offline'))
                    {{--<li class="nav-item"><a href="{{ route('offline') }}" class="nav-link {!! \Request::url() == route('offline') ? 'active' : '' !!}">@lang('Offline Version Creating')</a></li>--}}
                @endif
            </ul>
        </li>
    @endif

</ul>
