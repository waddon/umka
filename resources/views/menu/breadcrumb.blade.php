<div class="breadcrumb">
    @if(isset($data['breadcrumbs']))
        @foreach($data['breadcrumbs'] as $key => $breadcrumb)
            @if (count($data['breadcrumbs'])!=$key+1)
                <a href="{{$breadcrumb['url']}}" class="breadcrumb-item">{!!$breadcrumb['title']!!}</a>
            @else
                <span class="breadcrumb-item active">{!!$breadcrumb['title']!!}</span>
            @endif
        @endforeach
    @else
        <a href="{{route('home')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item active">Dashboard</span>
    @endif
</div>
