<div class="card card-sidebar-mobile">
    <div class="card-header header-elements-inline">
        <h6 class="card-title">@lang('Authorization')</h6>
    </div>
    <div class="card-body">
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <input id="email" type="email" class="form-control @if(isset($errors)) @error('email') is-invalid @enderror @endif mb-3" name="email" value="{{ old('email') }}" required autocomplete="email" Placeholder="@lang('Email')" autofocus>
            @if(isset($errors)) @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror @endif
            <input id="password" type="password" class="form-control @if(isset($errors)) @error('password') is-invalid @enderror @endif mb-3" name="password" required autocomplete="current-password" Placeholder="@lang('Password')">
            @if(isset($errors)) @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror @endif
            <div class="custom-control custom-checkbox mb-3">
                <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label class="custom-control-label" for="remember">@lang('Remember Me')</label>
            </div>
            <button type="submit" class="btn btn-primary form-control mb-3">@lang('Login')</button>
            <div class="text-center">
                @if (Route::has('password.request'))
                    <a href="{{ route('password.request') }}">
                        @lang('Forgot Your Password?')
                    </a>
                @endif/
                @if (Route::has('register'))
                    <a href="{{ route('register') }}">
                        @lang('Register')
                    </a>
                @endif
            </div>
        </form>
    </div>
</div>
