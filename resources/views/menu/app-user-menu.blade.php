                        <div class="sidebar-user">
                            <div class="card-body">
                                <div class="media">
                                    <div class="mr-3">
                                        <a href="{{ route('profile') }}"><img src="{{ auth()->user()->getAvatar() }}" width="38" height="38" class="rounded-circle" alt=""></a>
                                    </div>

                                    <div class="media-body">
                                        <div class="media-title font-weight-semibold">{{Auth::user()->name}}</div>
                                        <div class="font-size-xs opacity-50">{{Auth::user()->position}}</div>
                                        @if(!Auth::user()->activity)
                                            <div class="font-size-xs text-danger">@lang('Blocked')</div>
                                        @endif
                                    </div>

                                    <div class="ml-3 align-self-center">
                                        <a href="#" class="dropdown-toggle caret-0" data-toggle="dropdown" aria-expanded="false"><i class="icon-cog3"></i></a>
                                        <div class="dropdown-menu dropdown-menu-left">
                                            <a href="{{ route('profile') }}" class="dropdown-item"><i class="icon-user"></i>@lang('Profile')</a>
                                            <a href="{{ route('message') }}" class="dropdown-item"><i class="icon-comment-discussion"></i>@lang('Message List')</a>
                                            <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="icon-switch2"></i>@lang('Logout')</a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>

                                            {{--<a href="#" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
