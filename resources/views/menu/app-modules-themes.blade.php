@if(auth()->user()->activity)

    @foreach(auth()->user()->groups as $key => $group)
        @foreach($group->modules as $key2 => $module)
            @if( \Carbon\Carbon::parse($module->pivot->date_finish)->gte( \Carbon\Carbon::now()  ) )
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link module-{{$module->id}}">{{--<i class="icon-stack"></i> --}}{{--<span>{{$module->name}}</span>--}}<span><div>{{$module->name}}</div><div class="text-danger">@lang('Active before') {{\Carbon\Carbon::parse($module->pivot->date_finish)->format('d.m.Y')}}</div></span><span class="badge {{ isset(auth()->user()->data->modules->{$module->id}->passed) ? 'bg-success' : 'bg-warning' }} ml-auto passed">{{ auth()->user()->openThemesPercent($module->id) }}%</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="{{$module->name}}">
                        <!-- Introduction -->
                            @if(isset($module->data->introduction_id) && $module->data->introduction_id)
                                <li class="nav-item"><a href="{{route('page',$module->data->introduction_id)}}" class="nav-link introduction-{{$module->id}} {{ \Request::url() == route('page',$module->data->introduction_id) ? 'active' : ''}}"><span>@lang('Introduction')</span></a></li>
                            @endif
                        <!-- /introduction -->
                        <!-- Lessons & Themes -->
                            @foreach($module->lessons as $key3 => $lesson)
                                <li class="nav-item nav-item-submenu">
                                    <a href="#" class="nav-link">{{$lesson->name}}</a>
                                    <ul class="nav nav-group-sub">
                                        <!-- Introduction -->
                                            @if(isset($lesson->data->introduction_id) && $lesson->data->introduction_id)
                                                <li class="nav-item"><a href="{{route('page',$lesson->data->introduction_id)}}" class="nav-link introduction-{{$lesson->id}} {{ \Request::url() == route('page',$lesson->data->introduction_id) ? 'active' : ''}}"><span>@lang('Introduction')</span></a></li>
                                            @endif
                                        <!-- /introduction -->
                                        @foreach($lesson->activeThemes as $key3 => $theme)
                                            <li class="nav-item"><a href="{{route('themeshow',$theme->id)}}" class="nav-link {{ auth()->user()->ifActiveTheme($theme->id) ? '' : 'disabled' }} theme-{{$theme->id}} {{\Request::url() == route('themeshow',$theme->id) ? 'active' : ''}}"><span>{!!$theme->ifThemePassed() ? '<i class="icon-checkmark2 mr-1"></i> ' : ''!!}{{$theme->name}}</span></a></li>
                                        @endforeach
                                        <!-- Conclusion -->
                                            @if(isset($lesson->data->conclusion_id) && $lesson->data->conclusion_id)
                                                <li class="nav-item"><a href="{{route('page',$lesson->data->conclusion_id)}}" class="nav-link conclusion-{{$lesson->id}} {{ \Request::url() == route('page',$lesson->data->conclusion_id) ? 'active' : ''}}"><span>@lang('Conclusion')</span></a></li>
                                            @endif
                                        <!-- /conclusion -->
                                    </ul>
                                </li>
                            @endforeach
                        <!-- /lessons & themes -->
                        <!-- Conclusion -->
                            @if(isset($module->data->conclusion_id) && $module->data->conclusion_id)
                                <li class="nav-item"><a href="{{route('page',$module->data->conclusion_id)}}" class="nav-link conclusion-{{$module->id}} {{ \Request::url() == route('page',$module->data->conclusion_id) ? 'active' : ''}}"><span>@lang('Conclusion')</span></a></li>
                            @endif
                        <!-- /conclusion -->
                        <!-- Literature -->
                            @if(isset($module->data->literature_id) && $module->data->literature_id)
                                <li class="nav-item"><a href="{{route('page',$module->data->literature_id)}}" class="nav-link literature-{{$module->id}} {{ \Request::url() == route('page',$module->data->literature_id) ? 'active' : ''}}"><span>@lang('Literature')</span></a></li>
                            @endif
                        <!-- /literature -->
                        <!-- Webquestions -->
                            @if(isset($module->data->webquestions) && $module->data->webquestions)
                                <li class="nav-item"><a href="{{route('webquestions',$module->id)}}" class="nav-link webquestions-{{$module->id}} {{ \Request::url() == route('webquestions',$module->id) ? 'active' : ''}}"><span>@lang('Questions to the Webinar')</span></a></li>
                            @endif
                        <!-- /webquestions -->
                        <!-- Test -->
                            <li class="nav-item"><a href="{{route('testshow',$module->id)}}" class="nav-link test-{{$module->id}} {{ auth()->user()->ifOpenTest($module->id) ? '' : 'disabled' }} {{\Request::url() == route('testshow',$module->id) ? 'active' : ''}}"><span>@lang('Module Test')</span></a></li>
                        <!-- /test -->
                        <!-- Homework -->
                            @if(isset($module->data->homework) && $module->data->homework)
                                <li class="nav-item"><a href="{{route('homework',$module->id)}}" class="nav-link homework-{{$module->id}} {{isset(auth()->user()->data->modules->{$module->id}->passed) ? '' : 'disabled' }} {{ \Request::url() == route('homework',$module->id) ? 'active' : ''}}"><span>@lang('Homework')</span></a></li>
                            @endif
                        <!-- /homework -->
                    </ul>
                </li>

            @endif
        @endforeach
    @endforeach

    @foreach( \App\Course::get() as $key => $course)
        @if(auth()->user()->ifOpenExam($course->id))
            <li class="nav-item"><a href="{{route('examshow',$course->id)}}" class="nav-link exam-{{$module->id}} {{\Request::url() == route('examshow',$course->id) ? 'active' : ''}}"><span>@lang('Course Exam')</span></a></li>
        @endif
    @endforeach

    @foreach(auth()->user()->groups as $key => $group)
        @foreach($group->quizzes as $key2 => $quiz)
            <li class="nav-item"><a href="{{route('quizshow',$quiz->id)}}" class="nav-link quiz-{{$quiz->id}} {{\Request::url() == route('quizshow',$quiz->id) ? 'active' : ''}}"><i class="icon-clipboard2"></i> <span>{{$quiz->name}}</span></a></li>
        @endforeach
    @endforeach
@endif
