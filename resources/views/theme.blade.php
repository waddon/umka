@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            {!! $model->content !!}
        </div>
    </div>
    <div class="d-flex justify-content-between">
        @if($previous_theme_id = $model->previousThemeId())
            <a class="btn btn-primary {{ auth()->user()->ifActiveTheme($previous_theme_id) ? '' : 'disabled' }} theme-{{$previous_theme_id}}" href="{{route('themeshow',$previous_theme_id)}}"><i class="icon-arrow-left5 mr-2"></i>@lang('Previous Theme')</a>
        @else
            <div></div>
        @endif
        @if($next_theme_id = $model->nextThemeId())
            <a class="btn btn-primary {{ auth()->user()->ifActiveTheme($next_theme_id) ? '' : 'disabled' }} theme-{{$next_theme_id}}" href="{{route('themeshow',$next_theme_id)}}">@lang('Next Theme')<i class="icon-arrow-right5 ml-2"></i></a>
        @endif
    </div>
    
@endsection

@auth
@if(auth()->user()->id != \App\Option::getOption('offlineUser') || \App\Option::getOption('offlineShow'))
@section('scripts')
<script>
    $(document).ready(function(){
        setTimeout(openNextTheme, {{ \App\Option::getOption('openNewThemeTime') ? \App\Option::getOption('openNewThemeTime')*1000 : 180 }});

        function openNextTheme(){
            $.ajax({
                url: "{{route('opennexttheme')}}",
                type: 'POST',
                dataType : "json",
                data: {
                    theme:{{$model->id}},
                    _token:"{{ csrf_token() }}",
                },
                success: function (data) {
                    console.log(data);
                    $('.theme-' + data.result.result).removeClass('disabled');
                    $('.module-' + data.result.module + ' .badge').html(data.result.percent+'%');
                    if (data.openTest){
                        $('.test-' + data.result.module).removeClass('disabled');
                    }
                },
                error: function(){
                    console.log('Ajax error');
                }
            });
        }

        $('.content a').click(function(){
            $.ajax({
                url: "{{route('pushcounter')}}",
                type: 'POST',
                dataType : "json",
                data: {
                    theme:{{$model->id}},
                    user:{{auth()->user()->id}},
                    _token:"{{ csrf_token() }}",
                },
                success: function (data) {
                    console.log(data);
                },
                error: function(){
                    console.log('Ajax error');
                }
            });
        });

    });
</script>
@endsection
@endif
@endauth
