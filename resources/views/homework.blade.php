@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body content-area">

            {!! $content !!}

            @if(isset(auth()->user()->data->homework->username))
                <h2 class="text-center">{{ auth()->user()->data->homework->username }}</h2>
            @else
                <form action="{{$route}}" method="POST">
                    @csrf
                    <input type="text" name="username" class="form-control mb-3" placeholder="@lang('Enter your username...')" maxlength="191">
                    <div class="d-flex align-items-center">
                        <button type="submit" class="btn bg-success btn-labeled btn-labeled-right ml-auto"><b><i class="icon-paperplane"></i></b> @lang('Send')</button>
                    </div>
                </form>
            @endif
        </div>
    </div>
@endsection
