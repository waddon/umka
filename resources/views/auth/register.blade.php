@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h3>{{ __('Register') }}</h3></div>
                <div class="card-body">
                    <form id="register-form" method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="row">
                            <div class="form-group col-lg-6">
                                <label for="pin" class="text-md-right mb-0 text-muted font-italic">{{ __('Pin') }} <span class="text-danger">*</span></label>
                                <input id="pin" type="text" class="form-control" name="pin" required autofocus>
                                <label class="message validation-invalid-label"></label>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="name" class="text-md-right mb-0 text-muted font-italic">{{ __('Name') }} <span class="text-danger">*</span></label>
                                <input id="name" type="text" class="form-control" name="name" required>
                                <label class="message validation-invalid-label"></label>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="birthday" class="text-md-right mb-0 text-muted font-italic">{{ __('Birthday') }} <span class="text-danger">*</span></label>
                                <input id="birthday" type="text" class="form-control daterange-single" name="birthday" required pattern="[0-9]{2}.[0-9]{2}.[0-9]{4}">
                                <label class="message validation-invalid-label"></label>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="passport" class="text-md-right mb-0 text-muted font-italic">{{ __('Passport') }} <span class="text-danger">*</span></label>
                                <input id="passport" type="text" class="form-control" name="passport" required>
                                <label class="message validation-invalid-label"></label>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="password" class="text-md-right mb-0 text-muted font-italic">{{ __('Password') }} <span class="text-danger">*</span></label>
                                <input id="password" type="password" class="form-control" name="password" required>
                                <label class="message validation-invalid-label"></label>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="password-confirm" class="text-md-right mb-0 text-muted font-italic">{{ __('Confirm Password') }} <span class="text-danger">*</span></label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                <label class="message validation-invalid-label"></label>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="category_id" class="text-md-right mb-0 text-muted font-italic">{{ __('Category') }} <span class="text-danger">*</span></label>
                                <select id="category_id" name="category_id" data-placeholder="@lang('Select position category')" class="form-control select" required>
                                    <option value=""></option>
                                    <option value="1">@lang('Political Persons')</option>
                                    <option value="2">@lang('Deputies of local councils')</option>
                                    <option value="3">@lang('Heads of local government')</option>
                                    <option value="4">@lang('Law enforcement officers')</option>
                                    <option value="5">@lang('Persons who graduated from a university and are not public servants')</option>
                                    <option value="6">@lang('Others')</option>
                                </select>
                                <label class="message validation-invalid-label"></label>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="position" class="text-md-right mb-0 text-muted font-italic">{{ __('Position') }} <span class="text-danger">*</span></label>
                                <input id="position" type="position" class="form-control" name="position" required>
                                <label class="message validation-invalid-label"></label>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="email" class="text-md-right mb-0 text-muted font-italic">{{ __('E-Mail Address') }} <span class="text-danger">*</span></label>
                                <input id="email" type="email" class="form-control" name="email" required>
                                <label class="message validation-invalid-label"></label>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="phone" class="text-md-right mb-0 text-muted font-italic">{{ __('Phone') }} <span class="text-danger">*</span></label>
                                <input id="phone" type="phone" class="form-control" name="phone" required>
                                <label class="message validation-invalid-label"></label>
                            </div>
                            <div class="form-group col-lg-12">
                                <label for="filename" class="text-md-right mb-0 text-muted font-italic">{{ __('Confirmation document') }} <span class="text-danger">*</span></label>
                                <input type="file" name="filename" class="form-control-uniform form-control" data-fouc="" required>
                                <label class="message validation-invalid-label"></label>
                            </div>
                        </div>

                        <div class="form-group text-center">
                            <button id="submit" type="submit" class="btn btn-primary">{{ __('Register') }}</button>
                            <a href="{{route('home')}}" class="btn btn-light">{{__('Go home')}}</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function(){
    $('#register-form').submit(function(e){
        e.preventDefault();
        $('#submit').attr('disabled','disabled');
        // alert('---');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: '/register',
            dataType: "JSON",
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function(response){
                window.location.href = response.redirect;
            },
            error: function(response){
                errors = response.responseJSON.errors;
                $( '#register-form input' ).each(function( index ) {
                    let fieldname = $( this ).attr('name');
                    $(this).siblings('.message').first().text('');
                    if (errors[fieldname]){
                        $(this).siblings('.message').first().text( errors[fieldname].toString() );
                    }
                });
                console.log(response);
            }
        }).always((xhr, type, status) => {
            $('#submit').removeAttr('disabled');
        });
    });
});
</script>
@endsection
