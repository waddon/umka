@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            {!! $model->content !!}
        </div>
    </div>
@endsection
