@include('layouts.app-header')

    @if(Session::has('flash_message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">@lang('Well done!')</span> {!! session('flash_message') !!}.
        </div>
    @endif
    @if(Session::has('error_message'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">@lang('Oh snap!')</span> {!! session('error_message') !!}.
        </div>    
    @endif

<div id="content-for-print">
@section('content')
@show
</div>

@include('layouts.app-footer')
