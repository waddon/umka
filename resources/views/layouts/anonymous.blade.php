<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>
    <link rel="icon" type="image/png" href="/assets/img/logo.png" />

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/custom.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="/assets/js/main/jquery.min.js"></script>
    <script src="/assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="/assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script src="/assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script src="/assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script src="/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script src="/assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="/assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="/assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script src="/assets/js/plugins/forms/selects/select2.min.js"></script>

    {{--<script src="/assets/js/plugins/pickers/pickadate/picker.js"></script>
    <script src="/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
    <script src="/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
    <script src="/assets/js/plugins/pickers/pickadate/legacy.js"></script>--}}

    <script src="/assets/js/app.js"></script>
    <script src="/assets/js/demo_pages/dashboard.js"></script>
    {{--<script src="/assets/js/demo_pages/picker_date.js"></script>--}}

    <script src="/assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="/assets/js/demo_pages/form_select2.js"></script>

    <script src="/assets/js/custom.js"></script>

    <!-- /theme JS files -->

</head>

<body>

    <!-- Main navbar -->
    <div class="navbar navbar-expand-md">
        <div class="navbar-brand wmin-200">
            <a href="{{route('home')}}" class="d-inline-block">
                <img src="/assets/img/umk@.png" alt="">
            </a>
        </div>

        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
            <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                <i class="icon-paragraph-justify3"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                    </a>
                </li>
            </ul>

            <span class="ml-md-auto mr-md-3">
            </span>

            <ul class="navbar-nav align-items-center">

                <li class="nav-item dropdown">
                    <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                        <img src="/assets/img/lang/{{App::getLocale()}}.png" class="img-flag mr-2" alt="">
                        {{trans('project.lang.'.App::getLocale())}}
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="<?= route('setlocale', ['lang' => 'ru']) ?>" class="dropdown-item @if(App::getLocale()=='ru') active @endif"><img src="/assets/img/lang/ru.png" class="img-flag" alt=""> Русский</a>
                        <a href="<?= route('setlocale', ['lang' => 'en']) ?>" class="dropdown-item @if(App::getLocale()=='en') active @endif"><img src="/assets/img/lang/en.png" class="img-flag" alt=""> English</a>
                        <a href="<?= route('setlocale', ['lang' => 'ky']) ?>" class="dropdown-item @if(App::getLocale()=='ky') active @endif"><img src="/assets/img/lang/ky.png" class="img-flag" alt=""> Кыргызча‎</a>
                    </div>
                </li>

                <li class="nav-item">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-text-color icon-2x"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-text-color"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-eye2 icon-2x"></i>
                    </a>
                </li>

            </ul>
        </div>
    </div>
    <!-- /main navbar -->


    <!-- Page header -->
    <div class="page-header">
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <span class="breadcrumb-item active">Dashboard</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="breadcrumb justify-content-center">
                    <a href="#" class="breadcrumb-elements-item">
                        <i class="icon-list mr-2"></i>
                        Инструкция
                    </a>
                    <a href="#" class="breadcrumb-elements-item">
                        <i class="icon-spell-check mr-2"></i>
                        Глоссарий
                    </a>
                    <a href="#" class="breadcrumb-elements-item">
                        <i class="icon-download mr-2"></i>
                        Офлайн-версия
                    </a>
                    <a href="#" class="breadcrumb-elements-item">
                        <i class="icon-printer2 mr-2"></i>
                        Печать
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->
        

    <!-- Page content -->
    <div class="page-content pt-0">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                @section('content')
                @show

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->


    <!-- Footer -->
    <div class="navbar navbar-expand-lg navbar-light">
        <div class="text-center d-lg-none w-100">
            <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                <i class="icon-unfold mr-2"></i>
                Footer
            </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-footer">
            <span class="navbar-text">
                &copy; 2015 - 2018. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
            </span>

            <ul class="navbar-nav ml-lg-auto">
                <li class="nav-item"><a href="https://kopyov.ticksy.com/" class="navbar-nav-link" target="_blank"><i class="icon-lifebuoy mr-2"></i> Support</a></li>
                <li class="nav-item"><a href="http://demo.interface.club/limitless/docs/" class="navbar-nav-link" target="_blank"><i class="icon-file-text2 mr-2"></i> Docs</a></li>
                <li class="nav-item"><a href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov" class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i class="icon-cart2 mr-2"></i> Purchase</span></a></li>
            </ul>
        </div>
    </div>
    <!-- /footer -->

@section('modals')
@show

@section('scripts')
@show
        
</body>
</html>
