            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->


    <!-- Footer -->
    <div class="navbar navbar-expand-lg navbar-light">
        {{--<div class="text-center d-lg-none w-100">
            <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                <i class="icon-unfold mr-2"></i>
                @lang('Footer')
            </button>
        </div>--}}

        <div class="navbar-collapse collapse show" id="navbar-footer">
            <div class="d-flex flex-wrap flex-xl-nowrap pt-3 pb-3 w-100 align-items-center">
                <div class="footer-text order-xl-2 d-flex flex-nowrap flex-xl-wrap justify-content-between align-self-stretch">
                    <div class="footer-app-name d-flex justify-content-xl-end order-xl-2 align-items-end">
                        <h5>© {{env('APP_NAME')}}</h5>
                    </div>
                    <div class="footer-pages d-flex flex-nowrap flex-xl-wrap order-xl-1">
                        <h5 class="ml-2"><a href="{{route('page','about')}}">@lang('About Us')</a></h5>
                        <h5 class="ml-2"><a href="{{route('page','contacts')}}">@lang('Contacts')</a></h5>
                    </div>
                </div>
                <div class="footer-images order-xl-1 text-center d-flex">
                    <div><img src="/assets/img/logo1.png" alt=""></div>
                    <div><img src="/assets/img/logo2.png" alt=""></div>
                    <div><img src="/assets/img/logo3.png" alt=""></div>
                </div>
            </div>

        </div>
    </div>
    <div class="text-center p-3">@lang('Данное дистанционное обучение разработано при поддержке Кыргызско-Германского проекта "Перспективы для молодёжи", реализуемого Deutschaft für Internationale Zusammenarbeit (GIZ) GmbH (Германское общество по международному сотрудничеству) по поручению Правительства Германии')</div>
    <!-- /footer -->
    <script src="/js/main/jquery.min.js"></script>
    <script src="/js/main/bootstrap.bundle.min.js"></script>
    <script src="/js/plugins/loaders/blockui.min.js"></script>
    <script src="/js/plugins/ui/ripple.min.js"></script>
    <script src="/js/plugins/forms/styling/switchery.min.js"></script>
    <script src="/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script src="/js/plugins/ui/moment/moment.min.js"></script>
    <script src="/js/plugins/pickers/daterangepicker.js"></script>
    <script src="/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="/js/plugins/forms/styling/uniform.min.js"></script>
    <script src="/js/plugins/printers/printme/jquery-printme.min.js"></script>
    <script src="/js/admin.js"></script>
    <script src="{{mix('js/app.js')}}" ></script>
    <script>
        document.oncopy = function () {
            var bodyElement = document.body; 
            var selection = getSelection(); 
            //var href = document.location.href; 
            //var copyright = 'Источник: <a href="'+ href +'">' + href + '</a>'; 
            var copyright = '{{\App\Option::getOption('Copyright','')}}'; 
            var text = selection + copyright;
            var divElement = document.createElement('div'); 
            divElement.style.position = 'absolute'; 
            divElement.style.left = '-99999px'; 
            text1 = document.createTextNode(text); //создал текстовый узел
            divElement.appendChild(text1); //и добавил его
            bodyElement.appendChild(divElement); 
            selection.selectAllChildren(divElement); 
            setTimeout(function(){
                bodyElement.removeChild(divElement); 
            }, 0);
        };
    </script>
    @section('scripts')
    @show
</body>
</html>
