<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" type="image/png" href="/assets/img/logo.png" />

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/custom.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/hexa.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="/assets/js/main/jquery.min.js"></script>
    <script src="/assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="/assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script src="/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script src="/assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="/assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="/assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    {{--<script src="/assets/js/plugins/forms/styling/uniform.min.js"></script>--}}
    <!-- /theme JS files -->

    {{--<script src="/assets/js/plugins/speakers/responsivevoice.js"></script>--}}

    <script src="/assets/js/app.js"></script>
    {{--<script src="/assets/js/demo_pages/form_checkboxes_radios.js"></script>--}}

</head>

<body>

    <!-- Main navbar -->
    <div class="navbar navbar-expand-md">
        <div class="navbar-brand wmin-200">
            <a href="{{route('home')}}" class="d-inline-block">
                <img src="/assets/img/umk@.png" alt="">
            </a>
        </div>

        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
            <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                <i class="icon-paragraph-justify3"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                    </a>
                </li>
            </ul>

            <span class="ml-md-auto mr-md-3">
            </span>

            <ul class="navbar-nav align-items-center">

                @langselector()

                <li class="nav-item">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-text-color icon-2x"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-text-color"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-eye2 icon-2x"></i>
                    </a>
                </li>
                @auth
                    @can('dashboard')
                        <li class="nav-item">
                            <a href="{{route('dashboard')}}" class="navbar-nav-link" title="@lang('Dashboard')">
                                <i class="icon-equalizer3 icon-2x"></i>
                            </a>
                        </li>
                    @endcan
                @endauth
            </ul>
        </div>
    </div>
    <!-- /main navbar -->


    <!-- Page header -->
    <div class="page-header">
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('home')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> @lang('Home')</a>
                    <span class="breadcrumb-item active">Dashboard</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="breadcrumb justify-content-center">
                    <a href="#" class="breadcrumb-elements-item btn-read">
                        <i class="icon-speakers mr-2"></i>
                        @lang('Read')
                    </a>
                    <a href="#" class="breadcrumb-elements-item">
                        <i class="icon-list mr-2"></i>
                        @lang('Guide')
                    </a>
                    <a href="{{route('glossary')}}" class="breadcrumb-elements-item">
                        <i class="icon-spell-check mr-2"></i>
                        @lang('Glossary')
                    </a>
                    <a href="#" class="breadcrumb-elements-item">
                        <i class="icon-download mr-2"></i>
                        @lang('Offline version')
                    </a>
                    <a href="#" class="breadcrumb-elements-item">
                        <i class="icon-printer2 mr-2"></i>
                        @lang('Print')
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->
        

    <!-- Page content -->
    <div class="page-content pt-0">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-light sidebar-main sidebar-expand-md align-self-start">

            <!-- Sidebar mobile toggler -->
            <div class="sidebar-mobile-toggler text-center">
                <a href="#" class="sidebar-mobile-main-toggle">
                    <i class="icon-arrow-left8"></i>
                </a>
                <span class="font-weight-semibold">Main sidebar</span>
                <a href="#" class="sidebar-mobile-expand">
                    <i class="icon-screen-full"></i>
                    <i class="icon-screen-normal"></i>
                </a>
            </div>
            <!-- /sidebar mobile toggler -->


            <!-- Sidebar content -->
            <div class="sidebar-content">
                @auth
                    <div class="card card-sidebar-mobile">

                        <!-- User menu -->
                        <div class="sidebar-user">
                            <div class="card-body">
                                <div class="media">
                                    <div class="mr-3">
                                        <a href="#"><img src="{{ auth()->user()->getAvatar() }}" width="38" height="38" class="rounded-circle" alt=""></a>
                                    </div>

                                    <div class="media-body">
                                        <div class="media-title font-weight-semibold">{{Auth::user()->name}}</div>
                                        <div class="font-size-xs opacity-50">{{Auth::user()->position}}</div>
                                    </div>

                                    <div class="ml-3 align-self-center">
                                        <a href="#" class="dropdown-toggle caret-0" data-toggle="dropdown" aria-expanded="false"><i class="icon-cog3"></i></a>
                                        <div class="dropdown-menu dropdown-menu-left">
                                            <a href="{{ route('profile') }}" class="dropdown-item"><i class="icon-user"></i>@lang('Profile')</a>
                                            <a href="{{ route('message') }}" class="dropdown-item"><i class="icon-comment-discussion"></i>@lang('Message List')</a>
                                            <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="icon-switch2"></i>@lang('Logout')</a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>

                                            {{--<a href="#" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /user menu -->
                        <!-- Main navigation -->
                        <div class="card-body p-0">
                            <ul class="nav nav-sidebar" data-nav-type="accordion">
                                @include('menu.modules-themes')
                            </ul>
                        </div>
                        <!-- /main navigation -->
                    </div>
                @else
                    <div class="card card-sidebar-mobile">
                        <div class="card-header header-elements-inline">
                            <h6 class="card-title">@lang('Authorization')</h6>
                        </div>
                        <div class="card-body">
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror mb-3" name="email" value="{{ old('email') }}" required autocomplete="email" Placeholder="@lang('Email')" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror mb-3" name="password" required autocomplete="current-password" Placeholder="@lang('Password')">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="remember">@lang('Remember Me')</label>
                                    </div>

                                    <button type="submit" class="btn btn-primary form-control mb-3">@lang('Login')</button>

                                    <div class="text-center">
                                        @if (Route::has('password.request'))
                                            <a href="{{ route('password.request') }}">
                                                @lang('Forgot Your Password?')
                                            </a>
                                        @endif/
                                        @if (Route::has('register'))
                                            <a href="{{ route('register') }}">
                                                @lang('Register')
                                            </a>
                                        @endif
                                    </div>
                                </form>
                        </div>
                    </div>
                @endauth
            </div>
            <!-- /sidebar content -->
            
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Dashboard content -->
                <div class="row">
                    <div class="col-xl-12">

                        @if(Session::has('flash_message'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                                <span class="font-weight-semibold">@lang('Well done!')</span> {!! session('flash_message') !!}.
                            </div>
                        @endif
                        @if(Session::has('error_message'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                                <span class="font-weight-semibold">@lang('Oh snap!')</span> {!! session('error_message') !!}.
                            </div>    
                        @endif
                        
                        @section('content')
                        @show
                    </div>

                    {{--<div class="col-xl-3">

                        <!-- Daily sales -->
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h6 class="card-title">Daily sales stats</h6>
                            </div>

                            <div class="card-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A eius expedita ipsa corporis molestias neque earum ab consequatur, repellendus repudiandae.
                            </div>
                        </div>
                        <!-- /daily sales -->--}}

                    </div>
                </div>
                <!-- /dashboard content -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->


    <!-- Footer -->
    <div class="navbar navbar-expand-lg navbar-light">
        <div class="text-center d-lg-none w-100">
            <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                <i class="icon-unfold mr-2"></i>
                Footer
            </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-footer">
            <span class="navbar-text">
                &copy; 2015 - 2018. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
            </span>

            <ul class="navbar-nav ml-lg-auto">
                <li class="nav-item"><a href="https://kopyov.ticksy.com/" class="navbar-nav-link" target="_blank"><i class="icon-lifebuoy mr-2"></i> Support</a></li>
                <li class="nav-item"><a href="http://demo.interface.club/limitless/docs/" class="navbar-nav-link" target="_blank"><i class="icon-file-text2 mr-2"></i> Docs</a></li>
                <li class="nav-item"><a href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov" class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i class="icon-cart2 mr-2"></i> Purchase</span></a></li>
            </ul>
        </div>
    </div>
    <!-- /footer -->
    @section('scripts')
    @show

    <script type="text/javascript">
        $(document).ready(function(){
            var lang = {'en' : 'UK English Male','ru' : 'Russian Male'};
            function stripHTML(dirtyString) {
                var container = document.createElement('div');
                var text = document.createTextNode(dirtyString);
                container.appendChild(text);
                return container.innerHTML; // innerHTML will be a xss safe string
            }
            $(".btn-read").click(function(){
                responsiveVoice.speak( stripHTML($('.content').first().text().replace(/<[^>]+>/g,'')) , lang.{{app()->getLocale()}} );
            });
        });
    </script>
        
</body>
</html>
