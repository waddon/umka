            </div>
            <!-- /content area -->

            <!-- Footer -->
            <div class="navbar navbar-expand-lg navbar-light">
                <div class="text-center d-lg-none w-100">
                    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                        <i class="icon-unfold mr-2"></i>
                        @lang('Footer')
                    </button>
                </div>

                <div class="navbar-collapse collapse" id="navbar-footer">
                    <span class="navbar-text">
                        &copy; 2020. <a href="{{route('home')}}"> {{env('APP_NAME')}}</a> @lang('created by') <a href="http://www.activemedia.ua" target="_blank">ActiveMedia</a>
                    </span>

                    <ul class="navbar-nav ml-lg-auto">
                        <li class="nav-item"><a href="http://www.activemedia.ua" class="navbar-nav-link" target="_blank"><i class="icon-lifebuoy mr-2"></i> @lang('Support')</a></li>
                    </ul>
                </div>
            </div>
            <!-- /footer -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

    <script src="/js/main/jquery.min.js"></script>
    <script src="/js/main/bootstrap.bundle.min.js"></script>
    <script src="/js/plugins/loaders/blockui.min.js"></script>
    <script src="/js/plugins/ui/ripple.min.js"></script>
    <script src="/js/plugins/visualization/d3/d3.min.js"></script>
    <script src="/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script src="/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script src="/js/plugins/ui/moment/moment.min.js"></script>
    <script src="/js/plugins/pickers/daterangepicker.js"></script>
    <script src="/js/plugins/forms/styling/uniform.min.js"></script>
    <script src="/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script src="/js/plugins/notifications/bootbox.min.js"></script>
    <script src="/js/plugins/forms/styling/uniform.min.js"></script>
    <script src="/js/plugins/forms/selects/select2.min.js"></script>
    <script src="/js/admin.js"></script>
    <script src="/js/plugins/uploaders/dropzone.min.js"></script>
    <script src="/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="/js/demo_pages/form_select2.js"></script>
    <script src="{{mix('js/app.js')}}" ></script>    
    <script>mediaOptionsList='{!!\App\Media::getMediaOptionsList()!!}';</script>
    @section('modals')
    @show
    @section('scripts')
    @show
</body>
</html>
