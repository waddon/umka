<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}{{isset($model->name) ? ' - '.$model->name : ''}}</title>
    <link rel="icon" type="image/png" href="/assets/img/logo.png" />

    <link href="/css/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">

</head>

<body>
    <!-- Main navbar -->
    <div class="navbar navbar-expand-md">
        <div class="navbar-brand wmin-200">
            <a href="{{route('home')}}" class="d-inline-block">
                <img src="/assets/img/umk@.png" alt="">
            </a>
        </div>

        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
            <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                <i class="icon-paragraph-justify3"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">
            {{--<ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                    </a>
                </li>
            </ul>--}}

            <span class="ml-md-auto mr-md-3">
            </span>

            <ul class="navbar-nav align-items-center">

                @langselector()

                <li class="nav-item">
                    <a class="navbar-nav-link assist-up">
                        <i class="icon-text-color icon-2x"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="navbar-nav-link assist-down">
                        <i class="icon-text-color"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link assist-color">
                        <i class="icon-eye2 icon-2x"></i>
                    </a>
                </li>
                @auth
                    @can('dashboard')
                        @if(auth()->user()->id != \App\Option::getOption('offlineUser') || \App\Option::getOption('offlineShow'))
                            <li class="nav-item">
                                <a href="{{route('dashboard')}}" class="navbar-nav-link" title="@lang('Dashboard')">
                                    <i class="icon-equalizer3 icon-2x"></i>
                                </a>
                            </li>
                        @endif
                    @endcan
                @endauth
            </ul>
        </div>
    </div>
    <!-- /main navbar -->


    <!-- Page header -->
    <div class="page-header">
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                @include('menu.app-breadcrumb')

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="breadcrumb justify-content-center">
                    @if($path = \App\Media::getPath(\App\Option::getOption('guide')))
                    <a href="/storage/{{$path}}" target="_blank" class="breadcrumb-elements-item">
                        <i class="icon-list mr-2"></i>
                        @lang('Guide')
                    </a>
                    @endif
                    <a href="{{route('glossary')}}" class="breadcrumb-elements-item">
                        <i class="icon-spell-check mr-2"></i>
                        @lang('Glossary')
                    </a>
                    {{--@if(isset($model->id))
                    <a href="{{ route('pdf',$model->id) }}" class="breadcrumb-elements-item" download>
                        <i class="icon-download mr-2"></i>
                        @lang('Offline version')
                    </a>
                    @endif--}}
                    @if(isset($model->data->offline))
                    <a href="{{ route('home') . '/storage/' . $model->data->offline }}" class="breadcrumb-elements-item" download>
                        <i class="icon-download mr-2"></i>
                        @lang('Offline version')
                    </a>
                    @endif
                    <a href="#" class="breadcrumb-elements-item" id="print">
                        <i class="icon-printer2 mr-2"></i>
                        @lang('Print')
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->
        

    <!-- Page content -->
    <div class="page-content pt-0">

        <!-- Main sidebar -->
        @if (\Route::current()->getName() != 'register' && \Route::current()->getName() != 'login' && \Route::current()->getName() != 'password.request')
        <div class="sidebar sidebar-light sidebar-main sidebar-expand-md align-self-start">

            <!-- Sidebar mobile toggler -->
            <div class="sidebar-mobile-toggler text-center">
                <a href="#" class="sidebar-mobile-main-toggle">
                    <i class="icon-arrow-left8"></i>
                </a>
                <span class="font-weight-semibold">Main sidebar</span>
                <a href="#" class="sidebar-mobile-expand">
                    <i class="icon-screen-full"></i>
                    <i class="icon-screen-normal"></i>
                </a>
            </div>
            <!-- /sidebar mobile toggler -->


            <!-- Sidebar content -->
            <div class="sidebar-content">
                @auth
                    @if(auth()->user()->id != \App\Option::getOption('offlineUser') || \App\Option::getOption('offlineShow'))
                        <div class="card card-sidebar-mobile">
                            @include('menu.app-user-menu')
                            <div class="card-body p-0">
                                <ul class="nav nav-sidebar" data-nav-type="accordion">
                                    @include('menu.app-modules-themes')
                                </ul>
                            </div>
                        </div>
                    @else
                        <div class="card card-sidebar-mobile ">
                            <span class="p-3 text-center bg-warning">Оффлайн версия</span>
                        </div>
                    @endif
                @else
                    @include('menu.app-login-form')
                @endauth
            </div>
            <!-- /sidebar content -->
            
        </div>
        @endif        
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

