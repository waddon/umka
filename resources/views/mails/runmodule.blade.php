<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> <title>{{isset($data['subject']) ? $data['subject'] : 'Subject'}}</title>
</title>
</head>
<body style="margin: 0;padding: 0;">
    <table align="center" cellpadding="0" cellspacing="0" width="100%" style="background-color:#ededed;">
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" width="600px">
                    <tr>
                        <td>
                            <table width="100%" style="background-color:#fff;-webkit-box-shadow: -5px 5px 5px 1px rgba(0,0,0,0.51);box-shadow: -5px 5px 5px 1px rgba(0,0,0,0.51);margin-top: 50px;">
                                <tr>
                                    <td style="text-align: left;padding:10px;"><img src="http://umka.local/assets/img/umk@.png" alt="" style="height: 48px;"></td>
                                    <td style="text-align: right;padding:10px;font-family: arial;">
                                        <a href="{{route('page','about')}}" style="color:#000;text-decoration:none;margin-right: 15px;">@lang('About Us')</a>
                                        <a href="{{route('page','contacts')}}" style="color:#000;text-decoration:none;">@lang('Contacts')</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="padding: 100px 15px;font-family: arial;">
                            <img src="http://umka.local/assets/img/lettericon.png" alt="">
                            <h1 style="color: #cc0316;">{{isset($data['subject']) ? $data['subject'] : 'Subject'}}!</h1>
                            <p>@lang('messages.runModuleMessage',$data)</p>
                            <a href="{{route('home')}}" style="display: inline-block;background: #cc0316;color: #fff;padding: 15px 30px;text-decoration: none;margin-top: 15px;">@lang('Go to the website')</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table align="center" cellpadding="0" cellspacing="0" width="100%" style="background-color:#dddddd;">
        <tr>
            <td align="center" style="padding: 50px;color: #cc0316;font-family: arial;"><small><span style="font-weight: bold;">@lang('If you do not want to receive notifications, click')</span> <a href="{{route('profile')}}" style="color:#cc0316;font-style: italic;">@lang('Cancel subscription')</a></small></td>
        </tr>
    </table>
</body>
</html>