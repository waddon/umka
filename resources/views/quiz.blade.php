@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body content-area">
            @if(isset(auth()->user()->data->quizzes->{$quiz->id}))
                <h1 class="text-center">@lang('We Thank You For Your Attention!')</h1>
            @else
                <form id="quiz-form" action="{{route('quizshow',$quiz->id)}}" method="POST">
                    @csrf
                    <h1 class="text-center">@lang('Quiz')</h1>
                    {!!$quiz->content ?? ''!!}
                    <hr>
                    @if(is_array($quiz->data))
                        @foreach($quiz->data as $key => $question)
                        <div class="form-group row">
                            <div class="col-md-4">{{$question->text}}</div>
                            <div class="col-md-8">
                            @if($question->type=='radio')
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input answer" name="{{$question->id}}" id="{{$question->id}}-{{$key}}1" value="@lang('абсолютно непонятно и сложно')" required>
                                    <label class="custom-control-label" for="{{$question->id}}-{{$key}}1">@lang('абсолютно непонятно и сложно')</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input answer" name="{{$question->id}}" id="{{$question->id}}-{{$key}}2" value="@lang('много чего понятно, но немного сложно')" required>
                                    <label class="custom-control-label" for="{{$question->id}}-{{$key}}2">@lang('абсолютно непонятно и сложно')</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input answer" name="{{$question->id}}" id="{{$question->id}}-{{$key}}3" value="@lang('много чего понятно, но немного сложно')" required>
                                    <label class="custom-control-label" for="{{$question->id}}-{{$key}}3">@lang('много чего понятно, но немного сложно')</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input answer" name="{{$question->id}}" id="{{$question->id}}-{{$key}}4" value="@lang('все понятно и достаточно просто')" required>
                                    <label class="custom-control-label" for="{{$question->id}}-{{$key}}4">@lang('все понятно и достаточно просто')</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input answer" name="{{$question->id}}" id="{{$question->id}}-{{$key}}5" value="@lang('всё очень понятно и просто')" required>
                                    <label class="custom-control-label" for="{{$question->id}}-{{$key}}5">@lang('всё очень понятно и просто')</label>
                                </div>
                            @endif
                            @if($question->type=='text')
                                <textarea name="{{$question->id}}" id="{{$question->id}}" class="form-control" rows="5" required></textarea>
                            @endif
                            @if($question->type=='select-yes' || $question->type=='select-no' || $question->type=='select-yes-no')
                                <select name="{{$question->id}}" id="{{$question->id}}" class="form-control" data-type="{{$question->type}}" required>
                                    <option value="" hidden>@lang('Select')</option>
                                    <option value="yes">@lang('Yes')</option>
                                    <option value="no">@lang('No')</option>
                                </select>
                            @endif
                            </div>
                        </div>
                        @endforeach
                        <h3 class="text-center">@lang('We Thank You For Your Attention!')</h3>
                        <div class="text-center"><button class="btn btn-success">@lang('Send')</button></div>
                    @endif
                </form>
            @endif
        </div>
    </div>
<template id="textarea-empty">
    <div class="description mt-3">
        <p>@lang('Details'):</p>
        <textarea id="{id}" name="{id}" class="form-control" required></textarea>
    </div>
</template>
@endsection

@section('scripts')
<script>
    $('select[data-type=select-yes]').change(function(){
        if($(this).val()=='yes'){
            $(this).parent().append(renderTemplate('textarea-empty',{id:Date.now()}));
        } else {
            $(this).siblings('.description').remove();
        }
    });
    $('select[data-type=select-no]').change(function(){
        if($(this).val()=='no'){
            $(this).parent().append(renderTemplate('textarea-empty',{id:Date.now()}));
        } else {
            $(this).siblings('.description').remove();
        }
    });
    $('select[data-type=select-yes-no]').change(function(){
        $(this).siblings('.description').remove();
        $(this).parent().append(renderTemplate('textarea-empty',{id:Date.now()}));
    });
    $('#quiz-form').submit(function(e){
    });
function renderTemplate(name, data) {
    var template = document.getElementById(name).innerHTML;
    for (var property in data) {
        if (data.hasOwnProperty(property)) {
            var search = new RegExp('{' + property + '}', 'g');
            template = template.replace(search, data[property]);
        }
    }
    return template;
}    
</script>
@endsection
