@extends('layouts.app')

@section('content')
<style>
.banners{
    flex-basis: 405px;
    margin-left: 15px;
}
@media screen and (max-width: 1199px) {
    .banners{
        flex-basis: 100%;
        margin-left: 0;
    }
}
.uppercase{
    text-transform: uppercase;
}
.colored{
    color:#af091d;
    font-weight: bold
}
@media screen and (max-width: 539px) {
    .banner .icon img{
        width: 85px;
    }
}
</style>
    <div class="d-flex flex-wrap flex-xl-nowrap">
        <div>
            <div class="card">
                <div class="card-body">
                    {!! $model->content !!}
                </div>
            </div>
        </div>
        <div class="flex-grow-0 flex-shrink-0 banners" style="">
            <div class="card">
                <div class="banner d-flex align-items-center text-center p-2">
                    <div class="icon mr-1"><img src="/assets/img/module1.jpg" alt=""></div>
                    <h6 class="flex-grow-1 flex-shrink-1"><span class="uppercase colored">Модуль 1</span><br><span class="colored">Права человека - основа прав молодёжи</span><br>Терминолигия<br>Принципы<br>Ценности<br>Практика</h6>
                </div>
            </div>
            <div class="card">
                <div class="banner d-flex align-items-center text-center p-2">
                    <div class="icon mr-1"><img src="/assets/img/module2.jpg" alt=""></div>
                    <h6 class="flex-grow-1 flex-shrink-1"><span class="uppercase colored">Модуль 2</span><br><span class="colored">Административные процедуры</span><br>Основы работы<br>Процедуры<br>Процессы<br>Государственный и муниципальный уровни</h6>
                </div>
            </div>
            <div class="card">
                <div class="banner d-flex align-items-center text-center p-2">
                    <div class="icon mr-1"><img src="/assets/img/module3.jpg" alt=""></div>
                    <h6 class="flex-grow-1 flex-shrink-1"><span class="uppercase colored">Модуль 3</span><br><span class="colored">Молодёжная политика, участие молодёжи и эдвокаси</span><br>Основные составляющие, значимость для молодежи, важность участия молодежи в процессах принятия решений</h6>
                </div>
            </div>
            <div class="card">
                <div class="banner d-flex align-items-center text-center p-2">
                    <div class="icon mr-1"><img src="/assets/img/module4.jpg" alt=""></div>
                    <h6 class="flex-grow-1 flex-shrink-1"><span class="uppercase colored">Модуль 4</span><br><span class="colored">Местные стратегии развития молодёжи</span><br>Формирование навыков разработки местных стратегий развития молодежи на уровне местных сообществ</h6>
                </div>
            </div>
            <div class="card">
                <div class="banner d-flex align-items-center text-center p-2">
                    <div class="icon mr-1"><img src="/assets/img/module5.jpg" alt=""></div>
                    <h6 class="flex-grow-1 flex-shrink-1"><span class="uppercase colored">Модуль 5</span><br><span class="colored">Работа с молодёжью и управление проектом</span><br>Как и зачем работать с молодежью, способы ведения социальных проектов, что такое социальное предпринимательство</h6>
                </div>
            </div>
            <div class="card">
                <div class="banner d-flex align-items-center text-center p-2">
                    <div class="icon mr-1"><img src="/assets/img/module6.jpg" alt=""></div>
                    <h6 class="flex-grow-1 flex-shrink-1"><span class="uppercase colored">Модуль 6</span><br><span class="colored">Работа с общественностью и создание общественного мнения</span><br>PR: определение, элементы, функции, потребности для государственных и муниципальных служащих по делам молодежи</h6>
                </div>
            </div>

        </div>
    </div>
@endsection
