<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role_has_permission extends Model
{
    protected $table = 'role_has_permissions';
    public $timestamps = FALSE;
}
