<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Track extends Model
{

    protected $guarded = [];
    protected $casts = [
        'content' => 'object',
    ];

    static public function message($data=[])
    {
        $array = [];
        $class  = debug_backtrace()[1]['class'] ? substr(strrchr(debug_backtrace()[1]['class'], "\\"), 1) : 'unknown';
        $method = debug_backtrace()[1]['function'] ? debug_backtrace()[1]['function'] : 'unknown';        
        $array['ip'] = isset($data['ip']) ? $data['ip'] : \Request::ip();
        $array['user_id'] = isset($data['user_id']) ? $data['user_id'] : (auth()->check() ? auth()->user()->id : 0);
        $array['content'] = (object)['Class'=>$class, 'Method'=>$method];
        $id = isset($data['model']->id) ? $data['model']->id : 0;
        $name = isset($data['model']->name) ? $data['model']->name : '';
        if (is_array($data)){
            $array['name'] = isset($data['message']) ? $data['message'] : trans('messages.'.$class.'-'.$method,$data);
            foreach ($data as $key => $param) {
                if( !in_array($key,['ip','user_id','message'])) {$array['content']->{$key} = $param;}
            }
        } else {
            $array['name'] = $data;
        }
        Track::create($array);
        return true;
    }
}
