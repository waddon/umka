<?php

namespace App;

use App;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Quiz extends Model implements TranslatableContract
{
    use Translatable;
    
    public $translatedAttributes = ['name', 'content', 'data'];
    protected $guarded = [];

    public function groups()
    {
        return $this->belongsToMany('App\Group');
    }

    static public function getData()
    {
        $array = [];
        foreach (Quiz::get() as $key => $quiz) {
            $array[$quiz->id]['name'] = $quiz->name;
            $temp = (object)[];
            foreach ($quiz->data as $question) {
                $temp->{$question->id} = $question->text;
            }
            $array[$quiz->id]['data'] = $temp;
        }
        return $array;
    }
}