<?php

namespace App;

use App;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;

class Module extends Model implements TranslatableContract
{
    use Translatable;
    
    public $translatedAttributes = ['name','messages'];
    protected $guarded = [];

    protected $casts = [
        'data' => 'object',
    ];

    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    public function lessons()
    {
        return $this->hasMany('App\Lesson');
    }

    public function activeLessons()
    {
        return $this->hasMany('App\Lesson')->where('activity','=',true);
    }

    public function themes()
    {
        return $this->hasMany('App\Theme');
    }

    public function activeThemes()
    {
        return $this->hasMany('App\Theme')->where('activity','=',true);
    }

    // public function activeThemes()
    // {
    //     $result = 0;
    //     foreach ($this->lessons as $key => $lesson) {
    //         $result += $lesson->themes->count();
    //     }
    //     return $this->lessons->count() . ' (' . $result . ')';
    // }

    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function activeQuestions()
    {
        return $this->hasMany('App\Question')->where('activity','=',true);
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group')->withPivot('date_start','date_finish','notificated');
    }

    static public function pluckTranslate($placeholder = 0)
    {
        $result = $placeholder ? [0=> __('Select') . ' '. __('Module')] : [];
        foreach (Module::get() as $key => $element) {
            $result[$element->id] = Option::getoption('keyOrName') ? $element->translate(App::getLocale())->name : $element->key;
        }
        return $result;
    }

    static public function point_ones()
    {
        $result = [
            0 => __('Select Type'),
            1 => __('pcs'),
            2 => __('%'),
        ];
        return $result;
    }

    static public function time_ones()
    {
        $result = [
            0 => __('Select Type'),
            1 => __('days'),
            2 => __('hours'),
            3 => __('minutes'),
            4 => __('seconds'),
        ];
        return $result;
    }

    static public function time_ones_seconds()
    {
        $result = [
            1 => 24*60*60,
            2 => 60*60,
            3 => 60,
            4 => 1,
        ];
        return $result;
    }

    public function getTimeForPassing()
    {
        $step = Module::time_ones_seconds();
        $time_for_passing_one = isset($this->data->time_for_passing_one) ? $step[$this->data->time_for_passing_one] : $step[1];
        $time_for_passing = isset($this->data->time_for_passing) ? (int)($this->data->time_for_passing)*$time_for_passing_one : $time_for_passing_one;
        return $time_for_passing;
    }

    public function firstThemeId()
    {
        // $element = $this->themes->sortBy('order')->first();
        $element = $this->activeThemes->sortBy('order')->first();
        $result = $element ? $element->id : 0;
        return $result;
    }


    public function allThemeId($array = [])
    {
        // foreach ($this->themes as $key => $theme) {
        foreach ($this->activeThemes as $key => $theme) {
            $array[$theme->id] = $theme->id;
        }
        return (object)$array;
    }

    # выбор вопросов для теста
    public function getTestQuestions()
    {
        # Получаем все вопросы с перемешанными ответами
            $questions = [];
            foreach ($this->activeQuestions()->orderBy('id','ASC')->get() as $key1 => $question) {
                $temp['id'] = $question->id;
                $temp['text'] = $question->name;
                $temp['type'] = $question->type_id;                
                $temp2 = [];
                if($question->content){
                    foreach ($question->content as $key2 => $answer) {
                        $temp2[] = (object)['id'=>$answer->id, 'text'=>$answer->text];
                    }
                }
                if (Option::getoption('shuffleQuestions')) {shuffle ( $temp2 );}
                $temp['answers'] = $temp2;
                $questions[] = $temp;
            }
        # перемешиваем массив и обрезаем, если вопросов больше необходимого количества
            if (Option::getoption('shuffleQuestions')) {shuffle ( $questions );}
            $count = isset($this->data->question_count) ? $this->data->question_count : 0;
            if ($count && $count<count($questions)){
                $questions = array_slice($questions, 0, $count);
            }
        return $questions;
    }


    # проверка ответов
    public function checkAnswers($array=[])
    {
        $element = 'modules';
        $result = [];
        $qcount = count($array);
        $qcorrect = 0;
        \Log::info( '=== Пользователь: ' . auth()->user()->name );
        foreach ($array as $key => $question) {
            if ($this->questions->where('id',$key)->first()->check($question)){
                $qcorrect +=  1;
            } else {
                //\Log::warning( $question );
                \Log::warning( 'Неправильный ответ на ' . $key . ' вопрос');
            }
        }
        \Log::info( '=== Правильных ответов - ' . $qcorrect );
        $available = $this->data->point_one == 1 ? $qcorrect : ($qcorrect/$qcount*100);

        # сохраняем результаты тестирования в метаданные пользователя
            $max_count_passing = isset($this->data->max_count_passing) ? (int)($this->data->max_count_passing) : 3;
            $data = auth()->user()->data;
            $user_count_passing = isset($data->{$element}->{$this->id}->passing_count) ? (int)$data->{$element}->{$this->id}->passing_count + 1 : 1;
            if(!isset($data->{$element}->{$this->id})) $data->{$element}->{$this->id} = (object)[];
            $data->{$element}->{$this->id}->last_passing_date = Carbon::now()->format('d.m.Y H:i:s');
            $data->{$element}->{$this->id}->passing_count = $user_count_passing;
            if($available >= (int)$this->data->point_score){
                $data->{$element}->{$this->id}->passed = true;
            }
            auth()->user()->data = $data;
            if ($user_count_passing >= $max_count_passing && !isset($data->{$element}->{$this->id}->passed)){
                auth()->user()->activity = 0;
            }
            auth()->user()->save();

        $result = ['question_count' => $qcount, 'question_correct' => $qcorrect, 'available' => $available, 'need' => $this->data->point_score, 'point_one'=> $this->data->point_one];
        return $result;
    }

}