<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;

use Carbon\Carbon;
use App\Option;
use App\Module;
use App\User;

class Group extends Model
{

    protected $guarded = [];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function modules()
    {
        return $this->belongsToMany('App\Module')->withPivot('date_start','date_finish','notificated');
    }

    public function quizzes()
    {
        return $this->belongsToMany('App\Quiz');
    }

    # дата запуска модуля
    public function getDateStart($module_id=0)
    {
        $module = Module::findOrFail($module_id);
        if (isset($this->modules->find($module->id)->pivot->date_start)){
            $result = Carbon::parse($this->modules->find($module->id)->pivot->date_start)->format('d.m.Y');
        } else {
            $result = '';
        }
        return $result;
    }

    # дата окончания модуля
    public function getDateFinish($module_id=0)
    {
        $module = Module::findOrFail($module_id);
        if (isset($this->modules->find($module->id)->pivot->date_finish)){
            $result = Carbon::parse($this->modules->find($module->id)->pivot->date_finish)->format('d.m.Y');
        } else {
            $result = '';
        }
        return $result;
    }

    # активен ли модуль в данный момент
    public function ifModuleActive($module_id=0)
    {
        $result = false;
        $module = Module::findOrFail($module_id);
        if (isset($this->modules->find($module->id)->pivot->date_start) && isset($this->modules->find($module->id)->pivot->date_finish)){
            $date_start  = Carbon::parse($this->modules->find($module->id)->pivot->date_start);
            $date_finish = Carbon::parse($this->modules->find($module->id)->pivot->date_finish);
            $now = Carbon::now();
            if ($now->gte($date_start) && $date_finish->gte($now)) {$result = true;}
        }
        return $result;
    }

    # будет ли активен модуль в будущем
    public function ifModuleFuture($module_id=0)
    {
        $result = false;
        $module = Module::findOrFail($module_id);
        if (isset($this->modules->find($module->id)->pivot->date_start) && isset($this->modules->find($module->id)->pivot->date_finish)){
            $date_start  = Carbon::parse($this->modules->find($module->id)->pivot->date_start);
            $date_finish = Carbon::parse($this->modules->find($module->id)->pivot->date_finish);
            $now = Carbon::now();
            if ($date_start->gte($now) && $date_finish->gte($now)) {$result = true;}
        }
        return $result;
    }

    # открытие тем в открытых модулях
    public function openThemesForUsers($user_id = 0)
    {
        $group = $this;
        # определяем список пользователей
            if($user_id){
                $user = User::findOrFail($user_id);
                $users[] = $user;
            } else {
                $users = User::whereHas('groups', function(Builder $query) use ($group){
                        $query->where('id', '=', $group->id);
                    })->get();
            }
        # проходим по всем прикрепленным модулям и пользователям
            foreach ($this->modules as $key => $module) {
                foreach ($users as $key => $user) {
                    $data = $user->data;
                    # открываем доступ к модулю пользователю
                        if (!isset($data->modules->{$module->id})) $data->modules->{$module->id} = (object)[];
                        $data->modules->{$module->id}->opened = true;
                    # анализируем опцию (одна тема или все)
                        if (Option::getoption('themeCount')){
                            $themes = $module->allThemeId();
                            foreach ($themes as $key => $theme) {
                                if (!isset($data->themes->{$theme})) $data->themes->{$theme} = (object)[];
                                $data->themes->{$theme}->opened = true;
                            }
                        } else {
                            $theme_id = $module->firstThemeId();
                            if (!isset($data->themes->{$theme_id})) $data->themes->{$theme_id} = (object)[];
                            $data->themes->{$theme_id}->opened = true;
                        }
                    # сохраняем данные
                        $user->data = $data;
                        $user->save();
                }
            }
        return true;
    }

    /**
     * Проверка активности опроса для группы пользователей
     *
     * @param  int  $quiz_id
     * @return  bool
     */
    public function ifQuizActive($quiz_id)
    {
        $result = false;
        if ($this->quizzes()->where('id', $quiz_id)->exists()) {$result = true;}
        return $result;
    }

}
