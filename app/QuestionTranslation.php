<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionTranslation extends Model
{

    protected $casts = [
        'content' => 'object',
    ];

    protected function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }
    
    public $timestamps = false;
    protected $fillable = ['name','content'];
}