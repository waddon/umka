<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{

	protected $table = 'medias';
    protected $guarded = [];

    static public function getPath($id=0)
    {
    	$result = '';
    	$model = Media::find($id);
    	if ($model){
    		$result = $model->url;
    	}
    	return $result;
    }

    static public function getMediaOptionsList()
    {
        $result = '';
        foreach (Media::where('type','=','video')->get() as $key => $video) {
            $result .= '<option data-type="'.$video->mimetype.'" value="/storage/'.$video->url.'">' . $video->name . '</option>';
        }
        return $result;
    }

    static public function getApplications()
    {
        return Media::where('type','=','application')->pluck('name','id')->prepend(__('Undefined'),0);
    }

}
