<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    // use SoftDeletes;

    protected $table = 'roles';
    public $primaryKey  = 'id';

    public function users()
    {
        return $this->belongsToMany('App\User', 'model_has_roles', 'role_id', 'model_id');
    }
}
