<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    protected $guarded = [];

    static public function getMessages($user_id = 0)
    {
    	return Message::where([['sender_id','=',$user_id,'or'],['receiver_id','=',$user_id,'or']])->orderBy('created_at','ASC')->get();
    }
}
