<?php

namespace App;

use App;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;

class Lesson extends Model implements TranslatableContract
{
    use Translatable;
    
    public $translatedAttributes = ['name'];
    protected $guarded = [];

    protected $casts = [
        'data' => 'object',
    ];

    public function module()
    {
        return $this->belongsTo('App\Module');
    }

    public function themes()
    {
        return $this->hasMany('App\Theme');
    }

    public function activeThemes()
    {
        return $this->hasMany('App\Theme')->where('activity','=',true)->orderBy('order','ASC');
    }

    public static function boot()
    {
        parent::boot();
        self::created(function($model){
            $model->data = (object)[
                    "introduction_id"=>"0",
                    "conclusion_id"=>"0"
                ];
            $model->save();
        });
    }

    static public function pluckTranslate($placeholder = 0)
    {
        $result = $placeholder ? [0=> __('Select') . ' '. __('Lesson')] : [];
        foreach (Lesson::get() as $key => $element) {
            $result[$element->id] = Option::getoption('keyOrName') ? $element->name : $element->key;
        }
        return $result;
    }

}