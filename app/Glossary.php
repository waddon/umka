<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Glossary extends Model
{

    protected $guarded = [];

    public static function boot()
    {
        parent::boot();
        self::created(function($model){
            $model->locale = config()->get('app.locale');
            $model->save();
        });
    }

}
