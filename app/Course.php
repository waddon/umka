<?php

namespace App;

use App;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use App\Option;
use Carbon\Carbon;
use App\Question;
use App\Media;

class Course extends Model implements TranslatableContract
{
    use Translatable;
    
    public $translatedAttributes = ['name','messages'];
    protected $guarded = [];

    protected $casts = [
        'data' => 'object',
    ];

    public function modules()
    {
        return $this->hasMany('App\Module');
    }

    static public function pluckTranslate($placeholder = 0)
    {
        $result = $placeholder ? [0=> __('Select') . ' '. __('Course')] : [];
        foreach (Course::get() as $key => $element) {
            $result[$element->id] = Option::getoption('keyOrName') ? $element->translate(App::getLocale())->name : $element->key;
        }
        return $result;
    }

    # выбор вопросов для теста
    public function getExamQuestions()
    {
        # Получаем все вопросы с перемешанными ответами
            $questions = [];
                foreach ($this->modules as $key1 => $module) {
                    $module_question = [];
                    foreach ($module->activeQuestions as $key2 => $question) {
                        $temp['id'] = $question->id;
                        $temp['text'] = $question->name . ' (module ' . $module->id .')';
                        $temp['type'] = $question->type_id;                
                        $temp2 = [];
                        if($question->content){
                            foreach ($question->content as $key2 => $answer) {
                                //$temp2[] = $answer->text;
                                $temp2[] = (object)['id'=>$answer->id, 'text'=>$answer->text];
                            }
                        }
                        if (Option::getoption('shuffleQuestions')) {shuffle ( $temp2 );}
                        $temp['answers'] = $temp2;
                        $module_question[] = $temp;
                    }
                    if (isset($this->data->way_get_count_of_question) && $this->data->way_get_count_of_question){
                        # перемешиваем вопросы модуля и обрезаем, если вопросов больше необходимого количества
                        if (Option::getoption('shuffleQuestions')) {shuffle ( $module_question );}
                        $count = isset($module->data->exam_question_count) ? $module->data->exam_question_count : 0;
                        if ($count && $count<count($module_question)){
                            $module_question = array_slice($module_question, 0, $count);
                        }
                    }
                    # добавляем вопросы модуля к общему списку вопросов
                    $questions = array_merge($questions, $module_question);
                }
        # перемешиваем массив и обрезаем, если вопросов больше необходимого количества
            if (Option::getoption('shuffleQuestions')) {shuffle ( $questions );}
            $count = isset($this->data->question_count) ? $this->data->question_count : 0;
            if ($count && $count<count($questions)){
                $questions = array_slice($questions, 0, $count);
            }
        return $questions;
    }


    public function getTimeForPassing()
    {
        $step = Module::time_ones_seconds();
        $time_for_passing_one = isset($this->data->time_for_passing_one) ? $step[$this->data->time_for_passing_one] : $step[1];
        $time_for_passing = isset($this->data->time_for_passing) ? (int)($this->data->time_for_passing)*$time_for_passing_one : $time_for_passing_one;
        return $time_for_passing;
    }


    # проверка ответов
    public function checkAnswers($array=[],$id=0)
    {
        $element = Course::findOrFail($id);
        $variety = 'courses';
        $result = [];
        $qcount = count($array);
        $qcorrect = 0;
        \Log::info( '=== Пользователь: ' . auth()->user()->name );
        foreach ($array as $key => $question) {
            //$qcorrect +=  Question::where('id',$key)->first()->check($question);
            if (Question::where('id',$key)->first()->check($question)){
                $qcorrect +=  1;
            } else {
                \Log::warning( 'Неправильный ответ на ' . $key . ' вопрос');
            }
        }
        \Log::info( '=== Правильных ответов - ' . $qcorrect );
        $available = $element->data->point_one == 1 ? $qcorrect : ($qcorrect/$qcount*100);

        # сохраняем результаты тестирования в метаданные пользователя
            $max_count_passing = isset($element->data->max_count_passing) ? (int)($element->data->max_count_passing) : 3;
            $data = auth()->user()->data;
            $user_count_passing = isset($data->{$variety}->{$id}->passing_count) ? (int)$data->{$variety}->{$id}->passing_count + 1 : 1;
            if(!isset($data->{$variety}->{$id})) $data->{$variety}->{$id} = (object)[];
            $data->{$variety}->{$id}->last_passing_date = Carbon::now()->format('d.m.Y H:i:s');
            $data->{$variety}->{$id}->passing_count = $user_count_passing;
            if($available >= (int)$element->data->point_score){
                $data->{$variety}->{$id}->passed = true;
            }
            auth()->user()->data = $data;
            if ($user_count_passing >= $max_count_passing && !isset($data->{$variety}->{$id}->passed)){
                auth()->user()->activity = 0;
            }
            auth()->user()->save();

        $result = ['question_count' => $qcount, 'question_correct' => $qcorrect, 'available' => $available, 'need' => $element->data->point_score, 'point_one'=> $element->data->point_one];
        return $result;
    }

    public function getCertificateUrl()
    {
        $result = '#';
        $media = isset($this->data->certificate) ? Media::find($this->data->certificate) : false;
        if ($media) $result = '/storage/' . $media->url;
        return $result;
    }
}