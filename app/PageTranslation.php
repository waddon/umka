<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageTranslation extends Model
{

    // protected $casts = [
    //     'content' => 'object',
    // ];

    public $timestamps = false;
    protected $fillable = ['name','content'];
}