<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Carbon\Carbon;
use App\Course;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $dates = ['birthday', 'last_action_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'data' => 'object',
    ];

    public function groups()
    {
        return $this->belongsToMany('App\Group');
    }

    public function themes()
    {
        return $this->belongsToMany('App\Theme');
    }

    public function outMessages()
    {
        return $this->hasMany('App\Message','sender_id','id');
    }

    public function inMessages()
    {
        return $this->hasMany('App\Message','receiver_id','id');
    }

    public function getCategories()
    {
        $categories = [
            "1"=>__('Political Persons'),
            "2"=>__('Deputies of local councils'),
            "3"=>__('Heads of local government'),
            "4"=>__('Law enforcement officers'),
            "5"=>__('Persons who graduated from a university and are not public servants'),
            "6"=>__('Others'),
        ];
        return $categories;
    }

    public static function boot()
    {
        parent::boot();
        self::created(function($model){
            $model->data = (object)[
                'projects' => (object)[],
                'modules'  => (object)[],
                'courses'  => (object)[],
                'themes'   => (object)[],
            ];
            $model->save();
        });
    }

    // public function uriSegment($segment=0)
    // {
    //     $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    //     $uri_segments = explode('/', $uri_path);
    //     if (in_array($uri_segments[1], config()->get('app.locales')) || $uri_segments[1]=='') {
    //         unset($uri_segments[1]); //удаляем метку локали
    //     }
    //     # переиндексируем массив
    //     $uri_segments = array_values($uri_segments);
    //     $result = isset($uri_segments[$segment]) ? $uri_segments[$segment] : '';
    //     return $result;
    // }

    public function getAvatar()
    {
        return ($this->avatar) ? '/storage/' . $this->avatar : '/assets/img/placeholders/user2.png';
    }

    # проверка открытости темы для конкретного пользователя
    public function ifActiveTheme($theme_id=0)
    {
        $result = false;
        //if ($this->themes()->where('id',$theme_id)->exists()) {$result = true;}
        if (isset($this->data->themes->{$theme_id}->opened)) {$result = true;}
        return $result;
    }

    # список все тем, к которым есть доступ
    public function openThemes()
    {
        $result = [];
        foreach ($this->themes as $key => $theme) {
            $result[] = $theme->id;
        }
        return $result;
    }

    # открытие следующей темы
    public function openNextTheme($theme_id=0)
    {
        $result = 0;
        $percent = 0;
        $module_id = 0;
        # делаем отметку о прохождении темы
            $user = auth()->user();
            $data = $user->data;
            if (!isset($data->themes->{$theme_id})) $data->themes->{$theme_id} = (object)[];
            $data->themes->{$theme_id}->passed = true;
            $data->themes->{$theme_id}->last_passing_date = Carbon::now()->format('d.m.Y H:i:s');
            $user->data = $data;
            $user->save();
        # находим текущую тему и открываем следующую
            $theme = Theme::find($theme_id);
            if ($theme){
                # выбираем следующую тему текущего модуля
                    $where = [
                        ['module_id','=',$theme->module_id],
                        ['order','>',$theme->order],
                        ['activity','=',true]
                    ];
                    $newTheme = Theme::where($where)->orderBy('order')->first();
                    if ($newTheme){
                        $result = $newTheme->id;
                        // $array = $this->openThemes();
                        // $array[] = $result;
                        // $this->themes()->sync($array);
                        # делаем отметку об открытии
                            $data = $user->data;
                            if (!isset($data->themes->{$result})) $data->themes->{$result} = (object)[];
                            $data->themes->{$result}->opened = true;
                            $user->data = $data;
                            $user->save();
                    }
                $module_id = $theme->module->id;
                $percent = $this->openThemesPercent($module_id);
            }
        return ['result'=>$result,'module'=>$module_id,'percent'=>$percent,'openTest'=>$this->ifOpenTest($module_id)];
    }

    public function openThemesPercent($module_id=0)
    {
        $result = 0;
        $data = $this->data;
        $module = Module::findOrfail($module_id);
        $themes = $module->activeThemes->count();
        $openthemes = 0;
        foreach ($module->activeThemes as $key => $theme) {
            $openthemes += (isset($data->themes->{$theme->id}->passed)) ? 1 : 0;
        }
        $result = $themes>0 ? round($openthemes/$themes*100) : 100;
        return $result;
    }

    public function themesPassedCount($module_id=0)
    {
        $result = 0;
        $data = $this->data;
        $module = Module::findOrfail($module_id);
        $passed_themes = 0;
        foreach ($module->activeThemes as $key => $theme) {
            $passed_themes += (isset($data->themes->{$theme->id}->passed)) ? 1 : 0;
        }
        return $passed_themes;
    }

    public function openAllThemesPercent($course_id=0)
    {
        $result = 0;
        $data = $this->data;
        $course = Course::findOrfail($course_id);
        $themes = 0;
        $openthemes = 0;
        foreach ($course->modules as $key => $module) {
            $themes += $module->activeThemes->count();
            foreach ($module->themes as $key => $theme) {
                $openthemes += (isset($data->themes->{$theme->id}->passed)) ? 1 : 0;
            }
        }

        $result = $themes>0 ? round($openthemes/$themes*100) : 100;
        return $result;
    }

    # валидация начала тестирования
    public function validatePassingTest($id = 0)
    {
        $result = 'Ok';
        $module = Module::findOrFail($id);

            if (isset($this->data->modules->{$id})){
                # если уже сдан
                    if (isset($this->data->modules->{$id}->passed)){
                        $result = isset($module->messages->already) ? $module->messages->already : 'Already Passed';
                    }
                # если количество попыток исчерпано
                    $max_count_passing = isset($module->data->max_count_passing) ? (int)($module->data->max_count_passing) : 3;
                    $user_count_passing = isset($this->data->modules->{$id}->passing_count) ? (int)$this->data->modules->{$id}->passing_count : 0;
                    if ($user_count_passing >= $max_count_passing){
                        $result = isset($module->messages->exhausted) ? $module->messages->exhausted : 'Attempts exhausted';
                    }
                # если еще не прошел временной промежуток между попытками
                    $step = Module::time_ones_seconds();
                    $time_between_passing_one = isset($module->data->time_between_passing_one) ? $step[$module->data->time_between_passing_one] : $step[1];

                    $time_between_passing = isset($module->data->time_between_passing) ? (int)($module->data->time_between_passing)*$time_between_passing_one : $time_between_passing_one;
                    $user_last_passing = isset($this->data->modules->{$id}->last_passing_date) ? Carbon::parse($this->data->modules->{$id}->last_passing_date)->addSeconds($time_between_passing) : false;
                    if ( $user_last_passing && $user_last_passing->gte(Carbon::now())){
                        $result = (isset($module->messages->wait) ? $module->messages->wait : 'Need to wait for ') . $user_last_passing->format('d.m.Y') . __(' in ') . $user_last_passing->format('H:i');
                    }
            }

        return $result;
    }

    # валидация начала тестирования
    public function validatePassingExam($id = 0)
    {
        $result = 'Ok';
        $module = Course::findOrFail($id);

            if (isset($this->data->courses->{$id})){
                # если уже сдан
                    if (isset($this->data->courses->{$id}->passed)){
                        $result = isset($module->messages->already) ? $module->messages->already : 'Already Passed';
                    }
                # если количество попыток исчерпано
                    $max_count_passing = isset($module->data->max_count_passing) ? (int)($module->data->max_count_passing) : 3;
                    $user_count_passing = isset($this->data->courses->{$id}->passing_count) ? (int)$this->data->courses->{$id}->passing_count : 0;
                    if ($user_count_passing >= $max_count_passing){
                        $result = isset($module->messages->exhausted) ? $module->messages->exhausted : 'Attempts exhausted';
                    }
                # если еще не прошел временной промежуток между попытками
                    $step = Module::time_ones_seconds();
                    $time_between_passing_one = isset($module->data->time_between_passing_one) ? $step[$module->data->time_between_passing_one] : $step[1];

                    $time_between_passing = isset($module->data->time_between_passing) ? (int)($module->data->time_between_passing)*$time_between_passing_one : $time_between_passing_one;
                    $user_last_passing = isset($this->data->courses->{$id}->last_passing_date) ? Carbon::parse($this->data->courses->{$id}->last_passing_date)->addSeconds($time_between_passing) : false;
                    if ( $user_last_passing && $user_last_passing->gte(Carbon::now())){
                        $result = (isset($module->messages->wait) ? $module->messages->wait : 'Need to wait for ') . $user_last_passing->format('d.m.Y') . __(' in ') . $user_last_passing->format('H:i');
                    }
            }

        return $result;
    }

    # проверка модуля на прохождение всех тем и наличия вопросов к вебинару
    public function ifOpenTest($id=0)
    {
        $result = false;
        $data = auth()->user()->data;
        $moduledata = Module::findOrFail($id)->data;
        $count = 0;
        if (isset($data->webquestions) && is_array($data->webquestions)){
            foreach ($data->webquestions as $key => $webquestion) {
                $count++; 
            }
        }
        if ($this->openThemesPercent($id) == 100 && ($count>0 || !isset($moduledata->required_webquestions) || !$moduledata->required_webquestions)) {$result=true;}
        return $result;
    }

    # проверка курса на прохождение всех модулей
    public function ifOpenExam($id=0)
    {
        $result = false;
        $course = Course::findOrFail($id);
        $passed_modules = 0;
        foreach ($course->modules as $key => $module) {
            if (isset($this->data->modules->{$module->id}->passed)) $passed_modules++;
        }
        if ($course->modules->count()==$passed_modules && !isset(auth()->user()->data->courses->{$id}->passed)) $result=true;
        return $result;
    }

    public function tableModules()
    {
        $result = [];
        $courses = Course::get();
        foreach ($courses as $ckey => $course) {
            $course_row = (object)[
                'course'=>true,
                'name'=>$course->name,
                'themecount'=>0,
                'themepassed'=>0,
                'passing_count'=>isset($this->data->courses->{$course->id}->passing_count) ? $this->data->courses->{$course->id}->passing_count : 0,
                'last_passing_date'=> isset($this->data->courses->{$course->id}->last_passing_date) ? Carbon::parse($this->data->courses->{$course->id}->last_passing_date)->format('d.m.Y') : '',
                'passed'=>isset($this->data->courses->{$course->id}->passed) ? __('Passed') : '',
            ];
            foreach ($course->modules as $key => $module) {
                $row = (object)[];
                $row->name = $module->name;
                $row->themecount = $module->activeThemes->count();
                $course_row->themecount += $row->themecount;

                $themepassed = 0;
                foreach ($module->themes as $keythemes => $theme) {
                    if (isset($this->data->themes->{$theme->id}->passed)) {$themepassed++;}
                }
                $row->themepassed = $themepassed;
                $course_row->themepassed += $themepassed;

                $row->passing_count = isset($this->data->modules->{$module->id}->passing_count) ? $this->data->modules->{$module->id}->passing_count : 0;
                $row->last_passing_date = isset($this->data->modules->{$module->id}->last_passing_date) ? Carbon::parse($this->data->modules->{$module->id}->last_passing_date)->format('d.m.Y') : '';
                $row->passed = isset($this->data->modules->{$module->id}->passed) ? __('Passed') : '';

                $result[] = $row;
            }
            $result[] = $course_row;
        }
        // $modules = Module::get();
        // foreach ($modules as $key => $module) {
        //     $row = (object)[];
        //     $row->name = $module->name;
        //     $row->themecount = $module->activeThemes->count();
        //     $themepassed = 0;
        //     foreach ($module->themes as $keythemes => $theme) {
        //         if (isset($this->data->themes->{$theme->id}->passed)) {$themepassed++;}
        //     }
        //     $row->themepassed = $themepassed;
        //     $row->passing_count = isset($this->data->modules->{$module->id}->passing_count) ? $this->data->modules->{$module->id}->passing_count : 0;
        //     $row->last_passing_date = isset($this->data->modules->{$module->id}->last_passing_date) ? Carbon::parse($this->data->modules->{$module->id}->last_passing_date)->format('d.m.Y') : '';
        //     $row->passed = isset($this->data->modules->{$module->id}->passed) ? __('Passed') : '';

        //     $result[] = $row;
        // }

        return $result;
    }
}
