<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;
use Carbon\Carbon;

class ActiveMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Разрешаем работу если подльзователь активен
        if (Auth::check()) {
            if (!auth()->user()->activity) abort('403');
        }
        return $next($request);
    }
}