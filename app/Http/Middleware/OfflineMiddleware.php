<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

use App\User;
use App\Option;

class OfflineMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $offlineKey = Option::getOption('offlineKey');
        if (isset($request->offline) && $request->offline==$offlineKey){
            \Log::info('Offline middleware');
            $user = User::find( Option::getOption('offlineUser') );
            if ($user) {
                Auth::login($user);
            }
        }

        return $next($request);
    }
}