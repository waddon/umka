<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Theme;

class ThemeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            if (!Auth::check()) abort('403');
            # проверяем все группы пользователя на наличие доступа к модулю
                $module_id = Theme::findOrfail($request->id)->module->id;
                $user = Auth::user();
                $result = false;
                foreach ($user->groups as $key => $group) {
                    if ($group->ifModuleActive($module_id)) $result=true;
                }
                if (!$result) abort('403');
            if (!isset(Auth::user()->data->themes->{$request->id}->opened) ) {
                abort('403');
            }

        return $next($request);
    }
}