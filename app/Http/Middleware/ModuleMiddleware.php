<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class ModuleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) abort('403');
        # проверяем все группы пользователя на наличие доступа к модулю
            $user = Auth::user();
            $result = false;
            foreach ($user->groups as $key => $group) {
                if ($group->ifModuleActive($request->id)) $result=true;
            }
            if (!$result) abort('403');
        # если нет разрешения - то разрещаем вход
            if ( !isset($user->data->modules->{$request->id}->opened)) {
                abort('403');
            }

        return $next($request);
    }
}