<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'key'=>'required|max:255',
            'point_score'=>'required',
            'point_one' => 'integer|min:1',
            'question_count'=>'required',

            'time_for_passing'=>'required',
            'time_for_passing_one' => 'integer|min:1',
            'time_between_passing'=>'required',
            'time_between_passing_one' => 'integer|min:1',
            'max_count_passing'=>'required',
        ];
    }
}
