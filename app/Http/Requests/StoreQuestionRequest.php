<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->parameter('theme') ? $this->route()->parameter('theme') : 'NULL';
        return [
            'key'=>'required|max:255',
            'module_id' => 'integer|min:1',
            'type_id' => 'integer|min:1',
        ];
    }
}
