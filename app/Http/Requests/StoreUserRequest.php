<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->parameter('user') ? $this->route()->parameter('user') : 'NULL';
        return [
            'email'=>'required|email|unique:users,email,' . $id,
            'name' => 'required|string|max:255',
            'password' => 'required|string|min:6|max:255|same:password_confirmation',
            'password_confirmation' => 'required|string|max:255|same:password',
        ];
    }
}
