<?php

namespace App\Http\Controllers\Dashboard;

use App\Traits\BreadcrumbTrait;

use Illuminate\Http\Request;
use App\Http\Requests\StoreQuestionRequest;
use Illuminate\Support\Facades\Auth;
use App\Option;

use App\Http\Controllers\Controller;
use App\Question;
use App\Module;
use App;

class QuestionController extends Controller
{

    use BreadcrumbTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        \App\Track::message();
        return view('dashboard.questions.index', [
            'modules' => Module::pluckTranslate(),
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        \App\Track::message();
        return view('dashboard.questions.form', [
                'model'=> new Question,
                'modules'=> Module::pluckTranslate(1),
                'types'=> Question::types(),
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreQuestionRequest $request)
    {
        $array = [
                'key'=>$request->key,
                'module_id'=>$request->module_id,
                'type_id'=>$request->type_id,
            ];
        foreach (config()->get('app.locales') as $key => $language) {
            $array[$language] = [
                'name'=>$request->{'name-'.$language},
                'content'=>json_decode($request->{'content-'.$language}),
            ];
        }

        $model = Question::create($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return response([
            'message' => __('Element created successfully'),
            'formAction' => route('questions.update', $model->id),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Question::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.questions.show', [
                'model'=>$model,
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Question::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.questions.form', [
                'model'=>$model,
                'modules'=>Module::pluckTranslate(),
                'types'=> Question::types(),
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreQuestionRequest $request, $id)
    {
        $model = Question::findOrFail($id);
        $array = [
                'key'=>$request->key,
                'module_id'=>$request->module_id,
                'type_id'=>$request->type_id,
            ];
        foreach (config()->get('app.locales') as $key => $language) {
            $array[$language] = [
                'name'=>$request->{'name-'.$language},
                'content'=>json_decode($request->{'content-'.$language}),
            ];
        }

        $model->update($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return response([
            'message' => __('Element updated successfully'),
            'formAction' => route('questions.update', $model->id),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Question::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        $model->delete();
        echo json_encode(true);
        die();
    }

    # функция для получения аяксом данных для datatable
    public function datatable(Request $request)
    {
        $where = [];
        if ($request->module_id){
            $where = [['module_id','=',$request->module_id]];
        }
        $columns = [
            0 => 'id',
            1 => 'key',
            2 => 'module_id',
            3 => 'activity',
        ];

        $totalData = Question::where($where)->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $temp = Question::where($where)->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $temp = Question::where($where)->where([["id","=","{$search}","or"],['key','like',"%{$search}%","or"]])
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $temp = Question::where($where)->where([["id","=","{$search}","or"],['key','like',"%{$search}%","or"]]);
                $totalFiltered = $temp->count();
        }
        
        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['id'] = $model->id;
                $nestedData['name'] = Option::getoption('keyOrName') ? $model->translate(App::getLocale())->name : $model->key;
                $nestedData['module'] = Option::getoption('keyOrName') ? $model->module->translate(App::getLocale())->name : $model->module->key;
                $nestedData['type'] = $model->type();
                $nestedData['activity'] = $model->activity ? '<i class="icon-eye icon-2x text-success-600"></i>' : '<i class="icon-eye-blocked icon-2x" style="color:#eee;"></i>';
                $nestedData['actions'] = '<div class="list-icons">';
                    $nestedData['actions'] .= '<a href="'.route('questions.edit', $model->id).'" class="list-icons-item" title="'.__('Edit').'"><i class="icon-pencil7"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('questions.setactivity', $model->id).'" class="list-icons-item ajax-action" title="'.__('Change Activity').'"><i class="icon-switch22"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('questions.show', $model->id).'" class="list-icons-item" title="'.__('Preview').'"><i class="icon-eye"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('questions.destroy', $model->id).'" class="list-icons-item text-danger-600 btn-ajax-destroy" title="'.__('Delete').'" data-toggle="modal" data-target="#modal-ajax-destroy"><i class="icon-trash"></i></a>';
                $nestedData['actions'] .= '</div>';
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        echo json_encode($json_data);
    }

    public function setactivity(Request $request, $id=0)
    {
        $model = Question::findOrFail($id);
        $model->activity = $model->activity == false;
        $model->save();
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        echo json_encode(true);
    }
}
