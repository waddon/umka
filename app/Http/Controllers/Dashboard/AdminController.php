<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Traits\BreadcrumbTrait;

use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;

use App\User;
use App\Group;
use App\Media;
use App\Module;
use App\Option;
use App\Track;
use App\Page;
use App\Theme;
use Auth;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Storage;
use ZipArchive;

use Session;

class AdminController extends Controller 
{ 

    use BreadcrumbTrait;

    public function index()
    {
        return view('dashboard.index', [
            'data' => self::getBreadcrumb(),
        ]);
    }

    public function run(Request $request)
    {
        $module = Module::findOrFail($request->module_id);
        $group_id  = $request->group_id  ? $request->group_id  : 0;
        $array = [];
        if ($module && $group_id) {
            # вытаскиваем все имеющиеся группы группы
                foreach ($module->groups as $key => $group) {
                    $array[$group->id] = [
                        'date_start'=>$group->pivot->date_start,
                        'date_finish'=>$group->pivot->date_finish,
                        'notificated'=>$group->pivot->notificated,
                    ];
                }
            # добавляем или меняем данные з формы
                if ( Carbon::parse($request->date_finish)->lt(Carbon::now()) ) {
                    unset($array[$group_id]);
                    # отправляем всем пользователям письма об остановке модуля
                        if (Option::getOption('mailModuleStop')){
                            $group = Group::find($group_id);
                            if ($group){
                                foreach ($group->users as $key => $user) {
                                    $to = $user->email;
                                    $data = [
                                        'subject' => __('Module stop'),
                                        'module' => $module->name,
                                    ];
                                    Mail::to( $to )->send(new OrderShipped($data));
                                }
                            }
                        }
                } else {
                    $array[$group_id] = [
                        'date_start'=>$request->date_start,
                        'date_finish'=>$request->date_finish,
                        'notificated'=>false,
                    ];
                    # отправляем всем пользователям письма о запуске (изменении строков работы) модуля
                        if (Option::getOption('mailModuleStop')){
                            $group = Group::find($group_id);
                            if ($group){
                                foreach ($group->users as $key => $user) {
                                    $to = $user->email;
                                    $data = [
                                        'template' => 'runmodule',
                                        'subject' => __('Module start'),
                                        'module' => $module->name,
                                        'date_start'=>$request->date_start,
                                        'date_finish'=>$request->date_finish,
                                    ];
                                    Mail::to( $to )->send(new OrderShipped($data));
                                }
                            }
                        }
                }
            # смнхронизируем
                $module->groups()->sync( $array );
            # предоставление доступа пользователям
                $group = Group::findOrFail($group_id);
                $group->openThemesForUsers();
            \App\Track::message(['moduleName'=>$module->name, 'groupName'=>$group->name, 'date1'=>$request->date_start, 'date2'=>$request->date_finish]);        
        }
        echo json_encode([
            'module_id'=>$request->module_id,
            'date_start'=>$group->getDateStart($request->module_id),
            'date_finish'=>$group->getDateFinish($request->module_id),
            'active'=>$group->ifModuleActive($request->module_id),
        ]);
    }


    public function settings(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value) {
                Option::setOption($key, $value ? $value : '');
            }
            \App\Track::message();
            return redirect()->route('settings')
                ->with('flash_message', __('Settings updated successfully'));
        }        
        return view('dashboard.settings', [
            'documents' => Media::where('type','=','application')->get(),
            'users' => User::get(),
            'options' => Option::pluck('value','key'),
            'data' => self::getBreadcrumb(),
        ]);
    }

    public function tracks(Request $request)
    {
        if ($request->isMethod('post')) {
            Track::truncate();
            \App\Track::message();
            return redirect()->route('tracks')
                ->with('flash_message', __('Event Log was cleared'));
        }        
        return view('dashboard.tracks', [
            'data' => self::getBreadcrumb(),
        ]);
    }

    public function tracklist(Request $request)
    {
        $where = [];
        $columns = [
            0 => 'created_at',
            1 => 'user_id',
            2 => 'ip',
            3 => 'name',
        ];

        $totalData = Track::where($where)->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $temp = Track::where($where)->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $temp = Track::where($where)->where('name','like',"%{$search}%")
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $temp = Track::where($where)->where('name','like',"%{$search}%");
                $totalFiltered = $temp->count();
        }
        
        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['date'] = $model->created_at->format('d.m.Y H:i:s');
                $user = User::find($model->user_id);
                $nestedData['user'] = $model->user_id==-1 ? __('System') : ($user ? $user->name : __('Unknown'));
                $nestedData['ip'] = $model->ip;
                $nestedData['name'] = $model->name;
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        echo json_encode($json_data);
    }

    # делаем правильную ссылку для оффлайна
    function offlineUri($uri = '', $prefix = '')
    {
        $result = $uri;
        $base_url = route('home');
        $new_uri  = str_replace($base_url, '', $uri);
        if ($uri != $new_uri){
            $links = explode('/',$new_uri);
            if (count($links)==1) {$new_uri='index';}
            if (strpos($new_uri, '.') === false) {$new_uri .= '.html';}
            if (substr($new_uri,0,1)=='/') {$new_uri=substr($new_uri,1);}
            $result = $prefix . $new_uri;
        }
        return $result;
    }

    # получение контента по урлу
    public function getDataFromUrl($url, $level=0)
    {
        # получаем данные из файла
            $opts = [
                'http' => [
                    'method' => "GET",
                    'header' => "Accept-language: ".\App::getLocale()."\r\n"
                ]
            ];
            $context = stream_context_create($opts);
            for ($i=0; $i < 10; $i++) { 
                $html_request = @file_get_contents($url, false, $context);
                if ($html_request!==false){
                    break;
                }
            }
        # создаем префикс
            $prefix = '';
            for ($i=0; $i < $level; $i++) { 
                $prefix .= '../';
            }
        # меняем пути для ассетов
            $source  = [ '/css/', '/assets/', '/js/', '../../../storage', '/storage/' ];
            $replace = [ $prefix . 'css/', $prefix . 'assets/', $prefix . 'js/', $prefix . 'storage/', $prefix . 'storage/' ];
            $result = str_replace($source, $replace, $html_request);
        # находи все ссылки и меняем их на локальные, 
            preg_match_all('|href="([^"]*)"|U',$result,$matches); # простые ссылки
            foreach ($matches[1] as $key => $match) {
                $matches[2][$key] = 'href="' . $this->offlineUri($match,$prefix) . '"';
                $result = str_replace($matches[0][$key], $matches[2][$key], $result);
            }

        return $result;
    }

    # phpinfo
    public function phpinfo()
    {
        return view('dashboard.phpinfo', [
            'data' => self::getBreadcrumb(),
        ]);
    }

}