<?php

namespace App\Http\Controllers\Dashboard;

use App\Traits\BreadcrumbTrait;

use Illuminate\Http\Request;
use App\Http\Requests\StorePageRequest;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Page;
use App\Option;
use App\Media;
use App\Glossary;
use App;

class PageController extends Controller
{

    use BreadcrumbTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {        
        \App\Track::message();
        return view('dashboard.pages.index', [
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Page;
        \App\Track::message();
        return view('dashboard.pages.form', [
                'model'=>$model,
                'medias'=>Media::orderBy('name')->get(),
                'glossaries'=>Glossary::where('locale','=',App::getLocale())->orderBy('name')->get(),
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePageRequest $request)
    {
        $array = [
                'key'=>$request->key,
                'url'=>$request->url,
            ];
        foreach (config()->get('app.locales') as $key => $language) {
            $array[$language] = [
                'name'=>$request->{'name-'.$language},
                'content'=>$request->{'content-'.$language},
            ];
        }

        $model = Page::create($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return response([
            'message' => __('Element created successfully'),
            'formAction' => route('pages.update', $model->id),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Page::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.pages.show', [
                'model'=>$model,
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Page::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.pages.form', [
                'model'=>$model,
                'medias'=>Media::orderBy('name')->get(),
                'glossaries'=>Glossary::where('locale','=',App::getLocale())->orderBy('name')->get(),
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePageRequest $request, $id)
    {
        $model = Page::findOrFail($id);
        $array = [
                'key'=>$request->key,
                'url'=>$request->url,
            ];
        foreach (config()->get('app.locales') as $key => $language) {
            $array[$language] = [
                'name'=>$request->{'name-'.$language},
                'content'=>$request->{'content-'.$language},
            ];
        }

        $model->update($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return response([
            'message' => __('Element updated successfully'),
            'formAction' => route('pages.update', $model->id),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Page::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        $model->delete();
        echo json_encode(true);
        die();
    }

    # функция для получения аяксом данных для datatable
    public function datatable(Request $request)
    {
        $where = [];
        $columns = [
            0 => 'id',
        ];

        $totalData = Page::where($where)->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $temp = Page::where($where)->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $temp = Page::where($where)->where([["id","=","{$search}","or"],['key','like',"%{$search}%","or"]])
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $temp = Page::where($where)->where([["id","=","{$search}","or"],['key','like',"%{$search}%","or"]]);
                $totalFiltered = $temp->count();
        }
        
        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['id'] = $model->id;
                $nestedData['name'] = Option::getoption('keyOrName') ? $model->translate(App::getLocale())->name : $model->key;
                $nestedData['url'] = $model->url;
                $nestedData['actions'] = '<div class="list-icons">';
                    $nestedData['actions'] .= '<a href="'.route('pages.edit', $model->id).'" class="list-icons-item" title="'.__('Edit').'"><i class="icon-pencil7"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('pages.show', $model->id).'" class="list-icons-item" title="'.__('Preview').'"><i class="icon-eye"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('pages.destroy', $model->id).'" class="list-icons-item text-danger-600 btn-ajax-destroy" title="'.__('Delete').'" data-toggle="modal" data-target="#modal-ajax-destroy"><i class="icon-trash"></i></a>';
                $nestedData['actions'] .= '</div>';
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        echo json_encode($json_data);
    }

}
