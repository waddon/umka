<?php

namespace App\Http\Controllers\Dashboard;

use App\Traits\BreadcrumbTrait;

use Illuminate\Http\Request;
use App\Http\Requests\StoreGlossaryRequest;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Glossary;

class GlossaryController extends Controller
{

    use BreadcrumbTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {        
        \App\Track::message();
        return view('dashboard.glossaries.index', [
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Glossary;
        \App\Track::message();
        return view('dashboard.glossaries.form', [
                'model'=>$model,
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGlossaryRequest $request)
    {
        $array = [
                'name'=>$request->name,
                'description'=>$request->description,
            ];
        $model = Glossary::create($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return response([
            'message' => __('Element created successfully'),
            'formAction' => route('glossaries.update', $model->id),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Glossary::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.glossaries.form', [
                'model'=>$model,
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreGlossaryRequest $request, $id)
    {
        $model = Glossary::findOrFail($id);
        $array = [
                'name'=>$request->name,
                'description'=>$request->description,
            ];
        $model->update($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return response([
            'message' => __('Element updated successfully'),
            'formAction' => route('glossaries.update', $model->id),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Glossary::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        $model->delete();
        echo json_encode(true);
        die();
    }

    # функция для получения аяксом данных для datatable
    public function datatable(Request $request)
    {
        $where = [[ 'locale', '=', config()->get('app.locale') ]];
        $columns = [
            0 => 'id',
            1 => 'name',
            2 => 'description',
        ];

        $totalData = Glossary::where($where)->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $temp = Glossary::where($where)->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $temp = Glossary::where($where)->where([['name','like',"%{$search}%",'or'],['description','like',"%{$search}%",'or']])
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $temp = Glossary::where($where)->where([['name','like',"%{$search}%",'or'],['description','like',"%{$search}%",'or']]);
                $totalFiltered = $temp->count();
        }
        
        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['id'] = $model->id;
                $nestedData['name'] = $model->name;
                $nestedData['description'] = $model->description;
                $nestedData['activity'] = $model->activity ? '<i class="icon-eye icon-2x text-success-600"></i>' : '<i class="icon-eye-blocked icon-2x" style="color:#eee;"></i>';
                $nestedData['actions'] = '<div class="list-icons">';
                    $nestedData['actions'] .= '<a href="'.route('glossaries.edit', $model->id).'" class="list-icons-item" title="'.__('Edit').'"><i class="icon-pencil7"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('glossaries.setactivity', $model->id).'" class="list-icons-item ajax-action" title="'.__('Change Activity').'"><i class="icon-switch22"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('glossaries.destroy', $model->id).'" class="list-icons-item text-danger-600 btn-ajax-destroy" title="'.__('Delete').'" data-toggle="modal" data-target="#modal-ajax-destroy"><i class="icon-trash"></i></a>';
                $nestedData['actions'] .= '</div>';
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        echo json_encode($json_data);
    }


    public function setactivity(Request $request, $id=0)
    {
        $model = Glossary::findOrFail($id);
        $model->activity = $model->activity == false;
        $model->save();
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        echo json_encode(true);
    }
}
