<?php

namespace App\Http\Controllers\Dashboard;

use App\Traits\BreadcrumbTrait;

use Illuminate\Http\Request;
use App\Http\Requests\StoreModuleRequest;
use Illuminate\Support\Facades\Auth;
use App\Option;
use App\Page;

use App\Http\Controllers\Controller;
use App\Module;
use App\Group;
use App\Course;
use Carbon\Carbon;
use App;

class ModuleController extends Controller
{

    use BreadcrumbTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {        
        \App\Track::message();
        return view('dashboard.modules.index', [
            'groups' => Group::get(),
            'modules' => Module::get(),
            'courses' => Course::pluckTranslate(),
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Module;
        \App\Track::message();
        return view('dashboard.modules.form', [
                'model'=>$model,
                'courses'=>Course::pluckTranslate(1),
                'pages'=>Page::pluckTranslate(1),
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreModuleRequest $request)
    {
        $array = [
                'key'=>$request->key,
                'course_id'=>$request->course_id,
                'data'=>(object)[
                    'introduction_id'=>$request->introduction_id,
                    'conclusion_id'=>$request->conclusion_id,
                    'literature_id'=>$request->literature_id,
                    'webquestions'=>$request->webquestions,
                    'required_webquestions'=>$request->required_webquestions,
                    'homework'=>$request->homework,
                    'homework_id'=>$request->homework_id,
                    'point_score'=>$request->point_score,
                    'point_one'=>$request->point_one,
                    'question_count'=>$request->question_count,
                    'time_for_passing'=>$request->time_for_passing,
                    'time_for_passing_one'=>$request->time_for_passing_one,
                    'time_between_passing'=>$request->time_between_passing,
                    'time_between_passing_one'=>$request->time_between_passing_one,
                    'max_count_passing'=>$request->max_count_passing,
                    'exam_question_count'=>$request->exam_question_count,
                ],
            ];
        foreach (config()->get('app.locales') as $key => $language) {
            $array[$language] = [
                'name'=>$request->{'name-'.$language},
                'messages'=>(object)[
                    'welcome'  =>$request->{'welcome-'.$language},
                    'success'  =>$request->{'success-'.$language},
                    'failure'  =>$request->{'failure-'.$language},
                    'already'  =>$request->{'already-'.$language},
                    'exhausted'=>$request->{'exhausted-'.$language},
                    'wait'     =>$request->{'wait-'.$language},
                ],
            ];
        }

        $model = Module::create($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return response([
            'message' => __('Element created successfully'),
            'formAction' => route('modules.update', $model->id),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Module::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.modules.show', [
                'model'=>$model,
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Module::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.modules.form', [
                'model'=>$model,
                'courses'=>Course::pluckTranslate(1),
                'pages'=>Page::pluckTranslate(1),
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreModuleRequest $request, $id)
    {
        $model = Module::findOrFail($id);
        $array = [
                'key'=>$request->key,
                'course_id'=>$request->course_id,
                'data'=>(object)[
                    'introduction_id'=>$request->introduction_id,
                    'conclusion_id'=>$request->conclusion_id,
                    'literature_id'=>$request->literature_id,
                    'webquestions'=>$request->webquestions,
                    'required_webquestions'=>$request->required_webquestions,
                    'homework'=>$request->homework,
                    'homework_id'=>$request->homework_id,
                    'point_score'=>$request->point_score,
                    'point_one'=>$request->point_one,
                    'question_count'=>$request->question_count,
                    'time_for_passing'=>$request->time_for_passing,
                    'time_for_passing_one'=>$request->time_for_passing_one,
                    'time_between_passing'=>$request->time_between_passing,
                    'time_between_passing_one'=>$request->time_between_passing_one,
                    'max_count_passing'=>$request->max_count_passing,
                    'exam_question_count'=>$request->exam_question_count,
                ],
            ];
        foreach (config()->get('app.locales') as $key => $language) {
            $array[$language] = [
                'name'=>$request->{'name-'.$language},
                'messages'=>(object)[
                    'welcome'  =>$request->{'welcome-'.$language},
                    'success'  =>$request->{'success-'.$language},
                    'failure'  =>$request->{'failure-'.$language},
                    'already'  =>$request->{'already-'.$language},
                    'exhausted'=>$request->{'exhausted-'.$language},
                    'wait'     =>$request->{'wait-'.$language},
                ],
            ];
        }

        $model->update($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return response([
            'message' => __('Element updated successfully'),
            'formAction' => route('modules.update', $model->id),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Module::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        $model->delete();
        echo json_encode(true);
        die();
    }

    # функция для получения аяксом данных для datatable
    public function datatable(Request $request)
    {
        $where = [];
        if ($request->course_id){
            $where = [['course_id','=',$request->course_id]];
        }
        $columns = [
            0 => 'id',
        ];

        $totalData = Module::where($where)->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $temp = Module::where($where)->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $temp = Module::where($where)->where([["id","=","{$search}","or"],['key','like',"%{$search}%","or"]])
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $temp = Module::where($where)->where([["id","=","{$search}","or"],['key','like',"%{$search}%","or"]]);
                $totalFiltered = $temp->count();
        }
        
        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['id'] = $model->id;
                $nestedData['name'] = Option::getoption('keyOrName') ? $model->name : $model->key;
                $nestedData['themes'] = $model->themes()->count();
                $nestedData['activethemes'] = $model->activeThemes()->count();
                $temp = '';
                foreach ($model->groups as $key => $group) {
                    $temp .= $group->name . ' ('. __('from') . ' ' . Carbon::parse($group->pivot->date_start)->format('d.m.Y') . ' ' . __('to') . ' ' . Carbon::parse($group->pivot->date_finish)->format('d.m.Y') . ')<br>';
                }
                $nestedData['runs'] = $temp;
                $nestedData['actions'] = '<div class="list-icons">';
                    $nestedData['actions'] .= '<a href="'.route('modules.edit', $model->id).'" class="list-icons-item" title="'.__('Edit').'"><i class="icon-pencil7"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('modules.show', $model->id).'" class="list-icons-item" title="'.__('Preview').'"><i class="icon-eye"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('modules.destroy', $model->id).'" class="list-icons-item text-danger-600 btn-ajax-destroy" title="'.__('Delete').'" data-toggle="modal" data-target="#modal-ajax-destroy"><i class="icon-trash"></i></a>';
                $nestedData['actions'] .= '</div>';
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        echo json_encode($json_data);
    }

}
