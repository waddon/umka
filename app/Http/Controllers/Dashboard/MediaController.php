<?php

namespace App\Http\Controllers\Dashboard;

use App\Traits\BreadcrumbTrait;

use Illuminate\Http\Request;
use App\Http\Requests\StoreMediaRequest;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Storage;
use App\Media;

class MediaController extends Controller
{

    use BreadcrumbTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        \App\Track::message();
        return view('dashboard.medias.index', [
            'types' => Media::select('type')->distinct()->get()->toArray(),
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $model = new Media;
        // return view('dashboard.medias.form', [
        //         'model'=>$model,
        //         'data' => self::getBreadcrumb(),
        //     ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMediaRequest $request)
    {
        // $array = [
        //         'name'=>$request->name,
        //         'description'=>$request->description,
        //     ];
        // $model = Media::create($array);

        // return response([
        //     'message' => __('Element created successfully'),
        //     'formAction' => route('medias.update', $model->id),
        // ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreMediaRequest $request, $id)
    {
        $model = Media::findOrFail($id);
        $array = [
                'name'=>$request->name,
                'description'=>$request->description,
            ];
        $model->update($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return response([
            'message' => __('Element updated successfully'),
            'formAction' => route('medias.update', $model->id),
            'request' => $request->all()
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Media::findOrFail($id);
        if (Storage::disk('public')->exists($model->url)) Storage::disk('public')->delete($model->url);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        $model->delete();
        echo json_encode(true);
        die();
    }

    # функция для получения аяксом данных для datatable
    public function datatable(Request $request)
    {
        $where = [];
        if ($request->type){
            $where = [['type','=',$request->type]];
        }
        $columns = [
            0 => 'id',
            1 => 'name',
            2 => 'description',
            3 => 'created_at',
            4 => 'size',
        ];

        $totalData = Media::where($where)->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $temp = Media::where($where)->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $temp = Media::where($where)->where([['name','like',"%{$search}%",'or'],['description','like',"%{$search}%",'or']])
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $temp = Media::where($where)->where([['name','like',"%{$search}%",'or'],['description','like',"%{$search}%",'or']]);
                $totalFiltered = $temp->count();
        }
        
        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['preview'] = '<a href="/storage/'.$model->url.'" data-popup="lightbox"><img src="/storage/'.$model->url.'" alt="" class="img-preview rounded"></a>';
                $nestedData['name'] = $model->name;
                //$nestedData['description'] = '';
                $nestedData['created'] = $model->created_at->format('d.m.Y');
                $nestedData['fileinfo'] = '<strong>'.__('Size').':</strong> ' . number_format($model->size/1024, 1, '.',' ') . ' Kb<br><strong>'.__('Type').': </strong>' . $model->type . '<br><strong>'.__('Format').': </strong>' . substr(strstr($model->mimetype, '/'),1) . '<br><strong>'.__('Url').': </strong> '.'/storage/'.$model->url . '<br><strong>'.__('Description').': </strong>'.$model->description;
                $nestedData['actions'] = '<div class="list-icons">';
                    $nestedData['actions'] .= '<a href="'.route('medias.update', $model->id).'" class="list-icons-item btn-ajax-edit" title="'.__('Edit').'" data-toggle="modal" data-target="#modal-ajax-edit" data-name="'.$model->name.'" data-description="'.$model->description.'"><i class="icon-pencil7"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('medias.destroy', $model->id).'" class="list-icons-item text-danger-600 btn-ajax-destroy" title="'.__('Delete').'" data-toggle="modal" data-target="#modal-ajax-destroy"><i class="icon-trash"></i></a>';
                $nestedData['actions'] .= '</div>';
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        echo json_encode($json_data);
    }


    public function upload(Request $request)
    {
        $count = 0;
        foreach ($request->file() as $file) {
            $mimetype = $file->getMimeType();
            $type = explode( '/', $mimetype )[0];
            $path = Storage::disk('public')->putFile($type, $file);
            $media = Media::create([
                        'name'=>$file->getClientOriginalName(),
                        'description'=>'description',
                        'size'=>$file->getSize(),
                        'type'=>$type,
                        'mimetype'=>$mimetype,
                        'url'=>$path,
                    ]);
            $count++;
        \App\Track::message(['id'=>$media->id, 'name'=>$media->name]);
        }
        echo json_encode(['result'=>'ok', 'count'=>$count]);
    }

}
