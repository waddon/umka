<?php

namespace App\Http\Controllers\Dashboard;

use App\Traits\BreadcrumbTrait;

use Illuminate\Http\Request;
use App\Http\Requests\StoreLessonRequest;
use Illuminate\Support\Facades\Auth;
use App\Option;

use App\Http\Controllers\Controller;
use App\Course;
use App\Module;
use App\Lesson;
use App\Page;
use Carbon\Carbon;
use App;

class LessonController extends Controller
{

    use BreadcrumbTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {        
        \App\Track::message();
        return view('dashboard.lessons.index', [
            'modules' => Module::pluckTranslate(),
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Lesson;
        \App\Track::message();
        return view('dashboard.lessons.form', [
                'model'=>$model,
                'modules'=>Module::pluckTranslate(1),
                'pages'=>Page::pluckTranslate(1),
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLessonRequest $request)
    {
        $array = [
                'key'=>$request->key,
                'module_id'=>$request->module_id,
                'data'=>(object)[
                    'introduction_id'=>$request->introduction_id,
                    'conclusion_id'=>$request->conclusion_id,
                ],
            ];
        foreach (config()->get('app.locales') as $key => $language) {
            $array[$language] = [
                'name'=>$request->{'name-'.$language},
            ];
        }

        $model = Lesson::create($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return response([
            'message' => __('Element created successfully'),
            'formAction' => route('lessons.update', $model->id),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Lesson::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.lessons.form', [
                'model'=>$model,
                'modules'=>Module::pluckTranslate(1),
                'pages'=>Page::pluckTranslate(1),
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreLessonRequest $request, $id)
    {
        $model = Lesson::findOrFail($id);
        $array = [
                'key'=>$request->key,
                'module_id'=>$request->module_id,
                'data'=>(object)[
                    'introduction_id'=>$request->introduction_id,
                    'conclusion_id'=>$request->conclusion_id,
                ],
            ];
        foreach (config()->get('app.locales') as $key => $language) {
            $array[$language] = [
                'name'=>$request->{'name-'.$language},
            ];
        }

        $model->update($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return response([
            'message' => __('Element updated successfully'),
            'formAction' => route('lessons.update', $model->id),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Lesson::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        $model->delete();
        echo json_encode(true);
        die();
    }

    # функция для получения аяксом данных для datatable
    public function datatable(Request $request)
    {
        $where = [];
        if ($request->module_id){
            $where = [['module_id','=',$request->module_id]];
        }
        $columns = [
            0 => 'id',
        ];

        $totalData = Lesson::where($where)->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $temp = Lesson::where($where)->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $temp = Lesson::where($where)->where([["id","=","{$search}","or"],['key','like',"%{$search}%","or"]])
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $temp = Lesson::where($where)->where([["id","=","{$search}","or"],['key','like',"%{$search}%","or"]]);
                $totalFiltered = $temp->count();
        }
        
        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['id'] = $model->id;
                $nestedData['name'] = Option::getoption('keyOrName') ? $model->translate(App::getLocale())->name : $model->key;
                $nestedData['module'] = $model->module->name;
                $nestedData['themes'] = $model->themes->count();
                $nestedData['activethemes'] = $model->themes->where('activity',true)->count();
                $nestedData['actions'] = '<div class="list-icons">';
                    $nestedData['actions'] .= '<a href="'.route('lessons.edit', $model->id).'" class="list-icons-item" title="'.__('Edit').'"><i class="icon-pencil7"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('lessons.destroy', $model->id).'" class="list-icons-item text-danger-600 btn-ajax-destroy" title="'.__('Delete').'" data-toggle="modal" data-target="#modal-ajax-destroy"><i class="icon-trash"></i></a>';
                $nestedData['actions'] .= '</div>';
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        echo json_encode($json_data);
    }

}
