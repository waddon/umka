<?php

namespace App\Http\Controllers\Dashboard;

use App\Traits\BreadcrumbTrait;

use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;

use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;

use App\User;
use App\Group;
use App\Message;
use App\Option;
use App\Quiz;
use Auth;
use App\Module;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;

//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

//Enables us to output flash messaging
use Session;
use Carbon\Carbon;
use Storage;

class UserController extends Controller {

    use BreadcrumbTrait;

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request) {
        \App\Track::message();
        return view('dashboard.users.index', [
            'user_id'=>$request->user,
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() {
    //Get all roles and pass it to the view
        $roles = Role::get();
        $groups = Group::get();
        $categories = [
            "1"=>__('Political Persons'),
            "2"=>__('Deputies of local councils'),
            "3"=>__('Heads of local government'),
            "4"=>__('Law enforcement officers'),
            "5"=>__('Persons who graduated from a university and are not public servants'),
            "6"=>__('Others'),
        ];
        \App\Track::message();
        return view('dashboard.users.create', [
                'roles'=>$roles,
                'groups'=>$groups,
                'categories'=>$categories,
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreUserRequest $request) {

        $credentials = $request->only('pin','name', 'passport', 'category_id', 'position', 'email', 'phone');
        $credentials['birthday'] = new Carbon($request->birthday);
        $credentials['password'] = bcrypt($request->password);
        $user = User::create($credentials);

        $roles = $request['roles']; //Retrieving the roles field checking if a role was selected
        if (isset($roles)) {

            foreach ($roles as $role) {
            $role_r = Role::where('id', '=', $role)->firstOrFail();            
            $user->assignRole($role_r); //Assigning role to user
            }
        }
        $user->groups()->sync($request->groups);
        foreach ($user->groups as $key => $group) {
            $group->openThemesForUsers($user->id);
        }
    //Redirect to the users.index view and display message
        \App\Track::message(['id'=>$user->id, 'name'=>$user->name]);
        return redirect()->route('users.index')
            ->with('flash_message', __('Element created successfully'));
    }



    public function show(Request $request, $id)
    {
        $model = User::findOrFail($id);
        $categories = [
            "1"=>__('Political Persons'),
            "2"=>__('Deputies of local councils'),
            "3"=>__('Heads of local government'),
            "4"=>__('Law enforcement officers'),
            "5"=>__('Persons who graduated from a university and are not public servants'),
            "6"=>__('Others'),
        ];
        \App\Track::message();
        return view('dashboard.users.show', [
                'user' => $model,
                'modules' => Module::get(),
                'categories'=>$categories,
                'data' => self::getBreadcrumb(),
            ]);
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {
        $categories = [
            "1"=>__('Political Persons'),
            "2"=>__('Deputies of local councils'),
            "3"=>__('Heads of local government'),
            "4"=>__('Law enforcement officers'),
            "5"=>__('Persons who graduated from a university and are not public servants'),
            "6"=>__('Others'),
        ];
        $user = User::findOrFail($id); //Get user with specified id
        $roles = Role::get(); //Get all roles
        $groups = Group::get();
            \App\Track::message(['id'=>$user->id, 'name'=>$user->name]);
        return view('dashboard.users.edit', [
                'user'=>$user,
                'roles'=>$roles,
                'groups'=>$groups,
                'categories'=>$categories,
                'data' => self::getBreadcrumb(),
            ]); //pass user and roles data to view

    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateUserRequest $request, $id)
    {
        $user = User::findOrFail($id); //Get role specified by id

        $credentials = $request->only('pin','name', 'passport', 'category_id', 'position', 'email', 'phone');
        $credentials['birthday'] = new Carbon($request->birthday);
        $user->update($credentials);

        $roles = $request['roles']; //Retreive all roles

        if (isset($roles)) {        
            $user->roles()->sync($roles);  //If one or more role is selected associate user to roles          
        }        
        else {
            $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
        }
        $user->groups()->sync($request->groups);
        foreach ($user->groups as $key => $group) {
            $group->openThemesForUsers($user->id);
        }
            \App\Track::message(['id'=>$user->id, 'name'=>$user->name]);
        return redirect()->route('users.index')
            ->with('flash_message', __('Element updated successfully'));
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request, $id) {
        $model = User::findOrFail($id);
            \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        $model->delete();
        echo json_encode(true);
        die();
    }


    # функция для получения аяксом данных для datatable
    public function datatable(Request $request)
    {
        $where = [];
        if ($request->user_id){
            $where[] = ['id','=',$request->user_id];
        }
        if ($request->activity && $request->activity==1){
            $where[] = ['activity','=',0];
        }
        if ($request->activity && $request->activity==2){
            $where[] = ['activity','=',1];
        }
        if ($request->newletter && $request->newletter==1){
            $where[] = ['new_letter','=',0];
        }
        if ($request->newletter && $request->newletter==2){
            $where[] = ['new_letter','=',1];
        }
        $columns = [
            0 => 'id',
            1 => 'avatar',
            2 => 'name',
            3 => 'email',
            4 => 'roles',
            5 => 'activity',
            6 => 'created_at',
        ];

        $temp = User::where($where);
            if (isset($request->role_id) && $request->role_id) {
                $temp->whereHas('roles', function(Builder $query) use ($request){
                    $query->where('id', '=', $request->role_id);
                });
            }
            $totalData = $temp->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $request->input('order.0.column')!==null ? $columns[$request->input('order.0.column')] : $columns[0];
        $dir = $request->input('order.0.dir')!==null ? $request->input('order.0.dir') : 'ASC';

        if(empty($request->input('search.value'))){
            $temp = User::where($where)->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                if (isset($request->role_id) && $request->role_id) {
                    $temp->whereHas('roles', function(Builder $query) use ($request){
                        $query->where('id', '=', $request->role_id);
                    });
                }
                $models = $temp->get();
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $temp = User::where($where)->where([['name','like',"%{$search}%",'or'],['email','like',"%{$search}%",'or']])
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                if (isset($request->role_id) && $request->role_id) {
                    $temp->whereHas('roles', function(Builder $query) use ($request){
                        $query->where('id', '=', $request->role_id);
                    });
                }
                $models = $temp->get();
            $temp = User::where($where)->where([['name','like',"%{$search}%",'or'],['email','like',"%{$search}%",'or']]);
                if (isset($request->role_id) && $request->role_id) {
                    $temp->whereHas('roles', function(Builder $query) use ($request){
                        $query->where('id', '=', $request->role_id);
                    });
                }
                $totalFiltered = $temp->count();
        }
        
        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['id'] = $model->id;
                $nestedData['avatar'] = ($model->avatar) ? '<img src="/storage/'.$model->avatar.'" class="rounded-circle avatar" alt="">' : '<img src="/assets/img/placeholders/user2.png" class="rounded-circle avatar" alt="">';
                $nestedData['name'] = $model->name;
                $nestedData['email'] = $model->email;
                    $roles = [];
                    foreach ($model->roles as $key => $role) {
                        $roles[] = __($role->name);
                    }
                $nestedData['roles'] = implode(', ', $roles);
                $nestedData['newletter'] = $model->new_letter ? '<i class="icon-envelop2 icon-2x text-success-600"></i>' : '<i class="icon-envelop2 icon-2x" style="color:#eee;"></i>';
                $nestedData['activity'] = $model->activity ? '<i class="icon-unlocked2 icon-2x text-success-600"></i>' : '<i class="text-danger-600 icon-lock5 icon-2x"></i>';
                $nestedData['created'] = $model->created_at->format('d.m.Y');
                $nestedData['actions'] = '<div class="list-icons">';
                    $nestedData['actions'] .= '<a href="'.route('users.edit', $model->id).'" class="list-icons-item" title="'.__('Edit').'"><i class="icon-pencil7"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('users.show', $model->id).'" class="list-icons-item" title="'.__('Show').'"><i class="icon-eye"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('users.setactivity', $model->id).'" class="list-icons-item ajax-action" title="'.__('Change Activity').'"><i class="icon-switch22"></i></a>';
                    if(auth()->user()->can('admin')){
                        $nestedData['actions'] .= '<a href="'.route('users.removetestsprogress', $model->id).'" class="list-icons-item ajax-action" title="'.__('Remove Tests Progress').'"><i class="icon-spam"></i></a>';
                    }
                    $nestedData['actions'] .= '<a href="'.route('users.message', $model->id).'" class="list-icons-item" title="'.__('Message List').'"><i class="icon-envelop2"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('users.quiz', $model->id).'" class="list-icons-item" title="'.__('Quizzes').'"><i class="icon-clipboard2"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('users.destroy', $model->id).'" class="list-icons-item text-danger-600 btn-ajax-destroy" title="'.__('Delete').'" data-toggle="modal" data-target="#modal-ajax-destroy"><i class="icon-trash"></i></a>';
                $nestedData['actions'] .= '</div>';
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        return json_encode($json_data);
    }

    public function setactivity(Request $request, $id=0)
    {
        $model = User::findOrFail($id);
        $model->activity = $model->activity == false;
        # убираем все сделанные попытки открытых модулей
            if ($model->activity){
                $data = $model->data;
                foreach (Module::get() as $key => $module) {
                    if (isset($data->modules->{$module->id}->passing_count) && !isset($data->modules->{$module->id}->passed) ) {
                        $data->modules->{$module->id}->passing_count = 0;
                    }
                }
                $model->data = $data;
            }
        # окончание
        $model->save();
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        echo json_encode(true);
    }

    public function message(Request $request, $id=0)
    {
        $model = User::findOrFail($id);
        if ($request->isMethod('post')) {
            $array = [
                'sender_id' => 0,
                'receiver_id' => $model->id,
                'content' => $request->content ? $request->content : '...',
            ];
            Message::create($array);

            if (Option::getOption('mailMessage')){
                $to = $model->email;
                $data = [
                    'subject' => __('Message recieved'),
                ];
                Mail::to( $to )->send(new OrderShipped($data));
            }

            \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
            return redirect()->route('users.message',$model->id);
        }

        $model->new_letter = false;
        $model->save();
        return view('dashboard.users.message', [
                'model' => $model,
                'data' => self::getBreadcrumb(),
            ]);
    }

    public function download($id)
    {
        $user = User::findOrFail($id);
        if ($user->scan){
            return Storage::download($user->scan);
        } else {
            return;
        }
    }

    public function quiz(Request $request, $id)
    {
        $model = User::findOrFail($id);
        $quizzes = Quiz::getData();
        return view('dashboard.users.quiz', [
                'model' => $model,
                'quizzes' => $quizzes,
                'data' => self::getBreadcrumb(),
            ]);
    }


    public function removeTestsProgress(Request $request, $id=0)
    {
        $model = User::findOrFail($id);
        $data = $model->data;
        foreach (Module::get() as $key => $module) {
            if (isset($data->modules->{$module->id}->passed)){
                unset($data->modules->{$module->id}->passed);
            }
        }
        $model->data = $data;
        $model->save();
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return json_encode(true);
    }

}
