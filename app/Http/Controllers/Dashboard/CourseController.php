<?php

namespace App\Http\Controllers\Dashboard;

use App\Traits\BreadcrumbTrait;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCourseRequest;
use Illuminate\Support\Facades\Auth;
use App\Option;

use App\Http\Controllers\Controller;
use App\Course;
use Carbon\Carbon;
use App;

class CourseController extends Controller
{

    use BreadcrumbTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        \App\Track::message();
        return view('dashboard.courses.index', [
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Course;
        \App\Track::message();
        return view('dashboard.courses.form', [
                'model'=>$model,
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCourseRequest $request)
    {
        $array = [
                'key'=>$request->key,
                'data'=>(object)[
                    'point_score'=>$request->point_score,
                    'point_one'=>$request->point_one,
                    'way_get_count_of_question'=>$request->way_get_count_of_question,
                    'question_count'=>$request->question_count,
                    'time_for_passing'=>$request->time_for_passing,
                    'time_for_passing_one'=>$request->time_for_passing_one,
                    'time_between_passing'=>$request->time_between_passing,
                    'time_between_passing_one'=>$request->time_between_passing_one,
                    'max_count_passing'=>$request->max_count_passing,
                    'certificate' => $request->certificate
                ],
            ];
        foreach (config()->get('app.locales') as $key => $language) {
            $array[$language] = [
                'name'=>$request->{'name-'.$language},
                // 'content'=>$request->{'content-'.$language},
                'messages'=>(object)[
                    'welcome'  =>$request->{'welcome-'.$language},
                    'success'  =>$request->{'success-'.$language},
                    'failure'  =>$request->{'failure-'.$language},
                    'already'  =>$request->{'already-'.$language},
                    'exhausted'=>$request->{'exhausted-'.$language},
                    'wait'     =>$request->{'wait-'.$language},
                ],
            ];
        }

        $model = Course::create($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return response([
            'message' => __('Element created successfully'),
            'formAction' => route('courses.update', $model->id),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Course::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.courses.show', [
                'model'=>$model,
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Course::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.courses.form', [
                'model'=>$model,
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCourseRequest $request, $id)
    {
        $model = Course::findOrFail($id);
        $array = [
                'key'=>$request->key,
                'data'=>(object)[
                    'point_score'=>$request->point_score,
                    'point_one'=>$request->point_one,
                    'way_get_count_of_question'=>$request->way_get_count_of_question,
                    'question_count'=>$request->question_count,
                    'time_for_passing'=>$request->time_for_passing,
                    'time_for_passing_one'=>$request->time_for_passing_one,
                    'time_between_passing'=>$request->time_between_passing,
                    'time_between_passing_one'=>$request->time_between_passing_one,
                    'max_count_passing'=>$request->max_count_passing,
                    'certificate' => $request->certificate
                ],
            ];
        foreach (config()->get('app.locales') as $key => $language) {
            $array[$language] = [
                'name'=>$request->{'name-'.$language},
                // 'content'=>$request->{'content-'.$language},
                'messages'=>(object)[
                    'welcome'  =>$request->{'welcome-'.$language},
                    'success'  =>$request->{'success-'.$language},
                    'failure'  =>$request->{'failure-'.$language},
                    'already'  =>$request->{'already-'.$language},
                    'exhausted'=>$request->{'exhausted-'.$language},
                    'wait'     =>$request->{'wait-'.$language},
                ],
            ];
        }

        $model->update($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return response([
            'message' => __('Element updated successfully'),
            'formAction' => route('courses.update', $model->id),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Course::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        $model->delete();
        echo json_encode(true);
        die();
    }

    # функция для получения аяксом данных для datatable
    public function datatable(Request $request)
    {
        $where = [];
        $columns = [
            0 => 'id',
        ];

        $totalData = Course::where($where)->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $temp = Course::where($where)->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $temp = Course::where($where)->where([["id","=","{$search}","or"],['key','like',"%{$search}%","or"]])
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $temp = Course::where($where)->where([["id","=","{$search}","or"],['key','like',"%{$search}%","or"]]);
                $totalFiltered = $temp->count();
        }
        
        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['id'] = $model->id;
                $nestedData['name'] = Option::getoption('keyOrName') ? $model->translate(App::getLocale())->name : $model->key;
                $nestedData['modules'] = $model->modules->count();
                $nestedData['actions'] = '<div class="list-icons">';
                    $nestedData['actions'] .= '<a href="'.route('courses.edit', $model->id).'" class="list-icons-item" title="'.__('Edit').'"><i class="icon-pencil7"></i></a>';
                    // $nestedData['actions'] .= '<a href="'.route('courses.show', $model->id).'" class="list-icons-item" title="'.__('Preview').'"><i class="icon-eye"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('courses.destroy', $model->id).'" class="list-icons-item text-danger-600 btn-ajax-destroy" title="'.__('Delete').'" data-toggle="modal" data-target="#modal-ajax-destroy"><i class="icon-trash"></i></a>';
                $nestedData['actions'] .= '</div>';
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        echo json_encode($json_data);
    }

}
