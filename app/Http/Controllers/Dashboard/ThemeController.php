<?php

namespace App\Http\Controllers\Dashboard;

use App\Traits\BreadcrumbTrait;

use Illuminate\Http\Request;
use App\Http\Requests\StoreThemeRequest;
use Illuminate\Support\Facades\Auth;
use App\Option;
use App\Media;

use App\Http\Controllers\Controller;
use App\Module;
use App\Lesson;
use App\Theme;
use App;

class ThemeController extends Controller
{

    use BreadcrumbTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        \App\Track::message();
        return view('dashboard.themes.index', [
            'modules' => Module::pluckTranslate(),
            'lessons' => Lesson::pluckTranslate(),
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        \App\Track::message();
        return view('dashboard.themes.form', [
                'model'=> new Theme,
                'lessons'=> Lesson::pluckTranslate(1),
                'documents'=>Media::where('type','=','application')->pluck('name','id')->prepend(__('Select Application'),'0'),
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreThemeRequest $request)
    {
        $path = $request->offline_id ? Media::find($request->offline_id)->url : '';
        $array = [
                'key'=>$request->key,
                'lesson_id'=>$request->lesson_id,
                'module_id'=>Lesson::findOrFail($request->lesson_id)->module_id,
                'data'=>(object)[
                    'offline'=>$path,
                    'offline_id'=>$request->offline_id,
                ],
            ];
        foreach (config()->get('app.locales') as $key => $language) {
            $array[$language] = [
                'name'=>$request->{'name-'.$language},
                'content'=>$request->{'content-'.$language},
            ];
        }

        $model = Theme::create($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return response([
            'message' => __('Element created successfully'),
            'formAction' => route('themes.update', $model->id),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Theme::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.themes.show', [
                'model'=>$model,
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Theme::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.themes.form', [
                'model'=>$model,
                'lessons'=>Lesson::pluckTranslate(1),
                'documents'=>Media::where('type','=','application')->pluck('name','id')->prepend(__('Select Application'),'0'),
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreThemeRequest $request, $id)
    {
        $model = Theme::findOrFail($id);
        $path = $request->offline_id ? Media::find($request->offline_id)->url : '';
        $array = [
                'key'=>$request->key,
                'lesson_id'=>$request->lesson_id,
                'module_id'=>Lesson::findOrFail($request->lesson_id)->module_id,
                'data'=>(object)[
                    'offline'=>$path,
                    'offline_id'=>$request->offline_id,
                ],
            ];
        foreach (config()->get('app.locales') as $key => $language) {
            $array[$language] = [
                'name'=>$request->{'name-'.$language},
                'content'=>$request->{'content-'.$language},
            ];
        }

        $model->update($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return response([
            'message' => __('Element updated successfully'),
            'formAction' => route('themes.update', $model->id),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Theme::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        $model->delete();
        echo json_encode(true);
        die();
    }

    # функция для получения аяксом данных для datatable
    public function datatable(Request $request)
    {
        $where = [];
        $where2 = [];
        if ($request->lesson_id){
            $where = [['lesson_id','=',$request->lesson_id]];
        } elseif ($request->module_id){
            $module = Module::findOrFail($request->module_id);
            foreach ($module->lessons as $key => $lesson) {
                $where2[] = ['lesson_id','=',$lesson->id,'or'];
            }
        }
        $columns = [
            0 => 'id',
            1 => 'order',
            2 => 'key',
            3 => 'lesson_id',
            4 => 'activity',
        ];

        $totalData = Theme::where($where)->where($where2)->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $temp = Theme::where($where)->where($where2)->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $temp = Theme::where($where)->where($where2)->where([["id","=","{$search}","or"],['key','like',"%{$search}%","or"]])
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $temp = Theme::where($where)->where($where2)->where([["id","=","{$search}","or"],['key','like',"%{$search}%","or"]]);
                $totalFiltered = $temp->count();
        }
        
        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['id'] = $model->id;
                $nestedData['order'] = $model->order;
                $nestedData['name'] = Option::getoption('keyOrName') ? $model->translate(App::getLocale())->name : $model->key;
                $nestedData['module'] = $model->module->name . '<br>(' . $model->lesson->name . ')';
                $nestedData['activity'] = $model->activity ? '<i class="icon-eye icon-2x text-success-600"></i>' : '<i class="icon-eye-blocked icon-2x" style="color:#eee;"></i>';
                $nestedData['actions'] = '<div class="list-icons">';
                    $nestedData['actions'] .= '<a href="'.route('themes.edit', $model->id).'" class="list-icons-item" title="'.__('Edit').'"><i class="icon-pencil7"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('themes.moveup', $model->id).'" class="list-icons-item ajax-action" title="'.__('Move Up').'"><i class="icon-arrow-up7"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('themes.movedown', $model->id).'" class="list-icons-item ajax-action" title="'.__('Move Down').'"><i class="icon-arrow-down7"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('themes.setactivity', $model->id).'" class="list-icons-item ajax-action" title="'.__('Change Activity').'"><i class="icon-switch22"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('themes.show', $model->id).'" class="list-icons-item" title="'.__('Preview').'"><i class="icon-eye"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('themes.destroy', $model->id).'" class="list-icons-item text-danger-600 btn-ajax-destroy" title="'.__('Delete').'" data-toggle="modal" data-target="#modal-ajax-destroy"><i class="icon-trash"></i></a>';
                $nestedData['actions'] .= '</div>';
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        echo json_encode($json_data);
    }


    public function moveup(Request $request, $id=0)
    {
        $model = Theme::findOrFail($id);
        if ($model->order >1){
            $old_order = $model->order;
            $new_order = $model->order - 1;
            $element = Theme::where(
                [
                    ['module_id','=',$model->module_id],
                    ['order','=',$new_order]
                ])
            ->first();
            if($element){
                $element->order = $old_order;
                $element->save();
            }
            $model->order = $new_order;
            $model->save();
            \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        }
        echo json_encode(true);
    }

    public function movedown(Request $request, $id=0)
    {
        $model = Theme::findOrFail($id);
        $old_order = $model->order;
        $count = Theme::where(
            [
                ['module_id','=',$model->module_id],
                ['order','>',$old_order]
            ])->count();
        if ($count >0){
            $old_order = $model->order;
            $new_order = $model->order + 1;
            $element = Theme::where(
                [
                    ['module_id','=',$model->module_id],
                    ['order','=',$new_order]
                ])
            ->first();
            if($element){
                $element->order = $old_order;
                $element->save();
            }
            $model->order = $new_order;
            $model->save();
            \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        }
        echo json_encode(true);
    }

    public function setactivity(Request $request, $id=0)
    {
        $model = Theme::findOrFail($id);
        $model->activity = $model->activity == false;
        $model->save();
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        echo json_encode(true);
    }
}
