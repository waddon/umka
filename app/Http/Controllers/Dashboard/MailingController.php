<?php

namespace App\Http\Controllers\Dashboard;

use App\Traits\BreadcrumbTrait;

use Illuminate\Http\Request;
use App\Http\Requests\StoreMailingRequest;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Group;
use App\Mailing;

class MailingController extends Controller
{

    use BreadcrumbTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {        
        \App\Track::message();
        return view('dashboard.mailings.index', [
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Mailing;
        \App\Track::message();
        return view('dashboard.mailings.form', [
                'model'=>$model,
                'groups'=>Group::pluck('name','id')->prepend(__('Select Group'),0),
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMailingRequest $request)
    {
        $array = [
                'name'=>$request->name,
                'group_id'=>$request->group_id,
                'date_send'=>$request->date_send,
                'sent'=>false,
                'content'=>$request->content ? $request->content : '',
            ];
        $model = Mailing::create($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return response([
            'message' => __('Element created successfully'),
            'formAction' => route('mailings.update', $model->id),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Mailing::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.mailings.show', [
                'model'=>$model,
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Mailing::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.mailings.form', [
                'model'=>$model,
                'groups'=>Group::pluck('name','id')->prepend(__('Select Group'),0),
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreMailingRequest $request, $id)
    {
        $model = Mailing::findOrFail($id);
        $array = [
                'name'=>$request->name,
                'group_id'=>$request->group_id,
                'date_send'=>$request->date_send,
                'sent'=>false,
                'content'=>$request->content ? $request->content : '',
            ];
        $model->update($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return response([
            'message' => __('Element updated successfully'),
            'formAction' => route('mailings.update', $model->id),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Mailing::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        $model->delete();
        echo json_encode(true);
        die();
    }

    # функция для получения аяксом данных для datatable
    public function datatable(Request $request)
    {
        $where = [];
        $columns = [
            0 => 'id',
            1 => 'name',
        ];

        $totalData = Mailing::where($where)->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $temp = Mailing::where($where)->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $temp = Mailing::where($where)->where([['id','=',"{$search}",'or'],['name','like',"%{$search}%",'or']])
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $temp = Mailing::where($where)->where([['id','=',"{$search}",'or'],['name','like',"%{$search}%",'or']]);
                $totalFiltered = $temp->count();
        }
        
        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['id'] = $model->id;
                $nestedData['name'] = $model->name;
                $nestedData['group'] = $model->group->name;
                $nestedData['date_send'] = $model->date_send->format('d.m.Y');
                $nestedData['sent'] = $model->sent ? '<i class="icon-checkmark icon-2x text-success-600"></i>' : '<i class="icon-checkmark icon-2x" style="color:#eee;"></i>';
                $nestedData['actions'] = '<div class="list-icons">';
                    $nestedData['actions'] .= '<a href="'.route('mailings.edit', $model->id).'" class="list-icons-item" title="'.__('Edit').'"><i class="icon-pencil7"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('mailings.show', $model->id).'" class="list-icons-item" title="'.__('Show').'"><i class="icon-eye"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('mailings.setactivity', $model->id).'" class="list-icons-item ajax-action" title="'.__('Send Right Now').'"><i class="icon-paperplane"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('mailings.destroy', $model->id).'" class="list-icons-item text-danger-600 btn-ajax-destroy" title="'.__('Delete').'" data-toggle="modal" data-target="#modal-ajax-destroy"><i class="icon-trash"></i></a>';
                $nestedData['actions'] .= '</div>';
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        echo json_encode($json_data);
    }


    public function setactivity(Request $request, $id=0)
    {
        $model = Mailing::findOrFail($id);
        $model->send();
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        echo json_encode(true);
    }
}
