<?php

namespace App\Http\Controllers\Dashboard;

use App\Traits\BreadcrumbTrait;

use Illuminate\Http\Request;
use App\Http\Requests\StoreGroupRequest;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use App\User;
use App\Group;
use App\Module;
use App\Course;
use App\Quiz;
use Carbon\Carbon;

class GroupController extends Controller
{

    use BreadcrumbTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {        
        \App\Track::message();
        return view('dashboard.groups.index', [
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \App\Track::message();
        return view('dashboard.groups.create', [
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGroupRequest $request)
    {
        $array = [
                'name'=>$request->name,
            ];
        $model = Group::create($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return redirect()->route('groups.index')
            ->with('flash_message', __('Element created successfully'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Group::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.groups.show', [
            'model' => $model,
            'modules' => Module::get(),
            'quizzes' => Quiz::get(),
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Group::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.groups.edit', [
            'model' => $model,
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreGroupRequest $request, $id)
    {
        $model = Group::findOrFail($id);
        $array = [
                'name'=>$request->name,
            ];
        $model->update($array);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return redirect()->route('groups.index')
            ->with('flash_message', __('Element updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Group::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        $model->delete();
        echo json_encode(true);
        die();
    }

    # функция для получения аяксом данных для datatable
    public function datatable(Request $request)
    {
        $where = [];
        $columns = [
            0 => 'id',
        ];

        $totalData = Group::where($where)->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $temp = Group::where($where)->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $temp = Group::where($where)->where('name','like',"%{$search}%")
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $temp = Group::where($where)->where('name','like',"%{$search}%");
                $totalFiltered = $temp->count();
        }
        
        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['id'] = $model->id;
                $nestedData['name'] = '<a href="'.route('groups.show', $model->id).'" class="list-icons-item" title="'.__('Show').'">'.__($model->name).'</a>';
                $nestedData['students'] = $model->users->count();
                $temp = '';
                foreach ($model->modules as $key => $module) {
                    $temp .= $module->name . ' ('. __('from') . ' ' . Carbon::parse($module->pivot->date_start)->format('d.m.Y') . ' ' . __('to') . ' ' . Carbon::parse($module->pivot->date_finish)->format('d.m.Y') . ')<br>';
                }
                $nestedData['runs'] = $temp;
                $nestedData['actions'] = '<div class="list-icons">';
                    $nestedData['actions'] .= '<a href="'.route('groups.edit', $model->id).'" class="list-icons-item" title="'.__('Edit').'"><i class="icon-pencil7"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('groups.show', $model->id).'" class="list-icons-item" title="'.__('Show').'"><i class="icon-eye"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('groups.stat', $model->id).'" class="list-icons-item" title="'.__('Stat').'"><i class="icon-stats-bars2"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('groups.webquestions', $model->id).'" class="list-icons-item" title="'.__('Webquestions').'"><i class="icon-headset"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('groups.destroy', $model->id).'" class="list-icons-item text-danger-600 btn-ajax-destroy" title="'.__('Delete').'" data-toggle="modal" data-target="#modal-ajax-destroy"><i class="icon-trash"></i></a>';
                $nestedData['actions'] .= '</div>';
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        echo json_encode($json_data);
    }

    public function detail(Request $request)
    {
        $where = [];
        $columns = [
            0 => 'id',
            1 => 'avatar',
            2 => 'name',
            3 => 'activity',
        ];
        $temp = User::where($where);
            if (isset($request->group_id) && $request->group_id) {
                $temp->whereHas('groups', function(Builder $query) use ($request){
                    $query->where('id', '=', $request->group_id);
                });
            }
            $totalData = $temp->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $temp = User::where($where)->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                if (isset($request->group_id) && $request->group_id) {
                    $temp->whereHas('groups', function(Builder $query) use ($request){
                        $query->where('id', '=', $request->group_id);
                    });
                }
                $models = $temp->get();
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $temp = User::where($where)->where([['name','like',"%{$search}%",'or'],['email','like',"%{$search}%",'or']])
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                if (isset($request->group_id) && $request->group_id) {
                    $temp->whereHas('groups', function(Builder $query) use ($request){
                        $query->where('id', '=', $request->group_id);
                    });
                }
                $models = $temp->get();
            $temp = User::where($where)->where([['name','like',"%{$search}%",'or'],['email','like',"%{$search}%",'or']]);
                if (isset($request->group_id) && $request->group_id) {
                    $temp->whereHas('groups', function(Builder $query) use ($request){
                        $query->where('id', '=', $request->group_id);
                    });
                }
                $totalFiltered = $temp->count();
        }
        
        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['id'] = $model->id;
                $nestedData['avatar'] = ($model->avatar) ? '<img src="/storage/'.$model->avatar.'" class="rounded-circle avatar" alt="">' : '<img src="/assets/img/placeholders/user2.png" class="rounded-circle avatar" alt="">';
                $nestedData['name'] = $model->name;
                $nestedData['activity'] = $model->activity ? '<i class="icon-unlocked2 icon-2x text-success-600"></i>' : '<i class="text-danger-600 icon-lock5 icon-2x"></i>';
                    if (isset($request->module_id) && $request->module_id){
                        $color = isset($model->data->modules->{$request->module_id}->passed) ? 'bg-success' : 'bg-warning';
                        $percent = $model->openThemesPercent($request->module_id);
                    } else {
                        $color = isset($model->data->courses->{$request->course_id}->passed) ? 'bg-success' : 'bg-warning';
                        $percent = $model->openAllThemesPercent($request->course_id);
                    }
               $nestedData['progress'] = '<div class="progress"><div class="progress-bar '.$color.'" style="width: '.$percent.'%"><span>'.$percent.'%</span></div></div>';
                $nestedData['actions'] = '<div class="list-icons">';
                    $nestedData['actions'] .= '<a href="'.route('users.show', $model->id).'" class="list-icons-item" title="'.__('Show').'"><i class="icon-eye"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('users.setactivity', $model->id).'" class="list-icons-item ajax-action" title="'.__('Change Activity').'"><i class="icon-switch22"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('users.message', $model->id).'" class="list-icons-item" title="'.__('Message').'"><i class="icon-envelop2"></i></a>';
                $nestedData['actions'] .= '</div>';
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        echo json_encode($json_data);
    }

    public function stat($id)
    {
        $model = Group::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.groups.stat', [
            'model' => $model,
            'modules' => Module::get(),
            'courses' => Course::get(),
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
     * Включение, выключение опроса для определнной групы
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function setActivity(Request $request, $id)
    {
        $result = false;
        $model = Group::findOrFail($id);
        $quiz_id = isset($request->quiz_id) ? $request->quiz_id : 0;
        $quiz = Quiz::findOrFail($quiz_id);
        $array = [];
        foreach ($model->quizzes as $key => $quiz) {
            $array[] = $quiz->id;
        }
        if (!in_array( $quiz_id, $array )) {
            $array[] = $quiz_id;
            $result = true;
        } else {
            $this->del_from_array($quiz_id, $array, false);
        }
        $model->quizzes()->sync($array);
        return json_encode($result);
    }

    /**
     * Удаление элементов массива по значению
     *
     * @param  str  $needle
     * @param  array  $array
     * @param  bool  $all
     * @return void
     */
    function del_from_array($needle, &$array, $all = true){
            if(!$all){
                if(FALSE !== $key = array_search($needle,$array)) unset($array[$key]);
                return;
            }
            foreach(array_keys($array,$needle) as $key){
                unset($array[$key]);
            }
            return;
    }

    public function webquestions(Request $request, $id)
    {
        $model = Group::findOrFail($id);
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('dashboard.groups.webquestions', [
            'model' => $model,
            'data' => self::getBreadcrumb(),
        ]);
    }

}
