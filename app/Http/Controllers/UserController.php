<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use Storage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PDF;

use App\User;
use App\Course;
use App\Module;
use App\Theme;
use App\Message;
use App\Page;
use App\Quiz;

class UserController extends Controller
{

    # отображение модуля
    public function moduleshow(Request $request, $id)
    {
        $model = Module::findOrfail($id);
        $this->data->breadcrumbs[] = ["title"=>$model->name, "url"=>route('moduleshow',$model->id)];
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('page',[
            'model' => $model,
            'data' => $this->data,
        ]);
    }

    # отображение темы
    public function themeshow(Request $request, $id)
    {
        $model = Theme::findOrfail($id);
        $this->data->breadcrumbs[] = ["title"=>$model->module->name, "url"=>route('moduleshow',$model->module->id)];
        $this->data->breadcrumbs[] = ["title"=>$model->name, "url"=>route('themeshow',$model->id)];
        \App\Track::message(['id'=>$model->id, 'name'=>$model->name]);
        return view('theme',[
            'model' => $model,
            'data' => $this->data,
        ]);
    }

    public function opennexttheme(Request $request)
    {
        return json_encode([
            'result'=>auth()->user()->openNextTheme($request->theme),
        ]);
    }

    # отображение теста
    public function testshow(Request $request, $id)
    {
        $module = Module::findOrFail($id);
        $this->data->breadcrumbs[] = ["title"=>$module->name, "url"=>route('moduleshow',$module->id)];
        $this->data->breadcrumbs[] = ["title"=>__('Module Test'), "url"=>route('testshow',$module->id)];
        $user = auth()->user();
        # проверяем, пройдены ли все темы
            if ($user->openThemesPercent($id)<100) abort('403');
        # открываем вьюху в зависимости от проверок
            $message = $user->validatePassingTest($id);
            if ($message == 'Ok'){
                \App\Track::message(['id'=>$module->id, 'name'=>$module->name]);
                return view('test',[
                    'module' => $module,
                    'data' => $this->data
                ]);
            } else {
                $this->data->icon = '<i class="icon-warning22 mb-3 icon-2x"></i>';
                $this->data->message = $message;
                return view('message',['data' => $this->data,]);
            }
    }

    public function examshow(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        $this->data->breadcrumbs[] = ["title"=>__('Course Exam'), "url"=>route('examshow',$course->id)];
        $user = auth()->user();
        # проверяем, пройдены ли все темы
            if (!$user->ifOpenExam($id)) abort('403');
        # открываем вьюху в зависимости от проверок
            $message = $user->validatePassingExam($id);
            if ($message == 'Ok'){
                \App\Track::message(['id'=>$course->id, 'name'=>$course->name]);
                return view('exam',[
                    'course' => $course,
                ]);
            } else {
                $this->data->icon = '<i class="icon-warning22 mb-3 icon-2x"></i>';
                $this->data->message = $message;
                return view('message',['data' => $this->data,]);
            }
    }

    public function testcheck(Request $request, $id)
    {
        $this->data->breadcrumbs[] = ["title"=>__('Module Test Check'), "url"=>'#'];
        $user = auth()->user();
        if ($user->openThemesPercent($id)<100) abort('403');
        $message = $user->validatePassingTest($id);
            if ($message == 'Ok'){
                $inputs = [];
                foreach ($request->all() as $key => $input) {
                    if ( $key === (int)$key ){
                        if(!is_array($input) && substr($input,0,1)=='['){
                            $inputs[$key]=json_decode($input);
                        } else {
                            $inputs[$key]=$input;
                        }
                    }
                }

                $module = Module::findOrfail($id);
                $this->data->results = $module->checkAnswers($inputs);
                if ($this->data->results['available']>=$this->data->results['need']){
                    $this->data->icon = '<i class="icon-checkmark mb-3 icon-2x"></i>';
                    $this->data->message = isset($module->messages->success) ? $module->messages->success : __('Success message');
                    \App\Track::message(['message'=>'Успешная сдача теста по модулю <strong>«'.$module->name.'»</strong> (id=«'.$module->id.'»)']);
                    # добавляем кнопку с домашним заданием
                        if (isset($module->data->homework) && $module->data->homework && isset($module->data->homework_id) && $module->data->homework_id) {
                            $this->data->button_url = route('homework',$module->id);
                        }
                } else {
                    $this->data->icon = '<i class="icon-cross2 mb-3 icon-2x"></i>';
                    $this->data->message = isset($module->messages->failure) ? $module->messages->failure : __('Failure message');
                    \App\Track::message(['message'=>'Неудачная попытка сдачи теста по модулю <strong>«'.$module->name.'»</strong> (id=«'.$module->id.'»)']);
                }
                return view('message',['data' => $this->data,]);
            } else {
                $this->data->icon = '<i class="icon-warning22 mb-3 icon-2x"></i>';
                $this->data->message = $message;
                \App\Track::message(['message'=>'Попытка сдачи теста не по правилам']);
                return view('message',['data' => $this->data,]);
            }

    }


    public function profile(Request $request)
    {
        $this->data->breadcrumbs[] = ["title"=>__('Profile'), "url"=>route('profile')];
        if ($request->isMethod('post')) {
            # валидация данных
                $id = auth()->user()->id;
                $rules = [
                    'category_id' => 'required|integer|min:1',
                    'email'=>'required|email|unique:users,email,' . $id,
                    'position' => 'required|string|max:255',
                    'phone' => 'required|string|max:255',
                ];
                if ($request->password){
                    $rules['password'] = 'required|string|min:6|max:255|same:password_confirmation';
                    $rules['password_confirmation'] = 'required|string|max:255|same:password';
                }
                $this->validate($request, $rules);

            # подготовка и сохранение данных
                auth()->user()->category_id = $request->category_id;
                auth()->user()->email = $request->email;
                auth()->user()->position = $request->position;
                auth()->user()->phone = $request->phone;
                if ($request->password){
                    auth()->user()->password = bcrypt($request->password);
                }
                foreach ($request->file() as $file) {
                    Storage::disk('public')->delete(auth()->user()->avatar);
                    $path = Storage::disk('public')->putFile('avatars', $file);
                    auth()->user()->avatar = $path;
                }
                auth()->user()->save();
            \App\Track::message();
            return redirect()->route('profile')
                ->with('flash_message', __('Settings updated successfully'));
        }        

        return view('user.profile',[
            'courses'=>Course::get(),
            'model' => auth()->user(),
            'data' => $this->data,
        ]);
    }

    public function message(Request $request)
    {
        $this->data->breadcrumbs[] = ["title"=>__('Message List'), "url"=>route('message')];
        if ($request->isMethod('post')) {
            $array = [
                'sender_id' => auth()->user()->id,
                'receiver_id' => 0,
                'content' => $request->content ? $request->content : '...',
            ];
            Message::create($array);
            auth()->user()->new_letter = true;
            auth()->user()->save();
            \App\Track::message();
            return redirect()->route('message');
        }

        return view('user.message',[
            'model' => auth()->user(),
            'data' => $this->data,
        ]);
    }

    # Проверка результатов экзамена
    public function examcheck(Request $request, $id)
    {
        $this->data->breadcrumbs[] = ["title"=>__('Course Exam Check'), "url"=>'#'];
        $user = auth()->user();
        # проверяем, пройдены ли все темы
            if (!$user->ifOpenExam($id)) abort('403');

        $message = $user->validatePassingExam($id);
            if ($message == 'Ok'){
                $inputs = [];
                foreach ($request->all() as $key => $input) {
                    if ( $key === (int)$key ){
                        if(!is_array($input) && substr($input,0,1)=='['){
                            $inputs[$key]=json_decode($input);
                        } else {
                            $inputs[$key]=$input;
                        }
                    }
                }

                $course = Course::findOrfail($id);
                $this->data->results = $course->checkAnswers($inputs,$id);
                if ($this->data->results['available']>=$this->data->results['need']){
                    $this->data->icon = '<i class="icon-checkmark mb-3 icon-2x"></i>';
                    $this->data->message = isset($course->messages->success) ? $course->messages->success : __('Success message');
                    \App\Track::message(['message'=>'Успешная сдача экзамена по курсу '.$course->name]);
                } else {
                    $this->data->icon = '<i class="icon-cross2 mb-3 icon-2x"></i>';
                    $this->data->message = isset($course->messages->failure) ? $course->messages->failure : __('Failure message');
                    \App\Track::message(['message'=>'Неудачная попытка сдачи экзамена по курсу '.$course->name]);
                }
                return view('message',['data' => $this->data,]);
            } else {
                $this->data->icon = '<i class="icon-warning22 mb-3 icon-2x"></i>';
                $this->data->message = $message;
                \App\Track::message(['message'=>'Попытка сдачи экзамена не по правилам']);
                return view('message',['data' => $this->data,]);
            }

    }

    # подсчет количества нажатий на ссылки
    public function pushcounter(Request $request)
    {
        $theme_id = isset($request->theme) ? $request->theme : 0;
        $user_id  = isset($request->user)  ? $request->user  : 0;
        $theme = Theme::find($theme_id);
        $user  = User::find($user_id);
        if ($theme && $user){
            $data = $user->data;
            $push_theme  = isset($data->themes->{$theme_id}->push) ? $data->themes->{$theme_id}->push + 1 : 1;
            $push_module = isset($data->modules->{$theme->module->id}->push) ? $data->modules->{$theme->module->id}->push + 1 : 1;
            $data->themes->{$theme_id}->push = $push_theme;
            $data->modules->{$theme->module->id}->push = $push_module;
            $user->data = $data;
            $user->save();
        }
        return json_encode(['result'=>true]);
    }

    # вопросы к вебинару
    public function webquestions(Request $request, $id = 0)
    {
        $this->data->breadcrumbs[] = ["title"=>__('Questions to the Webinar'), "url"=> $id ? route('webquestions',$id) : route('webquestions')];
        if ($request->isMethod('post') && $request->content) {
            $data = auth()->user()->data;
            if (!isset($data->webquestions)) {$data->webquestions = [];}
            $module = Module::find($id);
            $data->webquestions[] = ['text'=>$request->content, 'module'=>$module ? $module->name : '-'];
            auth()->user()->data = $data;
            auth()->user()->save();
            \App\Track::message();
            if ($id) {
                return redirect()->route('webquestions',$id)
                ->with('flash_message', __('Question added successfully'));
            } else {
                return redirect()->route('webquestions')
                ->with('flash_message', __('Question added successfully'));
            }
        }

        return view('user.webquestions',[
            'route' => $id ? route('webquestions',$id) : route('webquestions'),
        ]);
    }

    # Домашнее задание
    public function homework(Request $request, $id = 0)
    {
        $module = Module::find($id);
        if (!isset($module->data->homework) || !$module->data->homework) {abort(403);}
        $this->data->breadcrumbs[] = ["title"=>__('Homework'), "url"=> $id ? route('homework',$id) : route('homework')];
        if ($request->isMethod('post') && $request->username) {
            $data = auth()->user()->data;
            if (!isset($data->homework)) {$data->homework = (object)[];}
            $data->homework->username = $request->username;
            auth()->user()->data = $data;
            auth()->user()->save();
            \App\Track::message();
            if ($id) {
                return redirect()->route('homework',$id)
                ->with('flash_message', __('Information about homework added successfully'));
            } else {
                return redirect()->route('homework')
                ->with('flash_message', __('Information about homework added successfully'));
            }
        }
        $module = Module::find($id);
        $page = Page::find(isset($module->data->homework_id) ? $module->data->homework_id : 0);

        return view('homework',[
            'route' => $id ? route('homework',$id) : route('homework'),
            'content' => $page ? $page->content : '',
        ]);
    }


    public function quizshow(Request $request, $id=0)
    {
        $quiz = Quiz::findOrFail($id);
        if ($request->isMethod('post')){
            $data = auth()->user()->data;
            if (!isset($data->quizzes)) {$data->quizzes = (object)[];}
            $data->quizzes->{$id} = $request->input();
            auth()->user()->data = $data;
            auth()->user()->save();
        }
        $this->data->breadcrumbs[] = ["title"=>__('Quiz'), "url"=>route('quizshow',$quiz->id)];
        return view('quiz',[
            'quiz' => $quiz,
        ]);
    }

    public function pdf($id=0)
    {
        $theme = Theme::findOrFail($id);
        $data = [
            'title' => $theme->key,
            'heading' => $theme->name,
            'content' => str_replace('../../../','',$theme->content),
            ];
        
        $pdf = PDF::loadView('pdf', $data);  
        return $pdf->download('pdf.pdf');

        // return view('pdf', $data);
    }
}
