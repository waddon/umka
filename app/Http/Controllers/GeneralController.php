<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use Storage;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;

use App\User;
use App\Page;
use App\Glossary;
use App\Option;

class GeneralController extends Controller
{
    public function index(Request $request)
    {
        return view('home',[
            'model' => Page::findOrFail(1),
        ]);
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('home');
    }

    # Регистрация нового пользователя
    public function postRegister(StoreUserRequest $request)
    {
        $credentials = $request->only('pin','name', 'passport', 'category_id', 'position', 'email', 'phone');
        $credentials['birthday'] = new Carbon($request->birthday);
        $credentials['password'] = bcrypt($request->password);
        $credentials['activity'] = true;

        $path = Storage::putFile('privat', $request->filename);

        $credentials['scan'] = $path;
        if ($user = User::create($credentials)) {
            if (Option::getOption('mailRegistration')){
                $to = $user->email;
                $data = [
                    'template' => 'register',
                    'subject' => __('Register in system'),
                    'user' => $user->name,
                    'email' => $user->email,
                    'password' => $request->password,
                ];
                Mail::to( $to )->send(new OrderShipped($data));
            }
            Auth::login($user);
            return response()->json(['redirect'=>route('home')]);
        } else {
            return response()->json(['error'=>'Wrong credentials'], 500);
        }
    }

    public function pageshow(Request $request, $page_slug)
    {
        $model = Page::where([['url','=',$page_slug,'or'],['id','=',$page_slug,'or']])->first();
        $this->data->breadcrumbs[] = ["title"=>$model->name, "url"=>route('page',$page_slug)];
        if ($model) {
            return view('page',[
                'model'=>$model,
                'data' => $this->data,
            ]);
        } else {
            abort(404);
        }
    }

    public function glossary(Request $request, $character = 'А')
    {
        $symbols = [
            "cyr0" =>"А","cyr1" =>"Б","cyr2" =>"В","cyr3" =>"Г","cyr4" =>"Д","cyr5" =>"Е","cyr6" =>"Ж","cyr7" =>"З","cyr8" =>"И",
            "cyr9" =>"Й","cyr10"=>"К","cyr11"=>"Л","cyr12"=>"М","cyr13"=>"Н","cyr14"=>"О","cyr15"=>"П","cyr16"=>"Р","cyr17"=>"С",
            "cyr18"=>"Т","cyr19"=>"У","cyr20"=>"Ф","cyr21"=>"Х","cyr22"=>"Ц","cyr23"=>"Ч","cyr24"=>"Ш","cyr25"=>"Щ","cyr26"=>"Ъ",
            "cyr27"=>"Ы","cyr28"=>"Ь","cyr29"=>"Э","cyr30"=>"Ю","cyr31"=>"Я"
        ];
        $character = isset($symbols[$character]) ? $symbols[$character] : $character;
        $this->data->breadcrumbs[] = ["title"=>__('Glossary'), "url"=>route('glossary')];
        $this->data->breadcrumbs[] = ["title"=>$character, "url"=>route('glossary',$character)];
        return view('glossary',[
            'character' => $character,
            'glossaries' => Glossary::where([
                    ['name','like',$character.'%'],
                    ['activity','=',true],
                    ['locale','=',app()->getLocale()]
                ])->orderBy('name','ASC')->get(),
            'data' => $this->data,
        ]);
    }
}
