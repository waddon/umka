<?php

namespace App\Traits;
use App;

use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;

use Carbon\Carbon;
use App\Option;
use App\Track;
use App\Module;
use App\User;
use App\Mailing;

trait CheckTrait
{

    # проверка открытых модулей на скорое закрытие и уведомление пользователей об этом
    static public function checkModules()
    {
        # чистим все связи у которых дата окончания была в прошлом
            $modulesStopCount  = 0;
            $modulesStopMails  = 0;
            $modulesStopUsersBlocked  = 0;
            foreach (Module::get() as $keymodule => $module) {
                $array = [];
                foreach ($module->groups as $keygroup => $group) {
                    # если первый день запуска модуля, то обнуляем у всех пользователей время бездействия
                    if ( Carbon::parse($group->pivot->date_start)->format('d.m.Y') == Carbon::now()->format('d.m.Y')){
                        foreach ($group->users as $key => $user) {
                            $user->last_action_at = Carbon::now();
                            $user->save();
                        }
                    }
                    if ( Carbon::parse($group->pivot->date_finish)->gte(Carbon::now()) ) {
                        $array[$group->id] = [
                            'date_start'=>$group->pivot->date_start,
                            'date_finish'=>$group->pivot->date_finish,
                        ];
                    } else {
                        $modulesStopCount++;
                        # отправляем всем пользователям письма об остановке модуля
                        # и блокируем пользователей, которые не прошли модуль
                            if (Option::getOption('mailModuleStop')){
                                foreach ($group->users as $key => $user) {
                                    $to = $user->email;
                                    $data = [
                                        'subject' => __('Module stop'),
                                        'message' => '',
                                        'module' => $module->name,
                                    ];
                                    Mail::to( $to )->send(new OrderShipped($data));
                                    $modulesStopMails++;
                                }
                            }
                    }
                }
                $module->groups()->sync( $array );
            } 

        # просматриваем все открытые модули всех групп на предмет скорого закрытия
            $beforeClosing = Carbon::now()->addHours( Option::getOption('timeModuleBeforeClosing',72) );
            $modulesBeforeCount  = 0;
            $modulesBeforeMails  = 0;
            foreach (Module::get() as $keymodule => $module) {
                foreach ($module->groups as $keygroup => $group) {
                    if ($group->pivot->notificated!=true && Carbon::parse($group->pivot->date_finish)->lte($beforeClosing) ){
                        $modulesBeforeCount++;
                        \Log::info( 'Отправляем' );
                        $group->pivot->notificated = true;
                        $group->pivot->save();
                        # отправляем пользователям, которые не прошли модуль, письма об грядущей остановке модуля
                            if (Option::getOption('mailModuleBeforeClosing')){
                                foreach ($group->users as $key => $user) {
                                    if (!isset($user->data->modules->{$module->id}->passed)){
                                        $to = $user->email;
                                        $data = [
                                            'subject' => __('Module will be stoped'),
                                            'message' => '',
                                            'module' => $module->name,
                                        ];
                                        Mail::to( $to )->send(new OrderShipped($data));
                                        $modulesBeforeMails++;
                                    }
                                }
                            }
                    } else {
                        \Log::info( 'Не отправляем уведомление, т.к. уже уведомлено или рано уведомлять о закрытии модуля' );
                    }
                }
            }

        Track::message([
                'modulesStopCount' => $modulesStopCount,
                'modulesStopMails' => $modulesStopMails,
                'modulesStopUsersBlocked' => $modulesStopUsersBlocked,
                'modulesBeforeCount' => $modulesBeforeCount,
                'modulesBeforeMails' => $modulesBeforeMails,
            ]);
        return true;
    }


    # проверка пользователей на бездействие
    static public function checkUsers()
    {
        # проверяем пользователей только открытых модулей
        $blocked_users = [];
        $usersBlocked = 0;
        $timeUserInactivity = Option::getOption('timeUserInactivity',144);
        foreach (Module::get() as $keymodule => $module) {
            foreach ($module->groups as $keygroup => $group) {
                if ( Carbon::parse($group->pivot->date_finish)->gte(Carbon::now()) && Carbon::parse($group->pivot->date_start)->lte(Carbon::now())){
                    foreach ($group->users as $key => $user){
                        if ($user->last_action_at && $user->last_action_at->addHours($timeUserInactivity)->lt(Carbon::now())) {
                            $blocked_users[] = $user->id;
                            $usersBlocked++;
                        }
                    }
                }
            }
        }
        $users = User::whereIn('id',$blocked_users)->get();
        foreach ($users as $key => $user) {
            $user->activity = false;
            $user->save();
            if (Option::getOption('mailUserInactivity')){
                $to = $user->email;
                $data = [
                    'subject' => __('User blocked'),
                    'message' => '',
                ];
                Mail::to( $to )->send(new OrderShipped($data));
            }
            \Log::info( 'Блокировка пользователя' );
        }
        Track::message([ 'usersBlocked' => $usersBlocked ]);

    }


    # отправка необходимых рассылок
    static public function sendMailings()
    {
        $now = Carbon::now()->format('d.m.Y');
        $mailings = Mailing::where([['sent','=',false]])->get();
        foreach ($mailings as $key => $mailing) {
            if ( $mailing->date_send->format('d.m.Y') == $now ) $mailing->send();
        }
    }
}
