<?php

namespace App\Traits;
use App;

trait ContentTrait
{

    public function getContent()
    {
        $result = '';
        $content = $this->translate(App::getLocale())->content;
        if ($content && is_array($content)){
            foreach ($this->translate(App::getLocale())->content as $key => $row) {
                $temp = str_replace('contenteditable="true"', '', $row);
                $temp = str_replace('card-group-control-left', 'card-group-control-right', $temp);
                $temp = str_replace('<a class="list-icons-item btn-remove-accordion-element" data-action="remove"></a>', '', $temp);
                $temp = preg_replace("/ {2,}/"," ",$temp);
                $result .=  $temp;
            }
        } else {
            $result = $content;
        }
        return $result;
    }


}