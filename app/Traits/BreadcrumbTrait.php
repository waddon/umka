<?php

namespace App\Traits;

trait BreadcrumbTrait
{
    static public function getBreadcrumb($parent=0)
    {

        $controllers = [
            'UserController' => 'users',
            'RoleController' => 'roles',
            'PermissionController' => 'permissions',
            'GroupController' => 'groups',
            'PageController' => 'pages',
            'MediaController' => 'medias',
            'GlossaryController' => 'glossaries',
            // 'MenuController' => 'menus',
            'CourseController' => 'courses',
            'ModuleController' => 'modules',
            'LessonController' => 'lessons',
            'ThemeController' => 'themes',
            'QuestionController' => 'questions',
            'QuizController' => 'quizzes',
        ];
        $icons = [
            'UserController' => '<i class="icon-users mr-2 icon-2x"></i>',
            'RoleController' => '<i class="icon-user-check mr-2 icon-2x"></i>',
            'PermissionController' => '<i class="icon-user-lock mr-2 icon-2x"></i>',
            'GroupController' => '<i class="icon-users4 mr-2 icon-2x"></i>',
            'PageController' => '<i class="icon-insert-template mr-2 icon-2x"></i>',
            'MediaController' => '<i class="icon-images2 mr-2 icon-2x"></i>',
            'GlossaryController' => '<i class="icon-spell-check mr-2 icon-2x"></i>',
            // 'MenuController' => '<i class="icon-menu2 mr-2 icon-2x"></i>',
            'CourseController' => '<i class="icon-clipboard3 mr-2 icon-2x"></i>',
            'ModuleController' => '<i class="icon-clipboard3 mr-2 icon-2x"></i>',
            'LessonController' => '<i class="icon-clipboard3 mr-2 icon-2x"></i>',
            'ThemeController' => '<i class="icon-stack-text mr-2 icon-2x"></i>',
            'QuestionController' => '<i class="icon-question7 mr-2 icon-2x"></i>',
            'QuizController' => '<i class="icon-clipboard2 mr-2 icon-2x"></i>',
        ];
        $data = [];
        $class  = debug_backtrace()[1]['class'] ? substr(strrchr(debug_backtrace()[1]['class'], "\\"), 1) : 'unknown';
        $method = debug_backtrace()[1]['function'] ? debug_backtrace()[1]['function'] : 'unknown';
        $data['breadcrumbs'] = [
            [
                'url' => route('home'),
                'title' => '<i class="icon-home2 mr-2"></i> '.__('Home'),
            ],
            [
                'url' => route('dashboard'),
                'title' => __('Dashboard'),
            ],
        ];
        if (isset($controllers[$class])){
            if($class!='AdminController') {
                if ($method != 'index'){
                    // if ($class != 'ThemeController'){
                        array_push($data['breadcrumbs'] , ['title' => __(ucwords($controllers[$class])), 'url' => route($controllers[$class].'.index')]);
                    // } else {
                    //     array_push($data['breadcrumbs'] , ['title' => __('Modules'), 'url' => route('modules.index')]);
                    //     array_push($data['breadcrumbs'] , ['title' => __(ucwords($controllers[$class])), 'url' => route('modules.show',$parent)]);
                    // }
                    array_push($data['breadcrumbs'] , ['title' => __(ucwords($method)) . ' ' . __(ucwords($controllers[$class])), 'url' => '#']);
                } else {
                    array_push($data['breadcrumbs'] , ['title' => __(ucwords($controllers[$class])), 'url' => route($controllers[$class].'.'.$method)]);
                }
            }
        }
        $data['icon']  = isset($icons[$class]) ? $icons[$class] : '<i class="icon-equalizer3 mr-2 icon-2x"></i>';
        $data['title'] = isset($controllers[$class]) ? __(ucwords($controllers[$class])) : __('Dashboard');

        if($class=='AdminController'){
            if($method=='settings'){
                array_push($data['breadcrumbs'] , ['title' => __(ucwords('settings')), 'url' => route('settings') ]);
                $data['icon'] = '<i class="icon-cog mr-2 icon-2x"></i>';
                $data['title'] = __('Settings');
            }
            if($method=='tracks'){
                array_push($data['breadcrumbs'] , ['title' => __(ucwords('event log')), 'url' => route('tracks') ]);
                $data['icon'] = '<i class="icon-eye mr-2 icon-2x"></i>';
                $data['title'] = __(ucwords('event log'));
            }
            if($method=='phpinfo'){
                array_push($data['breadcrumbs'] , ['title' => __(ucwords('about system')), 'url' => route('phpinfo') ]);
                $data['icon'] = '<i class="icon-cube3 mr-2 icon-2x"></i>';
                $data['title'] = __(ucwords('about system'));
            }
            if($method=='offline'){
                array_push($data['breadcrumbs'] , ['title' => __(ucwords('offline version creating')), 'url' => route('offline') ]);
                $data['icon'] = '<i class="icon-download mr-2 icon-2x"></i>';
                $data['title'] = __(ucwords('offline version creating'));
            }
        }
        return $data;
    }

}