<?php

namespace App;

use App;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Question extends Model implements TranslatableContract
{
    use Translatable;
    
    public $translatedAttributes = ['name','content'];
    protected $guarded = [];

    public function module()
    {
        return $this->belongsTo('App\Module');
    }

    static public function types()
    {
        $result = [
            0 => __('Select Type'),
            1 => __('Radio'),
            2 => __('Check'),
            3 => __('Order'),
        ];
        return $result;
    }

    public function type()
    {
        $result = [
            0 => __('Select Type'),
            1 => __('Radio'),
            2 => __('Check'),
            3 => __('Order'),
        ];
        return $result[$this->type_id];
    }

    # проверка правильности ответов по вопросу (0 - неправильно, 1 - правильно)
    public function check($answer='')
    {
        $result = 0;
        \Log::info('=== ответ на ' . ($this->id) . ' вопрос: ' . (is_array($answer) ? json_encode($answer) : $answer));
        # одиночный выбор
            if ($this->type_id==1 && !is_array($answer)){
                foreach ($this->content as $key => $option) {
                    if ($option->check==true && $option->id == $answer) {
                        $result = 1;
                        //\Log::info('+++   правильный вариант: ' . $option->text);
                    } else {
                        //\Log::info('--- неправильный вариант: ' . $option->text . ($option->check ? ' (+)' : ''));
                    }
                }
            }
        # множественный выбор
            if ($this->type_id==2 && is_array($answer)){
                $corrects = 0;
                $c_exists = 0;
                foreach ($this->content as $key => $option) {
                    if ($option->check==true) $corrects++;
                    if ($option->check==true && in_array($option->id, $answer)) $c_exists++;
                }
                if ($corrects == $c_exists && count($answer) == $corrects) $result=1;
            }
        # сортировка
            if ($this->type_id==3 && is_array($answer)){
                $corrects = 0;
                $c_exists = 0;
                foreach ($this->content as $key => $option) {
                    $corrects++;
                    if (isset($answer[$key]) && $option->id==$answer[$key]) $c_exists++;
                }
                if ($corrects == $c_exists) $result=1;
            }
        return $result;
    }
}