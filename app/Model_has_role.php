<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Model_has_role extends Model
{
    protected $table = 'model_has_roles';
    public $timestamps = FALSE;
}
