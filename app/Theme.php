<?php

namespace App;

use App;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Theme extends Model implements TranslatableContract
{
    use Translatable;
    
    public $translatedAttributes = ['name','content'];
    protected $guarded = [];

    protected $casts = [
        'data' => 'object',
    ];

    public function lesson()
    {
        return $this->belongsTo('App\Lesson');
    }

    public function module()
    {
        return $this->belongsTo('App\Module');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public static function boot()
    {
        parent::boot();
        self::created(function($model){
            $model->order = self::getOrder($model->module_id);
            $model->data = (object)[];
            $model->save();
        });
    }

    public static function getOrder($module_id=0)
    {
        $element = Theme::where('module_id','=',$module_id)->orderBy('order','DESC')->first();
        if ($element){
            return $element->order + 1;
        } else {
            return 1;
        }
    }

    public function previousThemeId()
    {
        $element = $this->module->activeThemes->where('order','<',$this->order)->sortByDesc('order')->first();
        $result = $element ? $element->id : 0;
        return $result;
    }

    public function nextThemeId()
    {
        $element = $this->module->activeThemes->where('order','>',$this->order)->sortBy('order')->first();
        $result = $element ? $element->id : 0;
        return $result;
    }

    public function ifThemePassed()
    {
        $data = auth()->user()->data;
        if (isset($data->themes->{$this->id}->passed) && Option::getoption('tickPassedThemes')) {$result = true;}
        else {$result = false;}
        return $result;
    }
}