<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{

    protected $fillable = [
        'key',
        'value',
    ];


    static public function getOption($key='',$default=false)
    {
        $result = $default;
        if ($key){
            $option = Option::where('key','=',$key)->first();
            if ($option) {
                $result = $option->value;
            }
        }
        return $result;
    }


    static public function setOption($key='', $value='')
    {
        $result = false;
        if ($key){
            Option::updateOrCreate(
                ['key'=>$key],
                ['value'=>$value]
            );
        }
        return $result;
    }

    static public function getUpdate($key='')
    {
        $result = false;
        if ($key){
            $option = Option::where('key','=',$key)->first();
            if ($option) {
                $result = $option->updated_at;
            }
        }
        return $result;
    }

}
