<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleTranslation extends Model
{

    protected $casts = [
        'messages' => 'object',
    ];

    protected function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    public $timestamps = false;
    protected $fillable = ['name','content', 'messages'];
}