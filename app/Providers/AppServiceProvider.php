<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App;
use Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        # Вытаскиваем свойство из таблицы
        Blade::directive('getterm', function ($name='') {
            return "<?php echo app('getterm')->show($name); ?>";
        });

        # Выбор языков
        Blade::directive('langselector', function ($name='') {
            return "<?php echo app('langselector')->show($name); ?>";
        });

        # добавляем роуты к ресурсам
        $registrar = new \App\Routing\ResourceAddRouts($this->app['router']);
        $this->app->bind('Illuminate\Routing\ResourceRegistrar', function () use ($registrar) {
            return $registrar;
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::singleton('getterm', function(){
            return new \App\Directives\Getterm();
        });
        App::singleton('langselector', function(){
            return new \App\Directives\Langselector();
        });
    }
}
