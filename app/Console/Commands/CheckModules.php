<?php

namespace App\Console\Commands;

use App\Traits\CheckTrait;

use App\Option;
use Illuminate\Console\Command;

class CheckModules extends Command
{
    use CheckTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:modules';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '---> Check open modules';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $previousPid = Option::getOption($this->signature);
        if ( $previousPid && function_exists ('posix_getpgid') && posix_getpgid($previousPid) ) {
            \Log::warning( __('messages.process-busy', ['name' => $this->signature]) );
        } else {
            \Log::info( __('messages.process-started', ['name' => $this->signature]) );
            Option::setOption( $this->signature, getmypid() );
            self::checkModules();
            self::sendMailings();
            \Log::info( __('messages.process-finished', ['name' => $this->signature]) );
        }
    }

}
