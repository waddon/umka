<?php
namespace App\Directives;

//use App\Option;
use App;

class Langselector{

    public function show($obj='', $data =[]){
        $result = '';
        $locale = App::getLocale();

        $languages = config()->get('app.locales');

        if (count($languages)>1){
            $result  = '<li class="nav-item dropdown">';
            $result .= '<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">';
            $result .= '<img src="/assets/img/lang/'.$locale.'.png" class="img-flag mr-2" alt="">' . __($locale) . '</a>';
            $result .= '<div class="dropdown-menu dropdown-menu-right">';
            foreach ($languages as $key => $language) {
                $result .= '<a href="' . route('setlocale', ['lang' => $language]) .'" class="dropdown-item'. ( $language==$locale ? ' active' : '').'"><img src="/assets/img/lang/'.$language.'.png" class="img-flag" alt=""> '.__($language).'</a>';
            }
            $result .= '</div>';
            $result .= '</li>';
        }

        return $result;
    }
}