<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;

class Mailing extends Model
{

    protected $dates = ['date_send'];

    protected $guarded = [];

    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    public function send()
    {
        foreach ($this->group->users as $key => $user) {
            $to = $user->email;
            $data = [
                'subject' => $this->name,
                'message' => $this->content,
            ];
            Mail::to( $to )->send(new OrderShipped($data));
        }
        $this->sent = true;
        $this->save();        
    }
}
