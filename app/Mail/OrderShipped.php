<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = isset($this->data['template']) ? $this->data['template'] : 'default';
        $subject  = isset($this->data['subject'])  ? $this->data['subject']  : 'Subject';
        $from     = isset($this->data['from'])     ? $this->data['from']     : env('MAIL_FROM_ADDRESS');
        $project  = isset($this->data['project'])  ? $this->data['project']  : env('MAIL_FROM_NAME');

        return $this->view( 'mails.' . $template, [
                'data' => $this->data,
            ])
            ->subject( $subject )
            ->from( $from, $project );
    }
}
