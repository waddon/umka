<?php

namespace App;

use App\Traits\ContentTrait;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Page extends Model implements TranslatableContract
{
    use Translatable;
    use ContentTrait;
    
    public $translatedAttributes = ['name','content'];
    protected $fillable = ['key','url'];

    static public function pluckTranslate($placeholder = 0)
    {
        $result = $placeholder ? [0=> __('Select') . ' '. __('Page')] : [];
        foreach (Page::get() as $key => $element) {
            $result[$element->id] = Option::getoption('keyOrName') ? $element->name : $element->key;
        }
        return $result;
    }

}