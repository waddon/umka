<?php
namespace App\Widgets;

use App\User;
use App\Role;

use App\Widgets\Contract\ContractWidget;

class UsersWidget implements ContractWidget 
{
    public function execute(){
        $data = (object)[
            'all' => User::count(),
            'list' => (object)[],
        ];
        foreach (Role::get() as $key => $role) {
            $data->list->{__($role->name)} = $role->users->count();
        }
        return view('Widgets::users', [
            'data' => $data
        ]);
    }
}