<?php
namespace App\Widgets;

use App\Module;

use App\Widgets\Contract\ContractWidget;

class ModulesWidget implements ContractWidget 
{
    public function execute(){
        $data = (object)[
            'all' => Module::count(),
            'list' => (object)[],
        ];
        foreach (Module::get() as $key => $module) {
            //$data->list->{$module->name} = $module->themes->count();
            $data->list->{$module->name} = 0;
            foreach ($module->lessons as $key => $lesson) {
                $data->list->{$module->name} += $lesson->themes->count();
            }
        }
        return view('Widgets::modules', [
            'data' => $data
        ]);
    }
}