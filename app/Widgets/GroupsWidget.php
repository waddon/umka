<?php
namespace App\Widgets;

use App\Group;

use App\Widgets\Contract\ContractWidget;

class GroupsWidget implements ContractWidget 
{
    public function execute(){
        $data = (object)[
            'all' => Group::count(),
            'list' => (object)[],
        ];
        foreach (Group::get() as $key => $group) {
            $data->list->{$group->name} = $group->users->count();
        }
        return view('Widgets::groups', [
            'data' => $data
        ]);
    }
}