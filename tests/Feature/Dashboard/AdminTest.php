<?php

namespace Tests\Feature\Dashboard;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdminTest extends TestCase
{
    /**
     * Главная панель админки.
     *
     * @return void
     */
    public function testDashboard()
    {
        $response = $this->withoutMiddleware()->get('/dashboard/');
        $response->assertStatus(200);
    }

}
