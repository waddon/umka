<?php

namespace Tests\Feature\Dashboard;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{

    /**
     * Список пользователей
     *
     * @return void
     */
    public function testUserIndex()
    {
        $response = $this->withoutMiddleware()->get(route('users.index'));
        $response->assertStatus(200);
    }

    /**
     * Форма добавления пользователя
     *
     * @return void
     */
    public function testUserFormCreate()
    {
        $response = $this->withoutMiddleware()->get(route('users.create'));
        $response->assertStatus(200);
    }

    /**
     * Форма просмотра пользователя
     *
     * @return void
     */
    public function testUserShow()
    {
        $response = $this->withoutMiddleware()->get(route('users.show',['user'=>1]));
        $response->assertStatus(200);
    }

    /**
     * Форма редактирования пользователя
     *
     * @return void
     */
    public function testUserFormEdit()
    {
        $response = $this->withoutMiddleware()->get(route('users.edit',['user'=>1]));
        $response->assertStatus(200);
    }

    /**
     * Сообщения пользователя
     *
     * @return void
     */
    public function testUserMessages()
    {
        $response = $this->withoutMiddleware()->get(route('users.message',['id'=>1]));
        $response->assertStatus(200);
    }

    /**
     * Скачивание документа, подтверждающего личность
     *
     * @return void
     */
    public function testUserDownload()
    {
        $response = $this->withoutMiddleware()->get(route('users.download',['id'=>1]));
        $response->assertStatus(200);
    }
}
