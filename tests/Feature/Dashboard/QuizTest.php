<?php

namespace Tests\Feature\Dashboard;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class QuizTest extends TestCase
{

    /**
     * Список опросников
     *
     * @return void
     */
    public function testQuizIndex()
    {
        $response = $this->withoutMiddleware()->get(route('quizzes.index'));
        $response->assertStatus(200);
    }

    /**
     * Форма добавления опросника
     *
     * @return void
     */
    public function testQuizFormCreate()
    {
        $response = $this->withoutMiddleware()->get(route('quizzes.create'));
        $response->assertStatus(200);
    }

    /**
     * Форма просмотра опросника
     *
     * @return void
     */
    public function testQuizShow()
    {
        $response = $this->withoutMiddleware()->get(route('quizzes.show',['quiz'=>1]));
        $response->assertStatus(200);
    }

    /**
     * Форма редактирования опросника
     *
     * @return void
     */
    public function testQuizFormEdit()
    {
        $response = $this->withoutMiddleware()->get(route('quizzes.edit',['quiz'=>1]));
        $response->assertStatus(200);
    }

}
